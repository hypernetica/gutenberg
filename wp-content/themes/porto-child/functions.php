<?php

require 'inc/breadcrumbs.php';
require 'inc/slider.php';

add_action( 'wp_enqueue_scripts', 'porto_child_css', 1001 );

// Load CSS
function porto_child_css() {
	// porto child theme styles
	wp_deregister_style( 'styles-child' );
	wp_register_style( 'styles-child', esc_url( get_stylesheet_directory_uri() ) . '/style.css' );
	wp_enqueue_style( 'styles-child' );

	if ( is_rtl() ) {
		wp_deregister_style( 'styles-child-rtl' );
		wp_register_style( 'styles-child-rtl', esc_url( get_stylesheet_directory_uri() ) . '/style_rtl.css' );
		wp_enqueue_style( 'styles-child-rtl' );
	}
}

function porto_child_lang_setup() {
    $path = get_stylesheet_directory().'/languages';
    load_child_theme_textdomain( 'porto-child', $path );
}
add_action( 'after_setup_theme', 'porto_child_lang_setup' );

add_shortcode( 'show_username', 'show_username_function' );
function show_username_function( $atts ) {
	global $current_user, $user_login;
	wp_get_current_user();
	add_filter('widget_text', 'do_shortcode');
	if ($user_login) 
		return $current_user->display_name;
	else
		return '';
}

/**
 * Extend WordPress search to include custom fields
 *
 * https://adambalee.com
 */

/**
 * Join posts and postmeta tables
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {    
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}
add_filter('posts_join', 'cf_search_join' );

/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where( $where ) {
    global $pagenow, $wpdb;

    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct( $where ) {
    global $wpdb;

    //if ( is_search() ) {
        return "DISTINCT";
    //}

    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );


/** 
 *  Main ordering logic for orderby attribute
 *  Refrence: https://docs.woocommerce.com/document/custom-sorting-options-ascdesc/
 */
add_filter('woocommerce_get_catalog_ordering_args', 'wh_catalog_ordering_args');

function wh_catalog_ordering_args($args) {
    global $wp_query;
    if (isset($_GET['orderby'])) {
		//$args['orderby'] = "SUBSTRING_INDEX(meta_value, '/', 1)  CONVERT(TRIM(SUBSTRING_INDEX(meta_value, '/', -1)), UNSIGNED INTEGER)"; //'meta_value title'; 
		$args['orderby'] = 'meta_value title'; 
        switch ($_GET['orderby']) {
            //for attribute/taxonomy=series
            case 'title' :
				$args['order'] = 'ASC';
				$args['orderby'] = 'title'; 
                break;
            case 'title-desc' :
                $args['order'] = 'DESC';
				$args['orderby'] = 'title'; 
                break;
            case 'series' :
				$args['order'] = 'ASC';
				$args['meta_key'] = '_srt_pa_series';
                break;
            case 'series-desc' :
                $args['order'] = 'DESC';
				$args['meta_key'] = '_srt_pa_series';
                break;
            case 'isbn' :
                $args['order'] = 'ASC';
				$args['meta_key'] = '_wwcp_isbn';
                break;
            case 'isbn-desc' :
                $args['order'] = 'DESC';
				$args['meta_key'] = '_wwcp_isbn';
                break;
	    	case 'catalogcode' :
                $args['order'] = 'ASC';
				$args['meta_key'] = '_wwcp_catalog_code';
                break;
            case 'catalogcode-desc' :
                $args['order'] = 'DESC';
				$args['meta_key'] = '_wwcp_catalog_code';
                break;
	    	case 'datepublished' :
				$args['order'] = 'ASC';
				$args['meta_key'] = '_wwcp_published_date';
                $args['orderby'] = 'meta_value';
                break;
            case 'datepublished-desc' :
                $args['order'] = 'DESC';
				$args['meta_key'] = 'bk_det_published_date';
                $args['orderby'] = 'meta_value';
				break;
            //for attribute/taxonomy=pa_length
            case 'pa-length-asc' :
                $args['order'] = 'ASC';
                $args['meta_key'] = 'pa_length';
                $args['orderby'] = 'meta_value_num';
                break;
            case 'pa-length-desc' :
                $args['order'] = 'DESC';
                $args['meta_key'] = 'pa_length';
                $args['orderby'] = 'meta_value_num';
                break;
        }
		
    }
    return $args;
}

/**
 *  Lets add the created sorting order to the dropdown list.
 *  Refrence: http://hookr.io/filters/woocommerce_catalog_orderby/
 */
//To under Default Product Sorting in Dashboard > WooCommerce > Settings > Products > Display.
add_filter('woocommerce_default_catalog_orderby_options', 'wh_catalog_orderby' );
add_filter('woocommerce_catalog_orderby', 'wh_catalog_orderby');

function wh_catalog_orderby($sortby) {

// 	  Remove default options
    unset( $sortby['popularity'] );   
	unset( $sortby['rating'] );   
	unset( $sortby['date'] );   
	unset( $sortby['price'] );   
	unset( $sortby['price-desc'] );   
	unset( $sortby['menu_order'] );   
// 'menu_order' => __( 'Default sorting', 'woocommerce' ),
// 'popularity' => __( 'Sort by popularity', 'woocommerce' ),
// 'rating'     => __( 'Sort by average rating', 'woocommerce' ),
// 'date'       => __( 'Sort by newness', 'woocommerce' ),
// 'price'      => __( 'Sort by price: low to high', 'woocommerce' ),
// 'price-desc' => __( 'Sort by price: high to low', 'woocommerce' ),
	
	
//    Add various options
//    $sortby['series-asc'] = 'Ανά Σειρά (αύξουσα)';
//    $sortby['series-desc'] = 'Ανά Σειρά (φθίνουσα)'; // Translate these
//    $sortby['isbn-asc'] = 'Ανά ISBN (αύξουσα)';
//    $sortby['isbn-desc'] = 'Ανά ISBN (φθίνουσα)'; // Translate these
//    $sortby['catalogcode-asc'] = 'Ανά κωδικό καταλόγου (αύξουσα)';
//    $sortby['catalogcode-desc'] = 'Ανά κωδικό καταλόγου (φθίνουσα)'; // Translate these	
    $sortby['title'] = _x( 'Title', 'Sort by', 'porto-child' );
//	$sortby['title-desc'] = __( 'Title: descending', 'porto-child' );
//	$sortby['datepublished'] = _x( 'Publication date', 'Sort by', 'porto-child' );//'Παλαιότερη έκδοση';
    $sortby['datepublished-desc'] = _x( 'Newest First', 'Sort by', 'porto-child' );// 'Νεότερη έκδοση'; 
    return $sortby;
}

// -------------------------------------------------------------------
// 
function alert($msg) {
    echo "<script type='text/javascript'>alert('$msg');</script>";
}

/**
 * Returns the book categories in a list.
 *
 * @param int    $product_id Product ID.
 * @param string $sep (default: ', ').
 * @param string $before (default: '').
 * @param string $after (default: '').
 * @return string
 */
function wc_get_book_categories_list( $product_id, $sep = ', ', $before = '', $after = '' ) {
        return get_the_term_list( $product_id, 'book_category', $before, $sep, $after );
}

/**
 * Returns the book series in a list.
 *
 * @param int    $product_id Product ID.
 * @param string $sep (default: ', ').
 * @param string $before (default: '').
 * @param string $after (default: '').
 * @return string
 */
function wc_get_book_series_list( $product_id, $sep = ', ', $before = '', $after = '' ) {
	return get_the_term_list( $product_id, 'pa_series', $before, $sep, $after );
}

// ---------
// Attempt at adding natural sorting 
/*function orderbyreplace($orderby_statement, $wp_query) {
	// do not modify queries in the admin
    if( is_admin() ) { 
        return $orderby_statement;
    }

    $orderby_statement = "";
	
    return $orderby_statement;}
add_filter('posts_orderby','orderbyreplace', 10, 2 );*/

// ---------
// GUTENBERG-35

add_filter('posts_fields', 'wpcf_create_temp_column');

function wpcf_create_temp_column($fields) {
  global $wpdb;
  $matches = 'Το|Η|Ο|Τα|Οι|Στο|Στις|Στην|Τις|Στα';
  //THEN trim(substr($wpdb->posts.post_title from 3)) 
  $has_the = " CASE 
      WHEN $wpdb->posts.post_title regexp( '^($matches)[[:space:]]' )
		THEN trim(SUBSTRING($wpdb->posts.post_title, LOCATE(' ', $wpdb->posts.post_title)+1)) 
      ELSE $wpdb->posts.post_title 
        END AS title2";
  if ($has_the) {
    $fields .= ( preg_match( '/^(\s+)?,/', $has_the ) ) ? $has_the : ", $has_the";
  }
  return $fields;
}


add_filter('posts_orderby', 'wpcf_sort_by_temp_column');

function wpcf_sort_by_temp_column ($orderby) {
	if (strpos($orderby, 'wp_posts.post_title') !== false) {
  		$custom_orderby = " UPPER(REPLACE(REPLACE(REPLACE(title2, '\'', ''), '(', ''), '\"', '')) ASC";
		if ($custom_orderby) {
		    $orderby = $custom_orderby;
	  	}
  }
  return $orderby;
}

// ------------------------------------------------
// GUTENBERG-42
// ------------------------------------------------
// 

// Move Cart to the 3rd column
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'porto_woocommerce_single_product_summary2', 'woocommerce_template_single_add_to_cart', 20 );

// Move Price to the 3rd column
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_filter( 'porto_woocommerce_single_product_summary2', 'woocommerce_template_single_price', 10 );

// Remove Additional Information tab
add_filter( 'woocommerce_product_tabs', 'hyp_remove_extra_info_tab', 98 );
 
function hyp_remove_extra_info_tab( $tabs ) {
    unset( $tabs['additional_information'] ); 
    return $tabs;
}
// Add extra tabs
add_filter( 'woocommerce_product_tabs', 'woo_bookstore_extra_tabs',100 );


function woo_bookstore_extra_tabs( $tabs ) {

        global $product;
        if( $product->product_type == 'book'  ){
            $tabs['details'] = array(
				'title'         =>      __( 'Book Details', 'porto-child' ),
				'priority'      =>      1,
				'callback'      => 'woo_bookstore_extra_tabs_content'
			);    
			
			$tabs['producers'] = array(
				'title'         =>      __( 'Book Producers', 'porto-child' ),
				'priority'      =>      2,
				'callback'      => 'woo_bookstore_extra_tabs_content'
			);
			
			$tabs['external_reviews'] = array(
				'title'         =>      __( 'External Reviews', 'porto-child' ),
				'priority'      =>      4,
				'callback'      => 'woo_bookstore_extra_tabs_content'
			);
			
			$tabs['additional_material'] = array(
				'title'         =>      __( 'Additional Material', 'porto-child' ),
				'priority'      =>      5,
				'callback'      => 'woo_bookstore_extra_tabs_content'
			);
			
			$tabs['audiovisual_material'] = array(
				'title'         =>      __( 'Audio-visual Material', 'porto-child' ),
				'priority'      =>      6,
				'callback'      => 'woo_bookstore_extra_tabs_content'
			);

			$tabs['author'] = array(
				'title'         =>      __( 'Author Information', 'porto-child' ),
				'priority'      =>      9,
				'callback'      => 'woo_bookstore_extra_tabs_content'
			);
			
        }

        return $tabs;

}

function woo_bookstore_extra_tabs_content($name) {

	global $product;
        $book 			= new WC_Product_Book( $product->id );
        $book_data      = array();
        $count 			= 0;
        $custom_fields 	= woo_book_get_custom_fields();
		$tab_name 		= str_replace('_', '-', $name);

        foreach( $custom_fields as $field ){
                if( isset( $field['visible'] ) && $field['visible'] == 'yes' ){
                        $book_data[$count] = array(
							'title' => apply_filters( 'woo_bookstore_custom_field_name', $field['name'], 'book-field' ),
                            'value' => $book->{$field['meta_key']},
                            'class' => str_replace( ',', ' ', $field['class'] )
                        );
                        $count++;
                }
        }

        if( !empty( $book ) ){
                $args = array(
					'book'                 => $book,
                    'book_data'            => $book_data,
                    'book_custom_fields'   => $custom_fields
                );
                $wc_get_template = function_exists('wc_get_template') ? 'wc_get_template' : 'woocommerce_get_template';
                $wc_get_template( 'single-product/tabs/tab-'. $tab_name . '.php', $args, '', get_stylesheet_directory() . 'woocommerce/' );
        }
}

/* ---------------------------- */
/* Add Excerpt div in thumbnail */
/* ---------------------------- */
function filter_woocommerce_single_product_image_html( $sprintf, $post_id ) { 
    // Check for excerpt 
    //$pattern = '/(?<=href=")([^"]*)/';
    //$replacement = get_permalink($post->ID);
    $issuu_code = get_field_object('bk_det_excerpt');
	if ($issuu_code) {
		wp_register_script( 'hyp-show-excerpt', get_stylesheet_directory_uri() . '/js/gutenberg.js', array( 'jquery' ));
        wp_enqueue_script( 'hyp-show-excerpt');
        wp_localize_script('hyp-show-excerpt', 'params', array(
                'issuu_code' => $issuu_code["value"],
        ));
		wp_register_script( 'issuu-embed', '//e.issuu.com/embed.js');
        wp_enqueue_script( 'issuu-embed');
		
		
        return preg_replace('/<div class="img-thumbnail">(.*?)<\/div>/',
							'
							<div class="img-thumbnail">
								$1
								<a href="#excerpt_iframe" class="lightbox" id="show_excerpt">
									<div class="img-excerpt-overlay">
										<span class="excerpt-text">Διαβάστε ένα απόσπασμα</span>
									</div>
								</a>
							</div>
							<div id="excerpt_iframe" class="white-popup issuuembed mfp-hide" data-configid="' . $issuu_code['value'] . '" ></div>'
								,$sprintf);
	}
	
	return $sprintf;
}; 
         
// ------------------------
// add the See Also slider 
// add the action 
add_action( 'woocommerce_after_single_product', 'show_see_also_slider', 10, 0 ); 

// ------------------------
