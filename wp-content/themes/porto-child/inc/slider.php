<?php

/* Slider functions. 
 * "See also" slider shows:
 * a) books from same series, if none then
 * b) books from same author, if none then
 * c) books from same book_category
 * Limit: 10
 */

function show_see_also_slider(  ) { 

	global $post;
	$post_id = get_the_ID();
	$tax_name = '';
	
	// Just one series per book.
	$series = get_field('bk_det_series');
	if ($series) {
		$query = new WP_Query(array('post_type'     => 'product',
									'post__not_in'  => array($post_id),
									'tax_query'     => array(
										array(
											'taxonomy' => 'book_series',
											'field'    => 'term_id',
											'terms'    => $series->term_id,
										),
									),
								));
		if ($query->have_posts()) {
			$tax_name = 'book_series';
			$tax_desc = __('from the same series', 'porto-child');
			$tax_value = $series->term_id;
		}
	}

	// A book can have many authors, get all.
	if (!($tax_name)) {
		$authors = get_field('bk_synt_author');
		if ($authors) {
			$query = new WP_Query(array('post_type'     => 'product',
										'post__not_in'  => array($post_id),
										'tax_query'     => array(
											array(
												'taxonomy' => 'book_author',
												'field'    => 'term_id',
												'terms'    => $authors,
											),
										),
									));
			if ($query->have_posts()) {
				$tax_name = 'book_author';
				$tax_desc = __('from the same author', 'porto-child');
				$tax_value = implode(',' , $authors);
			}
		}
	}

	if (!($tax_name)) {
		$arr = get_the_terms($post, 'book_category');
		if ($arr) {
			foreach ($arr as $term) {
				$book_cats[] = $term->term_id;
			}
			$query = new WP_Query(array('numberposts'	=> 1,
										'post_type'     => 'product',
										'post__not_in'  => array($post_id),
										'tax_query'     => array(
											array(
												'taxonomy' => 'book_category',
												'field'    => 'term_id',
												'terms'    => $book_cats,
												'include_children' => false,
												'operator' => 'IN',
											),
										),
									));
			if ($query->have_posts()) {
				$tax_name = 'book_category';
				$tax_desc = __('from the same category', 'porto-child');
				$tax_value = implode(',' , $book_cats);
			}
		}
	}
	if (!$tax_name) {
		return; // No "See Also" slider.
	}
	/*switch ($tax_name) {
		case 'book_category':
			echo ('Will search with categories: ' . $tax_value);
			break;
		case 'book_author':
			echo ('Will search with author: ' . $tax_value);
			break;
		case 'book_series':
			echo ('Will search with series: ' . $tax_value);
			break;
		default: 
			return;
	}*/
?>
	<div class="slider_wrapper">
		<div class="slider_wrapper_title">
			<?php echo ( __('See Also', 'porto-child') . ', ' . $tax_desc); ?>
		</div>
		<div class="slider_wrapper_content">
			<?php echo do_shortcode('[products_slider limit="10" cats="' . $tax_value . '" tax="' . $tax_name . '" limit="10"]'); ?> 
		</div>
	</div>
<?php
}; 
