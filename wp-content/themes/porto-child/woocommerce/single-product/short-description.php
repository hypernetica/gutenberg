<?php
/**
 * Single product short description
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/short-description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $product;
$dimensions = $product->get_dimensions(false);
$vals['shape'] =  implode( ' x ', array_filter( array_map( 'wc_format_localized_decimal', $dimensions ) ) ); 

foreach (['isbn', 
		  'selides', 
		  'published_date'] as $key ) {
	
	$arr = get_field_object('bk_det_' . $key);
	if (isset($arr)) {
		$vals[$key] = $arr['value'];
	} else {
		$vals[$key] = '';
	}
}

// Parse date and truncate month
if (isset($vals['published_date'])) {
	$loc = setlocale(LC_ALL, 0);
	setlocale(LC_ALL, 'el_GR.utf8');
	$parsed_time = strptime($vals['published_date'], '%B %Y');
	if ($parsed_time)
		$vals['published_date'] = 1900 + $parsed_time['tm_year'];
	setlocale(LC_ALL, $loc);
}

$short_description = get_field_object('bk_det_subtitle');

?>
<div class="woocommerce-product-details__short-description">
	<?php echo $short_description['value']; // WPCS: XSS ok. ?>
</div>

<div class="book-info-blocks">
	<div class="subcol-1">
		<div>
			<span class="force-first"><?php echo __('ISBN', 'porto-child') ?></span>
			<span class="book-info-block-value"><?php echo ($vals['isbn'] ?: ' - ') ?></span>
		</div>
	</div>
	<div class="subcol-3" >
		<div class="border-top border-right">
			<span class="force-first" style="text-align: right;"><?php echo __('publish year', 'porto-child') ?></span>
			<span class="book-info-block-value"><?php echo ($vals['published_date'] ?: ' - ') ?></span>
		</div>
		<div class="border-top border-right">
			<span class="force-last"><?php echo __('pages', 'porto-child') ?></span>
			<span class="book-info-block-value"><?php echo ($vals['selides'] ?: ' - ') ?></span>
		</div>
		<div class="border-top" style="flex-basis: 20%; flex-direction: column;">
			<span class="force-first no-top-margin"><?php echo __('shape', 'porto-child') ?></span>
			<span class="book-info-block-value no-top-margin" ><?php echo ($vals['shape'] ?: ' - ') ?></span>
		</div>
	</div>
</div>
