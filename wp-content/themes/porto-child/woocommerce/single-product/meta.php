<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( $product->is_type( 'book' ) ) : ?>
	
	<?php

	$field = get_field_object('bk_det_diglossi_triglossi_ekdosi');
	$label = $field['value'];
	if (isset($label)) {
		echo '<span class="book_language">' . $label . '<br/></span>';
	}
	?>
	
	<?php echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'porto-child' ) . ' ', '</span>' ); ?>

	<?php $book_categories = wp_get_post_terms( $product->get_id(), 'book_category', $args ); ?>
        <?php echo wc_get_book_categories_list( $product->get_id(), ', ', '<span class="book_categories">' . _n( 'Book Category:', 'Book Categories:', count( $book_categories ), 'porto-child' ) . ' ', '</span>' ); ?>

	<?php endif; ?>
	
	<?php echo wc_get_product_tag_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'porto-child' ) . ' ', '</span>' ); ?>


	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>
