<?php
/**
 * Book Additional Materials Tab
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product/tabs/tab-syntelestes.php
 *
 * @author 	Spyros Nathanail
 * @package 	WooBookstore/templates
 * @version     1.0
 */
?>
<?php global $product; ?>

    
<p>
<table class="woocommerce-product-attributes shop_attributes table table-striped">
    <tbody>
		<?php 
		// Additional material
		$repeater = get_field_object('bk_det_additional_material');
		if ($repeater) {
			echo ('
				<tr> 
				<td class="woocommerce-product-attributes-item__value">'
			);
			foreach ($repeater['value'] as $file_arr) {
				$file = $file_arr['bk_det_file'];
				//var_dump ($file);
				$url = $file['url'];
				$title = $file['title'];
				$caption = $file['caption'];
				$icon = $file['icon'];
				if( $file['type'] == 'image' ) {
					$icon =  $file['sizes']['thumbnail'];
				}
				echo ('<div class="gut-file-repeater">');
				if( $caption ) {
					echo ('<span class="wp-caption">');
				}
				echo ('
				<a href="' . $url .'" title="' . $title .'" download><img src="' . $icon .'" /></a>
				');

				if( $caption ) {
					echo ('
						<p class="wp-caption-text">' . $caption .'</p>
						</span>
					');
				}
				echo ('</div>');
			}
			echo ('</td>
			</tr>');
		}
		
		?>
	</tbody>
</table>	
</p>

