<?php
/**
 * Book External Reviews Tab
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product/tabs/tab-syntelestes.php
 *
 * @author 	Spyros Nathanail
 * @package 	WooBookstore/templates
 * @version     1.0
 */
?>
<?php global $product; ?>

    
<p>
<table class="woocommerce-product-attributes shop_attributes table table-striped">
    <tbody>
		<?php 
		// External reviews
		$repeater = get_field_object('bk_det_external_reviews');
		if ($repeater) {
			echo ('
				<tr> 
				<td class="woocommerce-product-attributes-item__value">'
			);
			//var_dump ($repeater);
			foreach ($repeater['value'] as $row) {
				$link = $row['bk_det_ext_rev_link'];
				echo ('<div class="book-ext-review-link"><a href="' . $link['url'] . '">' . $link['title'] . '</a></div>');
			}
			echo ('
				</td> 
				</tr>'
			);
		
		}
		?>
	</tbody>
</table>	
</p>		