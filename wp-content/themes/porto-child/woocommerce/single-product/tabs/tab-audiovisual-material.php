<?php
/**
 * Book Audio-visual materials Tab
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product/tabs/tab-syntelestes.php
 *
 * @author 	Spyros Nathanail
 * @package 	WooBookstore/templates
 * @version     1.0
 */
?>
<?php global $product; ?>

    
<p>
<table class="woocommerce-product-attributes shop_attributes table">
    <tbody>
		<?php 

		// Audiovisual material
		$repeater = get_field_object('bk_det_audiovisual_material');
		if ($repeater) {
			echo ('
				<tr> 
				<td class="woocommerce-product-attributes-item__value">
				<div class="book_video_container">'
			);
			
			// Group videos with thumbnails first
			foreach ($repeater['value'] as $video_arr) {
				$video = $video_arr['bk_det_video'];
				$title = $video_arr['bk_det_vid_title'];
				$show_thumbnail = $video_arr['bk_det_show_vid_thumbnail'];
				if (!$show_thumbnail)
					continue;
				
				echo ('<div class="book_video thumbnail"><div class="book_video_src">' . $video . '</div>
					   <div class="book_video_title">' . $title . '</div></div>');
			}
			
			echo ('
						</div>
					</td>
				</tr>
				<tr> 
					<td class="woocommerce-product-attributes-item__value">
						<div class="book_video_container">'
			);
			
			// then just titles
			foreach ($repeater['value'] as $video_arr) {
				$title = $video_arr['bk_det_vid_title'];
				$show_thumbnail = $video_arr['bk_det_show_vid_thumbnail'];
				if ($show_thumbnail)
					continue;
				// use preg_match to find iframe src
				preg_match('/src="(.+?)"/', $video, $matches);
				$src = $matches[1];
				echo ('<div class="book_video nothumbnail"><a href="' . $src . '" target="_new">' . $title . '</a></div>');
			}
			echo ('</div>
				</td>
			</tr>');
		
		}
		?>
	</tbody>
</table>	
</p>

