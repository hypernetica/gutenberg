<?php
/**
 * Book Product Tab
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product/tabs/tab-book.php
 *
 * @author 		WPini
 * @param mixed $book, $book_data
 * @package 	WooBookstore/templates
 * @version     1.0
 */
?>

<?php 
$book_category		= wp_get_post_terms( $book->get_id(), 'book_category', $args );
$book_author		= wp_get_post_terms( $book->get_id(), 'book_author', $args );
$book_publisher		= wp_get_post_terms( $book->get_id(), 'book_publisher', $args );
$book_editor		= wp_get_post_terms( $book->get_id(), 'book_editor', $args );
$book_translator	= wp_get_post_terms( $book->get_id(), 'book_translator', $args );
$book_illustrator	= wp_get_post_terms( $book->get_id(), 'book_illustrator', $args );
?>

<h2><?php _e( 'Book Information', 'woo-bookstore' ) ?></h2>
<p>
<table class="woocommerce-product-attributes shop_attributes table table-striped">
    <tbody>
    <?php foreach ( $book_data as $key => $data ) :?>
        <?php if( isset($data['value']) && !empty( $data['value'] ) ): ?>
        <tr class="<?php echo $data['class'] ?>">
            <th class="woocommerce-product-attributes-item__label"><?php echo $data['title'] ?></th>
            <td class="woocommerce-product-attributes-item__value"><?php echo $data['value'] ?></td>
        </tr>
        <?php endif ?>
	<?php endforeach ?>
    
    <?php
		$separator = ', '; 
		$after = '<td></tr>';
	?>
    <?php 
		$before = '<tr><th>'. _n( 'Category:', 'Categories:', count( $book_category ), 'woo-bookstore' ) .'</th><td>';  
		echo $book->get_categories( $separator, $before, $after ); 
		
		$before = '<tr><th>'. _n( 'Author:', 'Authors:', count( $book_author ), 'woo-bookstore' ) .'</th><td>';  
		echo $book->get_authors( $separator, $before, $after ); 
		
		$before = '<tr><th>'. _n( 'Publisher:', 'Publishers:', count( $book_publisher ), 'woo-bookstore' ) .'</th><td>';  
		echo $book->get_publishers( $separator, $before. ' ', $after ); 
		
	?>
    </tbody>
</table>
</p>

<?php if( woo_bookstore_single_display_author_bio() && ! woo_boostore_display_author_bio_as_tab() ): ?>
	
    <h2><?php _e( 'Author information', 'woo-bookstore' ) ?></h2>
    
    <?php if( ! empty( $book_author ) ): ?>
		<?php foreach( $book_author as $key => $author ): ?>
            <p>
            <?php if( woo_bookstore_single_display_author_thumbnail() ): ?>
                
                <?php 
                
                $thumbnail = woo_bookstore_thumbnail_url( $author->term_id, 'wb_book_author-thumb' );
                
                if ( ! $thumbnail )
                    $thumbnail = wb_woocommerce_placeholder_img_src();
                ?>
                <img src="<?php echo $thumbnail; ?>" class="alignleft" alt="<?php echo $author->name; ?>" />
    
            <?php endif; ?>
            <?php echo wpautop( $author->description ) ?>
            </p>
        <?php endforeach ?>
        
    <?php endif ?>   
    
<?php endif; ?>

<?php if( woo_bookstore_single_display_publisher_desc() && ! woo_boostore_display_publisher_desc_as_tab() ): ?>
	
    <h2><?php _e( 'Publisher information', 'woo-bookstore' ) ?></h2>
    
    <?php if( ! empty( $book_publisher ) ): ?>
    
		<?php foreach( $book_publisher as $key => $publisher ): ?>
            <p>
            <?php if( woo_bookstore_single_display_publisher_thumbnail() ): ?>
                
                <?php 
                
                $thumbnail = woo_bookstore_thumbnail_url( $publisher->term_id, 'wb_book_publisher-thumb' );
                
                if ( ! $thumbnail )
                    $thumbnail = wb_woocommerce_placeholder_img_src();
                ?>
                <img src="<?php echo $thumbnail; ?>" class="alignleft" alt="<?php echo $publisher->name; ?>" />
    
            <?php endif; ?>
            <?php echo wpautop( $publisher->description ) ?>
            </p>
        <?php endforeach ?>
        
    <?php endif ?>   
    
<?php endif; ?>
