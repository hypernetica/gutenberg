<?php
/**
 * Book Details Tab
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product/tabs/tab-syntelestes.php
 *
 * @author 	Spyros Nathanail
 * @package 	WooBookstore/templates
 * @version     1.0
 */
?>
<?php global $product; ?>

    
<p>
<table class="woocommerce-product-attributes shop_attributes table table-striped">
    <tbody>
		<?php foreach ( array(
					'published_date', 
					'internet_dates',
					'isbn', 
					'catalog_code', 
					'eudoxos_code', 
					'selides',
					'series', 
					'tomos', 
					'exofillo', 
					'glossa_prototipou', 
					'titlos_prototipou',
					'contents', 
					'kritikes', 
					'notes', 
					'sxolia', 
					'perilipsi'
					) as $key ) {
			unset ($names);
			$field = get_field_object('bk_det_' . $key); // Try ACF first
			if (empty($field['value'])) {
				continue;
			}
			if ($field['type'] == 'taxonomy') {
				if  (is_array($field['value'])) {
					foreach ( $field['value'] as $term ) {
						if (isset($term['name'])) {
							$names[] = $term['name'];
						} else {
							$names[] = $term;
						}
					}
				} else {
					if (isset($field['value']->name)) {
						$names[] = $field['value']->name;
					} else {
						$names[] = $field['value'];
					}
				}
			} else if ($field['type'] == 'link') {
				$names[] = '<a href="' . $field['value']['url'] . '">' . $field['value']['title'] . '</a>';
			} else if ($field['type'] == 'date_picker') {
				$names[] = $field['value'];				
			} else {
				$names[] = $field['value'];
			}
			echo ('
			<tr> 
				<th class="woocommerce-product-attributes-item__label">' . $field['label'] . '</th>
				<td class="woocommerce-product-attributes-item__value">' . join( ', ', $names ) . '</td>
			</tr>');
		} ?>
		
    	<?php 
		
		// Show dimensions
		$dimensions = wc_format_dimensions($product->get_dimensions(false));
		if ( $product->has_dimensions() ) {
            echo ('
				<tr> 
				<th class="woocommerce-product-attributes-item__label">' . __('Dimensions', 'porto-child') . '</th>
				<td class="woocommerce-product-attributes-item__value">' . $dimensions . '</td>
			</tr>');
        }
		
		// Show weight
		$weight = $product->get_weight();

    	if ( $product->has_weight() ) {
			echo ('
				<tr> 
				<th class="woocommerce-product-attributes-item__label">' . __('Weight', 'porto-child') . '</th>
				<td class="woocommerce-product-attributes-item__value">' . $weight . ' ' . get_option('woocommerce_weight_unit') . '</td>
			</tr>');
    	}
		
		?>
		
		
	</tbody>
</table>	
</p>

