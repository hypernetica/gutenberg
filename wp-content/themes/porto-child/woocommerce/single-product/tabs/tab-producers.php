<?php
/**
 * Book Producers Tab
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product/tabs/tab-syntelestes.php
 *
 * @author 	Spyros Nathanail
 * @package 	WooBookstore/templates
 * @version     1.0
 */
?>
<?php global $product; ?>

    
<p>
<table class="woocommerce-product-attributes shop_attributes table table-striped">
    <tbody>
		
		<?php		// Display author and publisher first
		$book_category		= wp_get_post_terms( $book->get_id(), 'book_category', $args );
		$book_author		= wp_get_post_terms( $book->get_id(), 'book_author', $args );
		$book_publisher		= wp_get_post_terms( $book->get_id(), 'book_publisher', $args );
		$separator = ', '; 
		$after = '<td></tr>';
		$before = '<tr><th>'. _n( 'Category:', 'Categories:', count( $book_category ), 'woo-bookstore' ) .'</th><td>';  
		echo $book->get_categories( $separator, $before, $after ); 
		
		$before = '<tr><th>'. _n( 'Author:', 'Authors:', count( $book_author ), 'woo-bookstore' ) .'</th><td>';  
		echo $book->get_authors( $separator, $before, $after ); 
		
		$before = '<tr><th>'. __( 'Publisher:', 'woo-bookstore' ) .'</th><td>';  
		echo $book->get_publishers( $separator, $before. ' ', $after ); 
		
		?> 
		
		<?php // then the others
			foreach ( array(
					'scientific_editor', 
					'epimeleia', 
					'metafrasi',
					'theorisi',
					'prologos',
					'eisagogi',
					'keimena',
					'epimetro',
					'eikonografisi',
					'exofillo') as $key ) {
			unset ($names);
			$field = get_field_object('bk_synt_' . $key); 
			if (empty($field['value'])) {
				continue;
			}

			if ($field['type'] == 'taxonomy') {
				foreach ( $field['value'] as $term ) {
					if (isset($term->name)) {
						$names[] = $term->name;
					} else {
						$names[] = $term;
					}
				}
			} else if ($field['type'] == 'link') {
				$names[] = '<a href="' . $field['value']['url'] . '">' . $field['value']['title'] . '</a>';
			} else {
				$names[] = $field['value'];
			}
			echo ('
			<tr> 
				<th class="woocommerce-product-attributes-item__label">' . $field['label'] . '</th>
				<td class="woocommerce-product-attributes-item__value">' . join( ', ', $names ) . '</td>
			</tr>');
			} ?>
	</tbody>
</table>	
</p>

