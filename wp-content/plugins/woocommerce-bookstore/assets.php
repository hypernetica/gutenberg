<?php 
//assets
/**
 * WP-ADMIN
 *
 */
add_action( 'admin_enqueue_scripts', 'wwb_assets' );

function wwb_assets( $hook = '' ){
	
	$min = ".min";
	if( WOO_BOOKSTORE_DEBUG )
		$min = "";
	
	wp_register_script( 'woo-bookstore-libjs', plugin_dir_url( __FILE__ ).'assets/js/woo-bookstore-lib'.$min.'.js', array( 'jquery' ), WPINI_WOO_BOOKSTORE_VERSION );
	wp_enqueue_script( 'woo-bookstore-libjs' );	
		
	wp_register_script( 'woo-bookstore-admin', plugin_dir_url( __FILE__ ).'assets/js/admin'.$min.'.js', array( 'jquery', 'jquery-ui-datepicker', 'jquery-ui-autocomplete', 'jquery-ui-sortable' ), WPINI_WOO_BOOKSTORE_VERSION );
	wp_enqueue_script( 'woo-bookstore-admin' );	
	
	wp_localize_script(
		'woo-bookstore-admin', 
		'wwb_script_vars', 
		wwb_javascript_messages()
	);	
	
	wp_register_style( 'woo-bookstore-admin-style', plugin_dir_url( __FILE__ ).'assets/css/woo-bookstore-admin'.$min.'.css', array(), WPINI_WOO_BOOKSTORE_VERSION );
	wp_enqueue_style( 'woo-bookstore-admin-style' );
	
	wp_register_style( 'woo-bookstore-lib', plugin_dir_url( __FILE__ ).'assets/css/woo-bookstore-lib'.$min.'.css', array(), WPINI_WOO_BOOKSTORE_VERSION );
	wp_enqueue_style( 'woo-bookstore-lib' );
	
	/**
	 * Font Awesome
	 *
	 */
	if( preg_match( '/woocommerce_page_woo-bookstore-settings/', $hook ) ){
		
		wp_register_style( 'wb_fontawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' );
		wp_enqueue_style( 'wb_fontawesome' );
		
	}
	
}

add_action( "admin_footer", "test_assets" );
function test_assets(){
	wp_register_style( 
		'wb-jquery-ui-main', plugin_dir_url( __FILE__ ).'assets/css/jquery-ui.min.css', array(), WPINI_WOO_BOOKSTORE_VERSION 
	);
	wp_enqueue_style( 'wb-jquery-ui-main' );
}

/**
 * Front End
 *
 */
add_action( 'wp_enqueue_scripts', 'wwb_frontend_assets' );
function wwb_frontend_assets(){
	
	$min = ".min";
	if( WOO_BOOKSTORE_DEBUG )
		$min = "";
	
	wp_register_script( 
		'woo-bookstore-libjs', 
		plugin_dir_url( __FILE__ ).'assets/js/woo-bookstore-lib'.$min.'.js', 
		array( 'jquery' ), 
		WPINI_WOO_BOOKSTORE_VERSION 
	);
	wp_enqueue_script( 'woo-bookstore-libjs' );	
	
	$use_default_css = get_option( 'woobookstore_use_default_css' );
	if( $use_default_css == 'yes' ){
		wp_register_style( 
			'woo-bookstore-frontend-style', 
			plugin_dir_url( __FILE__ ).'assets/css/front-end'.$min.'.css', 
			array(), 
			WPINI_WOO_BOOKSTORE_VERSION 
		);
		wp_enqueue_style( 'woo-bookstore-frontend-style' );
	}
	
	wp_register_script( 
		'woo-bookstore-js', 
		plugin_dir_url( __FILE__ ).'assets/js/front-end'.$min.'.js', 
		array( 'jquery', 'jquery-ui-autocomplete' ), 
		WPINI_WOO_BOOKSTORE_VERSION 
	);
	wp_enqueue_script( 'woo-bookstore-js' );	
	
	wp_localize_script(
		'woo-bookstore-js', 
		'wb_script_vars', 
		array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'woobookstore_advanced_search_autocomplete_group' => get_option( 'woobookstore_advanced_search_autocomplete_group' )
		)
	);

	$load_jquery_css = get_option( 'woobookstore_load_jquery_ui_css' );
	if( $load_jquery_css == 'yes' )
		woo_bookstore_load_jquery_ui();	
		
	wp_register_style( 
		'woo-bookstore-lib', 
		plugin_dir_url( __FILE__ ).'assets/css/woo-bookstore-lib'.$min.'.css', 
		array(), 
		WPINI_WOO_BOOKSTORE_VERSION 
	);
	wp_enqueue_style( 'woo-bookstore-lib' );
	
}

/**
 * Load jQuery UI Theme
 *
 */
function woo_bookstore_load_jquery_ui(){
	
	wp_register_style( 
		'wb-jquery-ui-main', plugin_dir_url( __FILE__ ).'assets/css/jquery-ui.min.css', array(), WPINI_WOO_BOOKSTORE_VERSION 
	);
	wp_enqueue_style( 'wb-jquery-ui-main' );
	
	wp_register_style( 
		'wb-jquery-ui-structure', plugin_dir_url( __FILE__ ).'assets/css/jquery-ui.structure.min.css', array(), WPINI_WOO_BOOKSTORE_VERSION 
	);
	wp_enqueue_style( 'wb-jquery-ui-structure' );
	
	wp_register_style( 
		'wb-jquery-ui-theme', plugin_dir_url( __FILE__ ).'assets/css/jquery-ui.theme.min.css', array(), WPINI_WOO_BOOKSTORE_VERSION 
	);
	wp_enqueue_style( 'wb-jquery-ui-theme' );
	
}
?>