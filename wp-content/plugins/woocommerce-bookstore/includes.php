<?php 
/**
 * Woocommerce Bookstore include files
 *
 */

/**
 * Plugin Settings
 *
 */
require_once( 'settings/main.php' );

require_once( 'classes/wpini-bookstore-admin.php' );

require_once( 'lib/functions.php' );
require_once( 'lib/ajax.php' );	
require_once( 'lib/ajax-front-end.php' );	
require_once( 'lib/wpml.php' );

require_once( 'woocommerce/main.php' );	