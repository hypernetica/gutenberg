<?php
/**
 * Functions for BookStore Options
 *
 * woo_bookstore_show_book_details_in_tab()
 * woo_bookstore_get_book_details_tab_priority()
 * woo_bookstore_single_display_author_bio()
 * woo_bookstore_single_display_author_thumbnail()
 * woo_boostore_display_author_bio_as_tab()
 * woo_bookstore_get_author_details_tab_priority()
 * woo_bookstore_single_display_publisher_desc()
 * woo_boostore_display_publisher_desc_as_tab()
 * woo_bookstore_get_publisher_description_tab_priority()
 * woo_bookstore_single_display_publisher_thumbnail()
 *
 */

/**
 * Return whether to display book details in a tab or not
 *
 */
function woo_bookstore_show_book_details_in_tab(){
	
	$show = true;
	
	$option = get_option( 'woo_book_show_details_in_tab' );
	
	if( $option != 'yes' )
		$show = false;
	
	return $show;
}

/** 
 * Book Tab Priority
 *
 */
function woo_bookstore_get_book_details_tab_priority(){
	
	$option = get_option( 'woo_book_details_tab_priority' );
	
	if( empty( $option ) )
		$priority = 1;
	else
		$priority = $option;
	
	return $option;
	
}

function get_valid_actions_for_book_details(){
	
	return array(
				'none' => __( 'none', 'woo-bookstore' ),
				'woocommerce_product_meta_start' 	=>	'woocommerce_product_meta_start',
				'woocommerce_product_meta_end'		=>	'woocommerce_product_meta_end',
				'woocommerce_product_thumbnails'	=>	'woocommerce_product_thumbnails',
				'woocommerce_share'					=>	'woocommerce_share'	
			);
	
}

/**
 * Single Book display author
 *
 */
function woo_bookstore_single_display_author_bio(){
	
	$display = true;
	
	$option = get_option( 'woo_book_single_show_author_bio' );
	
	if( $option != 'yes' )
		$display = false;
		
	return $display;
}

function woo_bookstore_single_display_author_thumbnail(){
	
	$display = true;
	
	$option = get_option( 'woo_book_single_show_author_thumb' );
	
	if( $option != 'yes' )
		$display = false;
		
	return $display;
	
}

function woo_boostore_display_author_bio_as_tab(){
	
	$display = false;
	
	$option = get_option( 'woo_book_single_author_bio_display_type' );
	
	if( $option == 'tab' )
		$display = true;
		
	return $display;
	
	
}

function woo_bookstore_get_author_details_tab_priority(){
	
	$option = get_option( 'woo_book_author_tab_priority' );
	
	if( empty( $option ) )
		$priority = 2;
	else
		$priority = $option;
	
	return $option;
	
}

function woo_bookstore_single_display_publisher_desc(){
	
	$display = true;
	
	$option = get_option( 'woo_book_single_show_publisher_description' );
	
	if( $option != 'yes' )
		$display = false;
		
	return $display;
	
}

function woo_boostore_display_publisher_desc_as_tab(){
	
	$display = false;
	
	$option = get_option( 'woo_book_single_publisher_description_display_type' );
	
	if( $option == 'tab' )
		$display = true;
		
	return $display;
	
}

function woo_bookstore_get_publisher_description_tab_priority(){
	
	$option = get_option( 'woo_book_publisher_tab_priority' );
	
	if( empty( $option ) )
		$priority = 3;
	else
		$priority = $option;
	
	return $option;
	
}

function woo_bookstore_single_display_publisher_thumbnail(){
	
	$display = true;
	
	$option = get_option( 'woo_book_single_show_publisher_thumb' );
	
	if( $option != 'yes' )
		$display = false;
		
	return $display;
	
}