<?php
/**
 * Custom Fields for product type BOOK
 *
 * TABLE OF CONTENTS
 *
 * woo_book_default_fields()
 * wpini_get_custom_fields_admin_html()
 * woo_book_get_unique_fields()
 *
 */

function woo_book_get_custom_fields(){
	
	$custom_fields = maybe_unserialize( get_option( 'wpini_woo_book_custom_fields' ) );
	if( empty( $custom_fields ) )
		$custom_fields = woo_book_default_fields();
	
	return $custom_fields;
	
	
}

function woo_book_default_fields(){
	
	return apply_filters( 'woo_bookstore_default_fields', array(
							array(
								'name'			=> 'ISBN',
								'field_type'	=> 'text',
								'unique'		=> 'yes',
								'meta_key'		=> '_wwcp_isbn',
								'length'		=> '10',
								'class'			=> '',
								'admin_class'	=> 'regular-text',
								'visible'		=> 'yes'	
							),
							
							array(
								'name'			=> 'ISBN13',
								'field_type'	=> 'text',
								'unique'		=> 'no',
								'meta_key'		=> '_wwcp_isbn13',
								'length'		=> '13',
								'class'			=> '',
								'admin_class'	=> 'regular-text',
								'visible'		=> 'yes'							
							),
							
							array(
								'name'			=>  'Number of pages',
								'field_type'	=> 'number',
								'unique'		=> 'no',
								'meta_key'		=> '_wwcp_page_num',
								'length'		=> '*',
								'class'			=> '',
								'admin_class'	=> 'small-text'	,
								'visible'		=> 'yes'						
							),
							
							array(
								'name'			=>  'Original Title',
								'field_type'	=> 'text',
								'unique'		=> 'no',
								'meta_key'		=> '_wwcp_original_title',
								'length'		=> '*',
								'class'			=> '',
								'admin_class'	=> 'regular-text',
								'visible'		=> 'yes'							
							),
							
							array(
								'name'			=>	'Published Date',
								'field_type'	=> 'date',
								'unique'		=> 'no',
								'meta_key'		=> '_wwcp_published_date',
								'length'		=> '*',
								'class'			=> '',
								'admin_class'	=> 'regular-text',
								'visible'		=> 'yes'							
							),
							
							array(
								'name'			=> 'Translators',
								'field_type'	=> 'textarea',
								'unique'		=> 'no',
								'meta_key'		=> '_wwcp_translated_by',
								'length'		=> '*',
								'class'			=> '',
								'admin_class'	=> '',
								'visible'		=> 'yes'							
							)							
							
							
						) );
	
}

function wpini_get_custom_fields_admin_html( $book ){
	
	$custom_fields = maybe_unserialize( get_option( 'wpini_woo_book_custom_fields' ) );
	
	if( empty( $custom_fields ) )
		$custom_fields = woo_book_default_fields();
	
	ob_start();	
	?>
    
    <?php if( !empty( $custom_fields ) ): ?>
        <div class="options_group">
			<?php foreach( $custom_fields as $field ): ?>        	
            
            <?php                 
                $current_value = isset( $book->{$field['meta_key']} ) ? $book->{$field['meta_key']} : '';
                $type = 'text';
                
                if( $field['field_type'] == 'textarea' )
                    $type = 'textarea';
                /*
                if( $field['field_type'] == 'textarea' )
                    $type = 'number';
				*/
            ?>
                
                <p class="form-field custom_field_type">
                    <label for="<?php echo $field['meta_key'] ?>"><?php echo __( $field['name'], 'woo-bookstore' ) ?></label>
                    <?php if( $type != 'textarea' ) : ?>
                    
                    <input class="short" maxlength="<?php echo $field['length']?>" type="<?php echo $type ?>" value="<?php echo $current_value ?>" name="book[<?php echo $field['meta_key'] ?>]" id="<?php echo $field['meta_key'] ?>" />
                    <?php else: ?>
                    <textarea style="width:100%" name="book[<?php echo $field['meta_key'] ?>]" id="<?php echo $field['meta_key'] ?>"><?php echo $current_value ?></textarea>
                    <?php endif ?>   
                    
                </p>
            <?php endforeach ?>
        </div> 
    <?php endif ?>
    
	<?php
	echo ob_get_clean();
}

function woo_book_get_unique_fields(){
	
	$custom_fields = woo_book_get_custom_fields();
	$unique_fields = array();
	
	if( ! empty( $custom_fields ) ){
		foreach( $custom_fields as $key => $field ){
			if( $field['unique'] == 'yes' )
				$unique_fields[] = $field['meta_key'];
		}
	}
	
	return $unique_fields;
}