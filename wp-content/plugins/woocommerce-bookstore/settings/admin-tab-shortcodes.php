<h3><?php _e( 'Woocommerce Bookstore Shortcodes', 'woo-bookstore' ) ?></h3>
<p>
	<?php _e( 'Below you can see all available shortcodes of the plugin. Every shortcode has a template path. Override it by copying file to your theme folder.', 'woo-bookstore' ) ?>
</p>
		
<?php Woo_Bookstore_Shortcodes::show_shortcode_information_table() ?>