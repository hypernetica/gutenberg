<?php
/**
 * WooCommerce Settings Admin Tab
 *
 ***/
 
require_once( 'woocommerce-admin-tab-functions.php' );

add_filter( 'woocommerce_settings_tabs_array', 'woo_bookstore_add_tab_woocommerce', 30);
add_action( 'woocommerce_update_options_woo_bookstore_st', 'woo_bookstore_options' );
add_action( 'woocommerce_settings_tabs_woo_bookstore_st', 'woo_bookstore_plugin_options' );

function woo_bookstore_add_tab_woocommerce( $tabs ){
	
	$tabs['woo_bookstore_st'] = __( 'Bookstore', 'woo-bookstore' );
	return $tabs;
	
}


function woo_bookstore_options(){
	
	$book_options = woo_bookstore_get_options();
	
	foreach ( $book_options as $option ) {
		woocommerce_update_options( $option );
	}
		
}

function woo_bookstore_plugin_options(){
	
	?>
    <div class="subsubsub_section">
        
        <br class="clear" />
        
        <?php $book_options = woo_bookstore_get_options(); ?>
        <?php foreach ( $book_options as $id => $tab ) : ?>
            <!-- tab #<?php echo $id ?> -->
            <div class="section" id="woo_bookstore_options_<?php echo $id ?>">
                <?php woocommerce_admin_fields( $book_options[$id] ) ?>
            </div>
        <?php endforeach ?>		
       	
    </div>	
	<?php
	
}