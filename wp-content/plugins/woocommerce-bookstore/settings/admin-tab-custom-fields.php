<?php
/**
 * Admin tab where user is able to edit book's custom fields
 *
 */
?>

<?php $custom_fields = woo_book_get_custom_fields(); ?>

<div id="woo_bookstore_custom_fields_messages" class="wb-admin-messages"></div>

<form id="woo-bookstore-edit-custom-fields" class="woo-bookstore-admin-form" action="<?php echo WPINI_WOO_BOOKSTORE_SETTINGS_URL ?>" method="POST" enctype="multipart/form-data">
        
	<?php $nonce = wp_create_nonce( 'woo_book_admin_settings_field' ) ?>
    <input type="hidden" name="woo_book_admin_nonce" id="woo_book_admin_nonce" value="<?php echo $nonce ?>"  />
    
    <h3><?php  _e( 'Book Custom Fields', 'woo-bookstore' ) ?></h3>
    <p><?php _e( "From this screen you can edit Book' s Custom Fields. Drag and Drop for priority.", 'woo-bookstore' ) ?></p>
    
	<table id="book_custom_fields" class="wb_admin_table">
        <thead>
            <th><?php _e( 'Name', 'woo-bookstore' ) ?></th>
            <th><?php _e( 'Type', 'woo-bookstore' ) ?></th>
            <th><?php _e( 'Unique', 'woo-bookstore' ) ?></th>
            <th><?php _e( 'Meta Key', 'woo-bookstore' ) ?></th>
            <th><?php _e( 'Length', 'woo-bookstore' ) ?></th>
            <th><?php _e( 'Class', 'woo-bookstore' ) ?></th>
            <th><?php _e( 'WP Style', 'woo-bookstore' ) ?></th>
            <th><?php _e( 'Visible', 'woo-bookstore' ) ?></th>
            <th><?php _e( 'Sort', 'woo-bookstore' ) ?></th>
            <th><a href="#" class="add_btn"><i class="fa fa-plus-square fa-2x"></i></a></td>
        </thead>
        
        <?php $count = 0 ?>
        
        <tbody id="book_custom_fields_sortable">
            <?php foreach( $custom_fields as $field ) : ?>
            <tr class="book_custom_field_row" id="custom_field_row_<?php echo $count ?>">
                <td>
                    <input class="wb_text_field" type="text" value="<?php echo apply_filters( 'woo_bookstore_custom_field_name', $field['name'], 'custom-field' ) ?>" name="book_fields[<?php echo $count ?>][name]" id="" />
                </td>
                <td class="wb_aligncenter">
                    <select name="book_fields[<?php echo $count ?>][field_type]">
                        <option value="text">Text</option>
                        <option <?php if( $field['field_type'] == 'number' ): ?>selected="selected"<?php endif ?> value="number"><?php _e( 'Number', 'woo-bookstore' ) ?></option>
                        <option <?php if( $field['field_type'] == 'date' ): ?>selected="selected"<?php endif ?> value="date"><?php _e( 'Date' , 'woo-bookstore' ) ?></option>  
                        <option <?php if( $field['field_type'] == 'textarea' ): ?>selected="selected"<?php endif ?> value="textarea"><?php _e( 'Textarea' , 'woo-bookstore' ) ?></option>                                           
                    </select>
                </td>
                <td class="wb_aligncenter">
                    <select name="book_fields[<?php echo $count ?>][unique]">
                        <option value="no"><?php _e( 'No', 'woo-bookstore' ) ?></option>
                        <option <?php if( $field['unique'] == 'yes' ): ?>selected="selected"<?php endif ?> value="yes"><?php _e( 'Yes', 'woo-bookstore' ) ?></option>                                            
                    </select>
                </td>
                <td>
                    <input class="wb_text_field wb-required wb_admin_custom_field_name" type="text" value="<?php echo $field['meta_key'] ?>" name="book_fields[<?php echo $count ?>][meta_key]" id="" data-line="<?php echo $count ?>" />
                </td>
                <td class="wb_aligncenter">
                    <input type="text" class="small-text" value="<?php echo ( empty( $field['length'] )? "*" : $field['length'] ) ?>" name="book_fields[<?php echo $count ?>][length]" id="" />
                </td>
                <td>
                    <input class="wb_text_field" type="text" value="<?php echo $field['class'] ?>" name="book_fields[<?php echo $count ?>][class]" id="" />
                </td>
                <td class="wb_aligncenter">
                    <select name="book_fields[<?php echo $count ?>][admin_class]">
                        <option value="small-text"><?php _e( 'Short', 'woo-bookstore' ) ?></option>                                    
                        <option <?php if( $field['admin_class'] == 'regular-text' ): ?>selected="selected"<?php endif ?> value="regular-text"><?php _e( 'Regular', 'woo-bookstore' ) ?></option>                                            
                    </select>                                    	
                </td>
                <td class="wb_aligncenter">
                    <input type="checkbox" <?php if( isset( $field['visible'] ) &&$field['visible'] == 'yes' ): ?>checked="checked"<?php endif ?> value="yes" name="book_fields[<?php echo $count ?>][visible]" id="" />
                </td>
                <td class="sort_handler wb_aligncenter">
                    <span class="sort_btn"><i class="fa fa-bars fa-lg"></i></span>
                </td>                                    
                <td class="wb_aligncenter">
                    <a href="#" rel="custom_field_row_<?php echo $count ?>" class="remove_btn"><i class="fa fa-times-circle fa-lg"></i></a>
                </td>
            </tr>
            <?php $count++; endforeach ?>
        </tbody>
    </table>                  	
                
    <p class="submit">  
        <input type="submit" id="save_woo_book_options" class="button button-primary" name="Submit" value="<?php  _e( 'Save Options', 'woo-bookstore' )  ?>" />
        <a href="<?php echo WPINI_WOO_BOOKSTORE_SETTINGS_URL ?>&bookstore_nonce=<?php echo $nonce ?>&restore=true" class="button primary-button woo_bookstore_restore"><?php _e( 'Restore to Defaults', 'woo-bookstore' ) ?></a>  
    </p>                   
</form>
<p class="description">
    <ul>
        <li><strong><?php _e( 'Meta Key', 'woo-bookstore'  ) ?>:</strong>&nbsp;<?php _e( 'The name of the product meta.', 'woo-bookstore' ) ?></li>
        <li><strong><?php _e( 'Class', 'woo-bookstore'  ) ?>:</strong>&nbsp;<?php _e( "Enter input's class for the front end.", 'woo-bookstore' ) ?></li>
        <li><strong><?php _e( 'WP Class', 'woo-bookstore'  ) ?>:</strong>&nbsp;<?php _e( 'For edit produt screen only. Define the class you would like for your input.', 'woo-bookstore' ) ?></li>
    </ul>
</p>