<form action="<?php echo WPINI_WOO_BOOKSTORE_SETTINGS_URL ?>&tab=advanced-search" method="POST" enctype="multipart/form-data">
	
    <?php 
		$advanced_search	= new Woo_Bookstore_Advanced_Search;
		$options 			= $advanced_search->get_advanced_search_options();		
	?>
    
    <?php $nonce = wp_create_nonce( 'woo_book_admin_advanced_search' ) ?>
    <input type="hidden" name="wb_admin_nonce" id="wb_admin_nonce" value="<?php echo $nonce ?>"  />
    
    <h3><?php _e( 'Advanced Book Search', 'woo-bookstore' ) ?></h3>
    <p>
		<?php _e( "From this screen you are able to edit which book fields you would like to be searchable.", 'woo-bookstore' ) ?>
    </p>
    
    
    <table class="wb_admin_table" id="">
        <thead>
            <th><?php _e( 'Enable', 'woo-bookstore' ) ?></th>
            <th><?php _e( 'Name', 'woo-bookstore' ) ?></th>
            <th style="width:30%"><?php _e( 'Label', 'woo-bookstore' ) ?></th>
            <th><?php _e( 'Type', 'woo-bookstore' ) ?></th>
            <th><?php _e( 'Class', 'woo-bookstore' ) ?></th>
            <th><?php _e( 'Autocomplete', 'woo-bookstore' ) ?></th>
            <th><?php _e( 'Sort', 'woo-bookstore' ) ?></th>
        </thead>
        
        <tbody id="book_advanced_search_sortable">
           
            <?php if( ! empty( $options ) ) : $count = 0;?>
                
				<?php foreach( $options as $key => $values ): $count++;?>
				<tr>
                	<td class="wb_aligncenter field_type_container_<?php echo $count ?>">
                    	<input name="fields[<?php echo $key ?>][enabled]" type="checkbox" value="1"<?php if( isset( $values['enabled'] ) && $values['enabled'] == 1 ): ?>CHECKED="CHECKED"<?php endif ?> data-row="<?php echo $count ?>" data-type="<?php echo $values['type'] ?>" data-key=<?php echo $key ?> />                        
                    </td>
                    <td><input class="wb_text_field" readonly="readonly" type="text" name="fields[<?php echo $key ?>][name]" value="<?php _e( $values['name'], 'woo-bookstore' ) ?>" data-row="<?php echo $count ?>" data-key=<?php echo $key ?> /></td>
                    <td>
                    	<input class="wb_text_field" type="text" name="fields[<?php echo $key ?>][label]" value="<?php echo $values['label'] ?>" data-row="<?php echo $count ?>" data-key=<?php echo $key ?> />
                    </td>
                    <td class="wb_aligncenter display_type_dropdown_container_<?php echo $count ?>">
                    	<?php $advanced_search->dropdown_field_types( 'fields['.$key.'][display_type]', $values['type'], $values['display_type'], "display_type_dropdown" ); ?>
                    </td>
                    <td>
                    	<input class="wb_text_field" type="text" name="fields[<?php echo $key ?>][class]" id="" value="<?php echo $values['class'] ?>" class="" data-row="<?php echo $count ?>" data-key=<?php echo $key ?> />
                    </td>
                    <td class="wb_aligncenter">
                    	<input name="fields[<?php echo $key ?>][autocomplete]" type="checkbox" value="1"<?php if( isset( $values['autocomplete'] ) && $values['autocomplete'] == 1 ): ?>CHECKED="CHECKED"<?php endif ?> class="wb-admin-autocomplete-check" data-row="<?php echo $count ?>" data-key=<?php echo $key ?> />                        
                    </td>
                    <td class="sort_handler wb_aligncenter">
                        <span class="sort_btn"><i class="fa fa-bars fa-lg"></i></span>
                    </td>
                    <input type="hidden" name="fields[<?php echo $key ?>][type]" value="<?php echo $values['type'] ?>" data-row="<?php echo $count ?>" data-key=<?php echo $key ?> />
                </tr>               	                 
                <?php endforeach ?>
                
            <?php endif?>   
        
        </tbody>
        
    </table>
				
    <p class="submit">  
        <input type="submit" id="save_advanced_search_options" class="button button-primary" name="Submit" value="<?php  _e( 'Save Options', 'woo-bookstore' )  ?>" />
        <a href="<?php echo WPINI_WOO_BOOKSTORE_SETTINGS_URL ?>&tab=advanced-search&reset_advanced_nonce=<?php echo $nonce ?>&restore=true" class="woo_bookstore_restore button primary-button"><?php _e( 'Restore to Defaults', 'woo-bookstore' ) ?></a>
    </p>     
            
</form>

<script>
jQuery( document ).ready(function(e) {
    woo_bookstore_admin_init_autocomplete_form();
});
</script>