<?php
/**
 * WooCommerce BookStore Settings
 *
 * TABLE OF CONTENTS
 *
 * woo_bookstore_settings()
 * save_woo_bookstore_settings()
 *
 */

//require some files for the settings
require_once( 'options-functions.php' );
require_once( 'custom_fields.php' );
require_once( 'woocommerce-admin-tab.php' );


function woo_bookstore_settings(){
	
	if (!current_user_can('manage_woocommerce'))  {
		wp_die( 'No permissions to view this page.', 'woo-bookstore' );
	}
	?>
    <div class="wrap">
	  	
        <h2><?php    _e( 'WooCommerce Bookstore Settings', 'woo-bookstore' ) ?></h2>	
    
		<?php $current_tab = ( isset( $_GET['tab'] ) ? $_GET['tab'] : 'custom-fields'  ) ?>
        
        <?php wwb_print_messages() ?>
        
        <?php
        
		$tabs = array( 
                    'custom-fields'		=> 	__( 'Custom Fields', 'woo-bookstore' ), 
					'advanced-search'	=> 	__( 'Advanced Search', 'woo-bookstore' ), 
                    'shortcodes' 		=>	__( 'Shortcodes', 'woo-bookstore' )
        );
        
        echo '<div id="icon-themes" class="icon32"><br></div>';
        echo '<h2 class="nav-tab-wrapper">';
        
        foreach( $tabs as $tab => $name ){
            
            $class = ( $tab == $current_tab ) ? ' nav-tab-active' : '';
            echo "<a class='nav-tab$class' href='".WPINI_WOO_BOOKSTORE_SETTINGS_URL."&tab=$tab'>$name</a>";
    
        }
        
        echo '</h2>';
        
        $file = WPINI_WOO_BOOKSTORE_DIR.'settings/admin-tab-'.$current_tab.'.php';
        if( file_exists( $file ) )
            require_once( 'admin-tab-'.$current_tab.'.php' ); 
        else
            require_once( 'admin-tab-custom-fields.php' ); 
		?>
        </div>
		<?php
}

add_action( 'admin_init', 'save_woo_bookstore_settings' );
function save_woo_bookstore_settings(){
	
	//book custom fields
	if( isset( $_POST['woo_book_admin_nonce'] ) ){
		
		check_admin_referer( 'woo_book_admin_settings_field', 'woo_book_admin_nonce' );
		$fields = apply_filters( 'woo_bookstore_save_custom_fields', $_POST['book_fields'], 'book-field' );
		add_option( 'wpini_woo_book_custom_fields', serialize( $fields ), '', true ) or update_option( 'wpini_woo_book_custom_fields', serialize( $fields ) );
		wp_redirect( WPINI_WOO_BOOKSTORE_SETTINGS_URL.'&message=settings_updated' );
		
	}
	
	if( isset( $_GET['bookstore_nonce'] ) ){
		
		check_admin_referer( 'woo_book_admin_settings_field', 'bookstore_nonce' );
		delete_option( 'wpini_woo_book_custom_fields' );
		wp_redirect( WPINI_WOO_BOOKSTORE_SETTINGS_URL.'&message=settings_updated' );
	
	}
	
	//advanced search
	if( isset( $_POST['wb_admin_nonce'] ) ){
		
		check_admin_referer( 'woo_book_admin_advanced_search', 'wb_admin_nonce' );
		
		if( ! empty( $_POST['fields'] ) ){
			
			//save search fields
			$fields = apply_filters( 'woo_bookstore_save_custom_fields', $_POST['fields'], 'advanced-search-field' );
			add_option( 'woo_bookstore_advanced_search', serialize( $fields ), '', true ) or 
			update_option( 'woo_bookstore_advanced_search', serialize( $fields ) );
			
			//save search fields types 
			$field_types = array();
			foreach( $_POST['fields'] as $field => $values ){
				$field_types[$field] = $values['type'];
			}
			
			//save search fields
			add_option( 'woo_bookstore_advanced_search_field_types', serialize( $field_types ), '', true ) or 
			update_option( 'woo_bookstore_advanced_search_field_types', serialize( $field_types ) );
			
			wp_redirect( WPINI_WOO_BOOKSTORE_SETTINGS_URL.'&tab=advanced-search&message=settings_updated' );
		}
	}
	
	if( isset( $_GET['reset_advanced_nonce'] ) ){
		
		check_admin_referer( 'woo_book_admin_advanced_search', 'reset_advanced_nonce' );
		delete_option( 'woo_bookstore_advanced_search' );
		delete_option( 'woo_bookstore_advanced_search_field_types' );
		wp_redirect( WPINI_WOO_BOOKSTORE_SETTINGS_URL.'&tab=advanced-search&message=settings_updated' );
		
	}
}