<?php
/**
 * WooCommerce Admin Tab Functions
 *
 */

function woo_bookstore_get_options( $option_name = array()  ){
	
	global $woocommerce;

	$is_woocommerce_2_0 = version_compare( preg_replace( '/-beta-([0-9]+)/', '', $woocommerce->version ), '2.1', '<' );

	$options['general_settings'] = array();

	if( $is_woocommerce_2_0 ){

		$settings_page = 'WooCommerce &gt; Settings &gt; Pages' ;
		
	}else{
		
		$settings_page = 'in this settings page';
		
	}
	
	$settings = array(

		array( 
			'name' => __( 'Book Single Page', 'woo-bookstore' ), 
			'type' => 'title', 
			'desc' => __( 'Settings for single book page.', 'woo-bookstore' ), 
			'id' => 'woo_bookstore_general_settings' 
		),
		
		array(
			'name'    	=> __( 'Book Details Tab', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Enable this option if you want to display Book Details in a product tab.", 'woo-bookstore' ),
			'id'      	=> 'woo_book_show_details_in_tab',
			'std'     	=> 'yes', 
			'default' 	=> 'yes', 
			'type'    	=> 'checkbox',
			'desc_tip'	=> ''
		),
		
		array(
			'name'    	=> __( 'Tab Priority', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Enter tab's priority for Book Details.", 'woo-bookstore' ),
			'id'      	=> 'woo_book_details_tab_priority',
			'std'     	=> '1', 
			'default' 	=> '1', 
			'type'    	=> 'number',
			'class'		=> 'small-text woo_book_show_details_in_tab',
			'desc_tip'	=> __( 'You should enable Book Details Tab above in order to use this option.', 'woo-bookstore' )
		),
		
		array(
			'name'    	=> __( 'Book Details', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( 'None means that no book details will be displayed. For developers: use function woo_bookstore_display_book_details( ID ) to display book details.', 'woo-bookstore' ),
			'id'      	=> 'woo_book_details_action',
			'std'     	=> '1', 
			'default' 	=> '', 
			'type'    	=> 'select',
			'options'	=> get_valid_actions_for_book_details(),
			'desc_tip'	=> __( "In case you do not want to use tabs enter where you would like to display book details. Note: this option will work only if you disable book details tab.", 'woo-bookstore' )
		),
		
		array( 
			'type' => 'sectionend', 
			'id' => 'woo_bookstore_general_settings' 
		),
		
		array( 
			'name' => __( 'Author Settings', 'woo-bookstore' ), 
			'type' => 'title', 
			'desc' => __( 'Setting for the authors.', 'woo-bookstore' ), 
			'id' => 'woo_bookstore_author_settings' 
		),
		
		array(
			'name'    	=> __( 'Show Author Bio', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Enable this option if you want to display book's author bio in single product page.", 'woo-bookstore' ),
			'id'      	=> 'woo_book_single_show_author_bio',
			'std'     	=> 'yes', 
			'default' 	=> 'yes', 
			'type'    	=> 'checkbox',
			'desc_tip'	=> ''
		),
		
		array(
			'name'    	=> __( 'Display Type', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Select how you would like Author's bio to be displayed.", 'woo-bookstore' ),
			'id'      	=> 'woo_book_single_author_bio_display_type',
			'std'     	=> 'yes', 
			'default' 	=> 'yes', 
			'type'    	=> 'select',
			'desc_tip'	=> '',
			'options'	=> array(
								'after_desc'	=> __( "After books's description.", 'woo-bookstore' ),
								'tab'			=> __( "Separate Product Tab", 'woo-bookstore' )
							)
		),
		
		array(
			'name'    	=> __( 'Tab Priority', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Enter tab's priority.", 'woo-bookstore' ),
			'id'      	=> 'woo_book_author_tab_priority',
			'std'     	=> '1', 
			'default' 	=> '2', 
			'type'    	=> 'number',
			'class'		=> 'small-text',
			'desc_tip'	=> __( 'You should select Separate Product Tab in order to use this option.', 'woo-bookstore' )
		),
		
		array(
			'name'    	=> __( 'Show Author Thumbnail', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Enable this option if you want to display book's author thumbnail in single product page.", 'woo-bookstore' ),
			'id'      	=> 'woo_book_single_show_author_thumb',
			'std'     	=> 'yes', 
			'default' 	=> 'yes', 
			'type'    	=> 'checkbox',
			'desc_tip'	=> ''
		),
		
		array( 
			'type' => 'sectionend', 
			'id' => 'woo_bookstore_author_settings' 
		),		
		
		array( 
			'name' => __( 'Publisher Settings', 'woo-bookstore' ), 
			'type' => 'title', 
			'desc' => __( 'Settings for publishers', 'woo-bookstore' ), 
			'id' => 'woo_bookstore_publisher_settings' 
		),
		
		array(
			'name'    	=> __( 'Show Publisher Description', 'woo-bookstore' ),
			'desc'    	=> "",
			'id'      	=> 'woo_book_single_show_publisher_description',
			'std'     	=> 'yes', 
			'default' 	=> 'yes', 
			'type'    	=> 'checkbox',
			'desc_tip'	=> ''
		),
		
		array(
			'name'    	=> __( 'Display Type', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Select how you would like Publisher's description to be displayed.", 'woo-bookstore' ),
			'id'      	=> 'woo_book_single_publisher_description_display_type',
			'std'     	=> 'yes', 
			'default' 	=> 'yes', 
			'type'    	=> 'select',
			'desc_tip'	=> '',
			'options'	=> array(
								'after_desc'	=> __( "After books's description.", 'woo-bookstore' ),
								'tab'			=> __( "Separate Product Tab", 'woo-bookstore' )
							)
		),
		
		array(
			'name'    	=> __( 'Tab Priority', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Enter tab's priority.", 'woo-bookstore' ),
			'id'      	=> 'woo_book_publisher_tab_priority',
			'std'     	=> '1', 
			'default' 	=> '3', 
			'type'    	=> 'number',
			'class'		=> 'small-text',
			'desc_tip'	=> __( 'You should select Separate Product Tab in order to use this option.', 'woo-bookstore' )
		),
		
		array(
			'name'    	=> __( 'Show Publisher Thumbnail', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Enable this option if you want to display publisher's author thumbnail in single product page.", 'woo-bookstore' ),
			'id'      	=> 'woo_book_single_show_publisher_thumb',
			'std'     	=> 'yes', 
			'default' 	=> 'yes', 
			'type'    	=> 'checkbox',
			'desc_tip'	=> ''
		),
		
		array( 
			'type' => 'sectionend', 
			'id' => 'woo_bookstore_publisher_settings' 
		),
		
		/**
		 * Permalinks
		 *
		 */
		array( 
			'name' 	=> __( 'Permalinks', 'woo-bookstore' ), 
			'type' 	=> 'title', 
			'desc' => __( 'Permalinks settings. Remember to resave permalinks after changing permalinks.', 'woo-bookstore' ), 
			'id' 	=> 'woo_bookstore_permalinks' 
		),
		
		array(
			'name'    	=> __( 'Book Category', 'woo-bookstore' ),
			'desc'    	=> '',
			'id'      	=> 'woo_book_category_permalink',
			'std'     	=> '1', 
			'default' 	=> 'book_category', 
			'type'    	=> 'text',
			'class'		=> 'input-text regular-input',
			'desc_tip'	=> ''
		),
		
		array(
			'name'    	=> __( 'Book Author', 'woo-bookstore' ),
			'desc'    	=> '',
			'id'      	=> 'woo_book_author_permalink',
			'std'     	=> '1', 
			'default' 	=> 'book_author', 
			'type'    	=> 'text',
			'class'		=> 'input-text regular-input',
			'desc_tip'	=> ''
		),
		
		
		array(
			'name'    	=> __( 'Book Publisher', 'woo-bookstore' ),
			'desc'    	=> '',
			'id'      	=> 'woo_book_publisher_permalink',
			'std'     	=> '1', 
			'default' 	=> 'book_publisher', 
			'type'    	=> 'text',
			'class'		=> 'input-text regular-input',
			'desc_tip'	=> ''
		),
		
		array( 
			'type' => 'sectionend', 
			'id' => 'woo_bookstore_permalinks'
		),
		
		/**
		 * Bookstore Pages
		 *
		 */
		array( 
			'name' => __( 'Bookstore Pages', 'woo-bookstore' ), 
			'type' => 'title', 
			'desc' => __( 'Bookstore basic pages. Enter here the basic pages of the bookstore that are going to be used by the plugin.', 'woo-bookstore' ), 
			'id' => 'woo_bookstore_pages' 
		),
		
		array(
			'name'    	=> __( 'Bookstore', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Book Archive page. Make sure you've inserted [woo_bookstore_catalog] shortcode in the selected page", 'woo-bookstore' ),
			'id'      	=> 'bookstore_archive_page_id',
			'std'     	=> 'yes', 
			'default' 	=> 'yes', 
			'type'    	=> 'select',
			'desc_tip'	=> "",
			'options'	=> woo_bookstore_get_pages()
		),
		
		array(
			'name'    	=> __( 'Advanced search', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Make sure you've inserted [woo_bookstore_advanced_search] shortcode in the selected page", 'woo-bookstore' ),
			'id'      	=> 'bookstore_adv_search_page_id',
			'std'     	=> 'yes', 
			'default' 	=> 'yes', 
			'type'    	=> 'select',
			'desc_tip'	=> "",
			'options'	=> woo_bookstore_get_pages()
		),
		
		array(
			'name'    	=> __( 'Search everywhere', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Check this option if do not want to user Advanced Search Page with shortcode. If you check this option please make sure that you haven't select Advanced Search Page above. If you really want to use this option we recommend you to set default WooCommerce shop as Advanced Search Page to avoid conflicts.", 'woo-bookstore' ),
			'id'      	=> 'woobookstore_search_everywhere',
			'std'     	=> 'no', 
			'default' 	=> 'no', 
			'type'    	=> 'checkbox',
			'desc_tip'	=> ''
		),
		
		array( 
			'type' => 'sectionend', 
			'id' => 'woo_bookstore_pages' 
		),
		
		/**
		 * Book's taxonomies image dimensions
		 *
		 */		
		array( 
			'name' => __( 'Images | Settings', 'woo-bookstore' ), 
			'type' => 'title', 
			'desc' => __( "Settings for bookstore's images. There are three (3) images being used by plugin. Author, Book Category and Publisher.", 'woo-bookstore' ), 
			'id' => 'woo_bookstore_image_settings' 
		),
		
		array(
			'name'    	=> __( 'Book author image', 'woo-bookstore' ),
			'desc'    	=> "",
			'id'      	=> 'wb_book_author-thumb_image_size',
			'default'  => array(
				'width'  => 400,
				'height' => 600,
				'crop'   => false
			),
			'std'      => array(
				'width'  => 400,
				'height' => 600,
				'crop'   => false
			),
			'type'    	=> 'woo_bookstore_image_width',
			'desc_tip'	=> ''
			
		),
		
		array(
			'name'    	=> __( 'Book category image', 'woo-bookstore' ),
			'desc'    	=> "",
			'id'      	=> 'wb_book_category-thumb_image_size',
			'default'  => array(
				'width'  => 400,
				'height' => 600,
				'crop'   => false
			),
			'std'      => array(
				'width'  => 400,
				'height' => 600,
				'crop'   => false
			),
			'type'    	=> 'woo_bookstore_image_width',
			'desc_tip'	=> ''
			
		),
		
		array(
			'name'    	=> __( 'Book publisher image', 'woo-bookstore' ),
			'desc'    	=> "",
			'id'      	=> 'wb_book_publisher-thumb_image_size',
			'default'  => array(
				'width'  => 400,
				'height' => 600,
				'crop'   => false
			),
			'std'      => array(
				'width'  => 400,
				'height' => 600,
				'crop'   => false
			),
			'type'    	=> 'woo_bookstore_image_width',
			'desc_tip'	=> ''
			
		),
		
		array( 
			'type' => 'sectionend', 
			'id' => 'woo_bookstore_image_settings' 
		),
				
		/**
		 * Book's taxonomies styling settings
		 *
		 */		
		array( 
			'name' => __( 'Styling | Settings', 'woo-bookstore' ), 
			'type' => 'title', 
			'desc' => __( 'Some basic styling settings for Bookstore.', 'woo-bookstore' ), 
			'id' => 'woo_bookstore_styling_settings' 
		),
		
		array(
			'name'    	=> __( 'Use default css', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Enable this option if you want to use default bookstore css style.", 'woo-bookstore' ),
			'id'      	=> 'woobookstore_use_default_css',
			'std'     	=> 'no', 
			'default' 	=> 'no', 
			'type'    	=> 'checkbox',
			'desc_tip'	=> __( "Uncheck this if you want to user your own style.", 'woo-bookstore' )
		),
		
		array(
			'name'    	=> __( 'jQuery UI css', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Enable this option if you want to use jQuery Flick Theme for autocomplete.", 'woo-bookstore' ),
			'id'      	=> 'woobookstore_load_jquery_ui_css',
			'std'     	=> 'no', 
			'default' 	=> 'no', 
			'type'    	=> 'checkbox',
			'desc_tip'	=> __( "Uncheck this if you want to user your own style.", 'woo-bookstore' )
		),
		
		array( 
			'type' => 'sectionend', 
			'id' => 'woo_bookstore_styling_settings' 
		),
		
		/**
		 * Advanced Search
		 *
		 */
		array( 
			'name' => __( 'Advanced search', 'woo-bookstore' ), 
			'type' => 'title', 
			'desc' => __( 'Settings for advanced search page.', 'woo-bookstore' ), 
			'id' => 'woo_bookstore_as_settings' 
		),
		
		array(
			'name'    	=> __( 'Group autocomplete results', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Enable this option if you want to use default bookstore css style.", 'woo-bookstore' ),
			'id'      	=> 'woobookstore_advanced_search_autocomplete_group',
			'std'     	=> 'no', 
			'default' 	=> 'no', 
			'type'    	=> 'checkbox',
			'desc_tip'	=> __( "Check if you want to group results in autocomplete dropdown.", 'woo-bookstore' )
		),
		
		array(
			'name'    	=> __( 'Select autocomplete taxonomies', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Select by which taxonomies you want autocomplete results to be grouped by.", 'woo-bookstore' ),
			'id'      	=> 'woobookstore_advanced_search_autocomplete_taxonomies',
			'std'     	=> 'yes', 
			'default' 	=> 'yes', 
			'type'    	=> 'multiselect',
			'desc_tip'	=> __( "This option will take affect if you check group results from above.", 'woo-bookstore' ),
			'options'	=> array(
				'book_category' 	=> __( "Book Category", 'woo-bookstore' ),
				'book_author'		=> __( "Book Author", 'woo-bookstore' ),
				'book_publisher'	=> __( "Book Publisher", 'woo-bookstore' )
			)
		),
		
		array(
			'name'    	=> __( 'Autocomplete results', 'woo-bookstore' ),
			'desc'    	=> __( "Enter autocomplete number of results.", 'woo-bookstore' ),
			'id'      	=> 'woobookstore_advanced_search_autocomplete_book_num',
			'std'     	=> '1', 
			'default' 	=> '10', 
			'type'    	=> 'number',
			'class'		=> 'small-text',
			'desc_tip'	=> __( "-1 for unlimited.", 'woo-bookstore' )
		),
		
		array( 
			'type' => 'sectionend', 
			'id' => 'woo_bookstore_as_settings' 
		),
		
		/**
		 * Cache results for Advanced Search
		 *
		 */
		array( 
			'name' => __( 'Advanced Settings', 'woo-bookstore' ), 
			'type' => 'title', 
			'desc' => __( 'Advanced settings for better experience.', 'woo-bookstore' ), 
			'id' => 'woo_bookstore_advanced_settings' 
		),
		
		array(
			'name'    	=> __( 'Related Products', 'woo-bookstore' ),
			'desc'    	=> "",
			'id'      	=> 'woobookstore_filter_related_products',
			'std'     	=> 'no', 
			'default' 	=> 'no', 
			'type'    	=> 'checkbox',
			'desc_tip'	=> __( "Check if you want WooCommerce Bookstore to filter related products query and display book from the same author, publisher or category.", 'woo-bookstore' )
		),
		
		array(
			'name'    	=> __( 'Enable Cache', 'woo-bookstore' ),
			'desc'    	=> "",
			'id'      	=> 'woobookstore_enable_cache',
			'std'     	=> 'no', 
			'default' 	=> 'no', 
			'type'    	=> 'checkbox',
			'desc_tip'	=> __( "For large number of books use this option for better performance.", 'woo-bookstore' )
		),
		
		array(
			'name'    	=> __( 'Cache lifetime', 'woo-bookstore' ),
			'desc'    	=> "",
			'id'      	=> 'woobookstore_cache_lifetime',
			'std'     	=> '1', 
			'default' 	=> '300', 
			'type'    	=> 'number',
			'class'		=> 'small-text',
			'desc_tip'	=> __( "Enter how long would you like your results to be in the cache( seconds )", 'woo-bookstore' )
		),
		
		array(
			'name'    	=> __( 'Cache Type', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Select where you would like to store cache results.", 'woo-bookstore' ),
			'id'      	=> 'woobookstore_cache_type',
			'std'     	=> 'yes', 
			'default' 	=> 'db', 
			'type'    	=> 'select',
			'desc_tip'	=> __( "For large number of book we recommend files.", 'woo-bookstore' ),
			'options'	=> array(
								'db'	=> __( "Database", 'woo-bookstore' ),
								'file'	=> __( "File", 'woo-bookstore' )
							)
		),
		
		array(
			'name'    	=> __( 'Purge Cache', 'woo-bookstore' ),
			'desc'    	=> '<br /><br />'.__( "Purge cache on post saved.", 'woo-bookstore' ),
			'id'      	=> 'woobookstore_purge_cache_on_post_save',
			'std'     	=> 'no', 
			'default' 	=> 'no', 
			'type'    	=> 'checkbox',
			'desc_tip'	=> __( "Check here if you would like to purge the cache when a post is being saved.", 'woo-bookstore' )
		),
		
		array(
			'name'    	=> __( 'Empty Cache', 'woo-bookstore' ),
			'desc'    	=> "",
			'desc_tip' 	=> __( "Click to empty plugin's cache", 'woo-bookstore' ) ,
			'id'      	=> 'woobookstore_empty_cache',
			'std'     	=> '1', 
			'default' 	=> '', 
			'type'    	=> 'woo_bookstore_empty_cache',
			'class'		=> 'small-text',
			'desc_tip'	=> ""
		),
		
		array(
			'name'    	=> __( 'Debug mode', 'woo-bookstore' ),
			'desc'    	=> "",
			'id'      	=> 'woobookstore_debug_mode',
			'std'     	=> 'no', 
			'default' 	=> 'no', 
			'type'    	=> 'checkbox',
			'desc_tip'	=> __( "This will unminify all css and js for debug information.", 'woo-bookstore' )
		),
		
		array( 
			'type' => 'sectionend', 
			'id' => 'woo_bookstore_advanced_settings' 
		),
		
		/**
		 * Export books
		 *
		 */
		array( 
			'name' => __( 'Export Books', 'woo-bookstore' ), 
			'type' => 'title', 
			'desc' => __( 'Export books from WooCommerce', 'woo-bookstore' ), 
			'id' => 'woo_bookstore_export_settings' 
		),
		
		array(
			'name'    	=> __( 'CSV separator', 'woo-bookstore' ),
			'desc'    	=> "",
			'id'      	=> 'woobookstore_export_separator',
			'std'     	=> ",", 
			'default' 	=> ",", 
			'type'    	=> 'select',
			'class'		=> 'small-text',
			'desc_tip'	=> __( "Enter csv separator e.g. ; e.t.c.", 'woo-bookstore' ),
			'options'	=> array(
				'comma' 	=> __( "Comma (,)", 'woo-bookstore' ),
				'semicolon' => __( "Semicolon (;)", 'woo-bookstore' ),
				'tab' 		=> __( "Tab", 'woo-bookstore' ),
			)			
		),
		
		array(
			'name'    	=> __( 'Export', 'woo-bookstore' ),
			'desc'    	=> "",
			'desc_tip' 	=> __( "Click to export products", 'woo-bookstore' ) ,
			'id'      	=> 'woo_bookstore_export_books',
			'std'     	=> '1', 
			'default' 	=> '', 
			'type'    	=> 'woo_bookstore_export_books',
			'class'		=> 'small-text',
			'desc_tip'	=> ""
		),
		
		array(
			'name'    	=> __( 'Download', 'woo-bookstore' ),
			'desc'    	=> "",
			'desc_tip' 	=> __( "Click to download files", 'woo-bookstore' ) ,
			'id'      	=> 'woo_bookstore_download_file',
			'std'     	=> '1', 
			'default' 	=> '', 
			'type'    	=> 'woo_bookstore_download_file',
			'class'		=> 'small-text',
			'desc_tip'	=> ""
		),
		
		array( 
			'type' => 'sectionend', 
			'id' => 'woo_bookstore_export_settings' 
		),
		
	);


	//if( $is_woocommerce_2_0 ) {

		$options['general_settings'] = $settings;

	//}else{

		//$options['general_settings'] = array_merge( $general_settings_start,  array( get_wcp_page_option() ), $general_settings_end );
	//}

	return $options;
	
}

/**
 * Register custom field for html. This field is going to be used here Woocommerce > Settings > Bookstore 
 *
 */
if ( ! has_action( 'woocommerce_admin_field_woo_bookstore_empty_cachel' ) ) {
	add_action( 'woocommerce_admin_field_woo_bookstore_empty_cache', 'woo_bookstore_admin_empty_cache' );
}

if ( ! has_action( 'woocommerce_admin_field_woo_bookstore_export_booksl' ) ) {
	add_action( 'woocommerce_admin_field_woo_bookstore_export_books', 'woo_bookstore_export_books' );
}

if ( ! has_action( 'woocommerce_admin_field_woo_bookstore_download_filel' ) ) {
	add_action( 'woocommerce_admin_field_woo_bookstore_download_file', 'woo_bookstore_download_file' );
}

function woo_bookstore_admin_empty_cache( $value ){
	
	$nonce = wp_create_nonce( 'wwb_ajax_call' );
	
	ob_start();
	?>
	<tr valign="top">
		<th scope="row" class="titledesc"><?php echo esc_html( $value['title'] ) ?> <?php echo $value['desc'] ?></th>
		<td class="forminp bookstore_html_field">
			<a class="button woo_bookstore_ajax_btn" id="woo_bookstore_admin_empty_cache" href="#"><?php _e( 'Empty', 'woo-bookstore' ) ?></a>
            <input type="hidden" value="<?php echo $nonce ?>" id="wwb_empty_cache_non"  />
			<p class="description" id="woo_bookstore_admin_empty_cache_messages"></p>
            <p class="description"><?php _e( "Click to empty plugin's cache", 'woo-bookstore' ) ?></p>
		</td>
	</tr>
    <?php
	echo ob_get_clean();
	
}

function woo_bookstore_export_books( $value ) {
	
	$nonce = wp_create_nonce( 'wwb_ajax_call' );
	
	ob_start();
	?>
	<tr valign="top">
		<th scope="row" class="titledesc"><?php echo esc_html( $value['title'] ) ?> <?php echo $value['desc'] ?></th>
		<td class="forminp bookstore_html_field">
			<a class="button woo_bookstore_ajax_btn" id="woo_bookstore_export_books" href="#"><?php _e( 'Export', 'woo-bookstore' ) ?></a>
            <input type="hidden" value="<?php echo $nonce ?>" id="wwb_export_non"  />
			<p class="description" id="woo_bookstore_admin_export_messages"></p>
            <p class="description"><?php _e( "Click to export books", 'woo-bookstore' ) ?></p>
		</td>
	</tr>
    <?php
	echo ob_get_clean();
		
}


function woo_bookstore_download_file( $value ) {
	
	$export_file 	= WPINI_WOO_BOOKSTORE_UPLOADS_DIR.'books.csv';
	$link 			= WPINI_WOO_BOOKSTORE_UPLOADS_DIR_URI."books.csv";
	
	ob_start();
	?>
	<tr valign="top">
		<th scope="row" class="titledesc"><?php echo esc_html( $value['title'] ) ?> <?php echo $value['desc'] ?></th>
		<td class="forminp bookstore_html_field">
        	<?php if( file_exists( $export_file ) ) :?>
				<?php $filemtime = filemtime( $export_file ) ?>
                <a class="button" id="" href="<?php echo $link ?>" target="_blank">
                    <?php _e( 'Download', 'woo-bookstore' ) ?>
                </a>
                <p class="description"><?php _e( "Created: ", 'woo-bookstore' ) ?> <span class="export_creation_date"><?php echo date_i18n( get_option( 'date_format' ), $filemtime ) ?> <?php echo date_i18n( get_option( 'time_format' ), $filemtime ) ?></span></p>
			<?php else: ?>
				
                <p class="description"><?php _e( "No file created.", 'woo-bookstore' ) ?></p>
                            
        	<?php endif ?>
        </td>
	</tr>
    <?php
	echo ob_get_clean();	
	
	
}

/**
 * Return all pages of Wordpress
 *
 */
function woo_bookstore_get_pages(){
	
	$pages = array();
	
	$args = array( 
				'post_type' 	=> 'page',
				'post_status'	=> 'publish',
				'posts_per_page'=> -1,
				'orderby'		=> 'title',
				'order'			=> 'ASC'		
			);
	
	$query = new WP_Query( $args );
	if( $query->found_posts ){
		foreach( $query->posts as $post ){
			$pages[$post->ID] = $post->post_title;
		}
	}
	return $pages;
}