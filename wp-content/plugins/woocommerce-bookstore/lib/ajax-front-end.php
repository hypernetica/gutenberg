<?php
/**
 * Front end AJAX scripts
 *
 * TABLE OF CONTENTS
 *
 * wb_search_books_callback()
 * woo_bookstore_build_search_params()
 * woo_bookstore_ajax_search_show_books()
 *
 */
 
/**
 * Autocomplete
 *
 */ 
add_action( 'wp_ajax_wb_advanced_search_books', 'wb_search_books_callback' );
add_action( 'wp_ajax_nopriv_wb_advanced_search_books', 'wb_search_books_callback' );

function wb_search_books_callback(){
	
	$term = isset( $_REQUEST['term'] ) && ! empty( $_REQUEST['term'] ) ? esc_sql( $_REQUEST['term'] ) : "" ;
	
	$results = array();
	
	if( ! empty( $term ) ){
	
		if( isset( $_REQUEST['type'] ) ){
			/*
			Array
			(
				[action] => wb_advanced_search_books
				[type] => search__wwcp_isbn
				[term] => fff
			)
			*/
			$term_name = str_replace( 'search_', '', $_REQUEST['type'] );
					
		}
		
		//return an array of book fields and their types e.g. taxonomy or postmeta
		$field_types = get_option( 'woo_bookstore_advanced_search_field_types' );
		
		if( ! empty( $field_types ) ){
			
			$field_types = maybe_unserialize( $field_types );
			$term_type = isset( $field_types[$term_name] ) ? $field_types[$term_name] : "";
			
			$results = Woo_Bookstore_Advanced_Search::search_for_book_term( $term, $term_name, $term_type );
			
			if( empty( $results ) )
				array_push( $results, 
								array(
									'label' 	=> __( 'Found no results', 'woo-bookstore' ),
									'value'		=> '',
									'url'		=> '',
									'category'	=> __( 'Books', 'woo-bookstore' ),
								)
							);
			
		}
	
	}
		
	echo json_encode( $results );
	
	die();
}

/**
 * Build seo friendly url's for advanced search
 *
 */
add_action( 'wp_ajax_woo_bookstore_search_params', 'woo_bookstore_build_search_params' );
add_action( 'wp_ajax_nopriv_woo_bookstore_search_params', 'woo_bookstore_build_search_params' );

function woo_bookstore_build_search_params(){
	
	$url = "";
	
	if( ! empty( $_POST['data'] ) ){
	
		parse_str( $_POST['data'], $data );
		
		$query_params = array();
		
		//get taxonomies
		if( isset( $data['fields']['taxonomy'] ) && ! empty( $data['fields']['taxonomy'] ) ){
			
			foreach( $data['fields']['taxonomy'] as $tax => $values ){
				
				if( ! empty( $values ) ){
					
					$query_str 		= ""; 
					$search_terms 	= array();
					
					if( ! is_array( $values ) )
						$values = array( $values );
					
					foreach( $values as $val ){
						if( ! empty( $val ) && $val != 'any' )
							$search_terms[] = $val;
					}
					
					if( ! empty( $search_terms ) ){
						$query_str .= "filter_".$tax."=";//build query string
						$query_str .= implode( ',', $search_terms );
						$query_params[] = $query_str;
					}
					
				}
				
			}
		}
		
		//get meta
		if( isset( $data['fields']['meta'] ) && ! empty( $data['fields']['meta'] ) ){
			
			foreach( $data['fields']['meta'] as $meta => $values ){
				
				if( ! empty( $values ) ){
					
					$query_str 		= ""; 
					$search_terms 	= array();
					
					if( ! is_array( $values ) )
						$values = array( $values );
					
					foreach( $values as $val ){
						if( ! empty( $val ) && $val != 'any' )
							$search_terms[] = $val;
					}
					
					if( ! empty( $search_terms ) ){
						$query_str .= "filter_".$meta."=";//build query string
						$query_str .= implode( ',', $search_terms );
						$query_params[] = $query_str;
					}
					
				}
				
			}
			
		}
		
		if( isset( $data['fields']['free_text'] ) && ! empty( $data['fields']['free_text'] ) ){
		
			$query_str .= "filter_keyword=".$data['fields']['free_text'];//build query string
			$query_params[] = $query_str;
			
		}
		
		if( ! empty( $query_params ) )
			$url = get_permalink( get_option( 'bookstore_adv_search_page_id' ) ).'?wb_search=books&'.implode( '&', $query_params );
		
	}
	
	echo json_encode( 
		array( 'url' => $url )
	);
	
	die();
}

/**
 * Show books in search results 
 *
 */
add_action( 'wp_ajax_woo_bookstore_ajax_results', 'woo_bookstore_ajax_search_show_books' );
add_action( 'wp_ajax_nopriv_woo_bookstore_ajax_results', 'woo_bookstore_ajax_search_show_books' );
function woo_bookstore_ajax_search_show_books(){
	
	if( ! empty( $_POST['data'] ) ){
		
		$books 		= array();
		$do_search	= false;
		$html		= '';
		
		//extra query params from ajax request
		$per_page 	= isset( $_POST['per_page'] ) ? $_POST['per_page'] : 12;
		$columns 	= isset( $_POST['columns'] ) ? $_POST['columns'] : 4;
		$paged		= ( $_POST['paged'] ? $_POST['paged'] : 1 );
				
		parse_str( $_POST['data'], $data );
		
		$cache_enabled	= ( get_option( 'woobookstore_enable_cache' ) == 'yes' )? true : false;
		
		$lang = "";
		if( defined( 'ICL_LANGUAGE_CODE' ) )
			$lang = ICL_LANGUAGE_CODE;
		
		if( $cache_enabled ){
			
			$cache_id 	= Woo_Bookstore_Advanced_Search::build_cache_id( $lang.$paged.$_POST['data'] );//build a unique cache id	
			$books 		= Woo_Bookstore_Advanced_Search::get_cache_results( $cache_id );
			
		}
		
		if( empty( $books ) ){
		
			
			if( version_compare( WPINI_WOO_BOOKSTORE_WC_VERSION, '3.0', '<' ) ) {
			
				$meta_query = 
					array(
						array(
							'meta_key' 		=> '_visibility',
							'meta_value'	=> 'visible',
							'compare'		=> 'IN'
						)
					);
			
			} else {
				
				$meta_query = array();
				
			}
			
			$tax_query	= array(
							array(
								'taxonomy'	=> 'product_type',
								'field' 	=> 'slug',
								'terms' 	=> 'book'
							) 
						);
			
			if( version_compare( WPINI_WOO_BOOKSTORE_WC_VERSION, '3.0', '>=' ) ) {
				
				array_push(
					$tax_query,
					array(
						'taxonomy'	=> 'product_visibility',
						'field' 	=> 'slug',
						'terms'    	=> 'exclude-from-search',
						'operator' 	=> 'NOT IN'
					)
				);
				
			}
			
			$post__in = array();
			
			//build taxonomy query			
			if( isset( $data['fields']['taxonomy'] ) && ! empty( $data['fields']['taxonomy'] ) ){
				
				foreach( $data['fields']['taxonomy'] as $taxonomy => $values ){
					
					if( ! is_array( $values ) )
						$values = array( $values );
					
					foreach( $values as $val ){
						
						if( ! empty( $val ) && $val != 'any' ){
							
							// Search taxonomy terms
							$get_the_term = get_term_by( 'slug', $val, $taxonomy );
							
							if( ! empty( $get_the_term ) && ! is_wp_error( $get_the_term ) ) {
								
								$do_search = true;
								array_push( 
									
									$tax_query, 
									array(
										'taxonomy'	=> $taxonomy,
										'field' 	=> 'slug',
										'terms' 	=> $val,
										'operator'	=> 'IN'
									)
								);
								
							} else {
								
								if( class_exists( 'WP_Term_Query' ) ) {
									
									$term_args = array(
										'taxonomy' => $taxonomy,
										'hide_empty' 	=> true,
										'name__like'	=> $val,
										'fields'		=> 'ids'
									);
									
									$wp_term_query = new WP_Term_Query( $term_args );
									
									if( ! is_wp_error( $term_query ) && ! empty( $term_query->terms ) ) {
								
										$do_search = true;
										array_push( 
											$tax_query, 
											array(
												'taxonomy'	=> $taxonomy,
												'field' 	=> 'term_id',
												'terms' 	=> $term_query->terms,
												'operator'	=> 'IN'
											) 
										);
									
									}
									
								}
									
							}
							
						}
					}
						
					
				}
			}
			
			//build meta query	
			if( isset( $data['fields']['meta'] ) && ! empty( $data['fields']['meta'] ) ) {
				
				foreach( $data['fields']['meta'] as $meta => $values ){
					
					if( ! is_array( $values ) )
						$values = array( $values );
					
					foreach( $values as $val ){
						
						if( !empty( $val ) && $val != 'any' ){
							
							$do_search = true;
							array_push( 
								$meta_query, 
								array(
									'key'		=> $meta,
									'value' 	=> $val,
									'compare' 	=> 'LIKE'
								) 
							);
							
						}
					}
					
				}
				
			}
			
			//free text search
			if( isset( $data['fields']['free_text'] ) && ! empty( $data['fields']['free_text'] ) ) {
				
				$post__in = Woo_Bookstore_Advanced_Search::free_text_search( $data['fields']['free_text'] );
				if( ! empty( $post__in )  )
					$do_search = true;
				
			}
						
			if( $do_search ){
				
				$args = array(
							'post_type'			=> 'product',
							'post_status'		=> 'publish',
							'meta_query'		=> $meta_query,
							'tax_query'			=> $tax_query,
							'posts_per_page'	=> $per_page,
							'paged'				=> $paged,
							'post__in'			=> $post__in
						);
				
				$books = new WP_Query( apply_filters( 'woo_bookstore_query_args', $args ) );
				
			}
			
			if( $cache_enabled )
				Woo_Bookstore_Advanced_Search::push_to_cache( $cache_id, $books );
			
		}
		
	}
	
	/**
	 * THE HTML
	 *
	 */
	add_filter( 'post_class', 'woo_book_post_class' );
	
	ob_start();
	
	$template_name = ( !empty( $books ) ? 'woo-bookstore-catalog.php' : 'no-books-found.php' );
	$wc_get_template = function_exists('wc_get_template') ? 'wc_get_template' : 'woocommerce_get_template';
	$wc_get_template( $template_name, array( 
							'products' 	=> $books,
							'columns'	=> $columns
						), 
						'', 
						( !empty( $books ) ? WPINI_WOO_BOOKSTORE_DIR . 'templates/bookstore/' : WPINI_WOO_BOOKSTORE_DIR . 'templates/loop/' )
					);
	
	woo_bookstore_pagination( $books, $paged );
	
	wp_reset_postdata();

	echo json_encode( 
		array( 
			'results' => '<div class="woocommerce columns-' . $columns . '">' . ob_get_clean() . '</div>'
		)
	);
	
	remove_filter( 'post_class', 'woo_book_post_class' );
	
	die();
	
}