<?php
/****
 *
 * AJAX Functions
 *
 * wwb_search_book()
 * wwb_search_value()
 * wwb_clear_cache()
 *
 *
 */
function wwb_search_book() {
		
	$term = strtolower( $_GET['term'] );
	$results = array();
	
	$args = array(
				's' 				=> $term,
				'post_type' 		=> array( 'product' ),
				'post_status'		=> 'publish',
				'posts_per_page'	=> '-1'
			);
	
	$loop = new WP_Query( $args );
	
	while( $loop->have_posts() ) {
		$loop->the_post();
		$suggestion = array();
		$suggestion['label'] 	= get_the_title();
		$suggestion['ID']		= get_the_ID();
		$suggestion['isbn']		= get_post_meta( $suggestion['ID'], wwb_post_meta_name( 'isbn' ) ,true );
		$results[] = $suggestion;
	}
	
	wp_reset_query();
	
	
	$response = json_encode( $results );
	echo $response;
	exit();

}

add_action( 'wp_ajax_wwb_search_book', 'wwb_search_book' );
add_action( 'wp_ajax_nopriv_wwb_search_book', 'wwb_search_book' ); 

function wwb_search_value() {
	
	check_ajax_referer( 'wwb_ajax_call', 'security' );
	
	$return = "false";
	
	extract($_POST, EXTR_PREFIX_SAME, "wpini");
	
	if( woo_book_value_exists( $post, $type, $value ) )
		$return = "true";
		
	echo $return;

	exit();

}

add_action( 'wp_ajax_wwb_search_value', 'wwb_search_value' );
add_action( 'wp_ajax_nopriv_wwb_search_value', 'wwb_search_value' );

function wwb_clear_cache(){
	
	$msg 	= __( 'Invalid request','woo-bookstore' );
	$status = "error";
	
	check_ajax_referer( 'wwb_ajax_call', 'security' );
	
	global $wpdb;
	$wpdb->query( "TRUNCATE TABLE {$wpdb->prefix}woobookstore_cache" );
	
	//clear cache files as well
	$cache_folder = Woo_Bookstore_Advanced_Search::get_cache_folder();
	if( is_dir( $cache_folder ) ){
		array_map( 'unlink', glob( $cache_folder."*" ) );
	}
		
	$msg 	= __( 'Cache cleared successfully!','woo-bookstore' );
	$status = "success";
	
	echo json_encode( 
		array(
			'status' 	=> $status,
			'msg'		=> $msg
		)
	);
	
	die();
	
}

add_action( 'wp_ajax_wwb_clear_cache', 'wwb_clear_cache' );
add_action( 'wp_ajax_nopriv_wwb_clear_cache', 'wwb_clear_cache' );

function wwb_admin_run_updater_callback(){
	
	$msg 	= __( 'Invalid request','woo-bookstore' );
	$status = "error";
	
	$user_ID = get_current_user_id();
	
	if( isset( $_POST['security'] ) ){
		
		if( wp_verify_nonce( $_POST['security'], $user_ID.'-woo-bookstore-run-updater' ) ){
			
			$run 	= WPini_Bookstore_Admin::woo_bookstore_updater();
			
			if( $run ){
				
				$file = fopen( WPINI_WOO_BOOKSTORE_UPLOADS_DIR.'updater.json', 'w' );
				fwrite( $file, json_encode( array( 'updater_executed' => '1', 'last_update' => strtotime( date( "Y-m-d H:i:s" ) ) ) ) );
				fclose( $file );
				
			}
			
			$status = ( $run ? 'success' : 'error' );
			$msg	= __( 'Updater executed successfully!','woo-bookstore' );
		
		}
		
	}
	
	echo json_encode( 
		array(
			'status' 	=> $status,
			'msg'		=> $msg
		)
	);
	
	die();
}
add_action( 'wp_ajax_wwb_admin_run_updater', 'wwb_admin_run_updater_callback' );
add_action( 'wp_ajax_nopriv_wwb_admin_run_updater', 'wwb_admin_run_updater_callback' );

function wwb_export_books_callback(){
	
	$msg 	= __( 'Invalid request','woo-bookstore' );
	$status = "error";
	
	check_ajax_referer( 'wwb_ajax_call', 'security' );
	
	$wb_exporter = new WC_Book_Exporter;
	$response	 = $wb_exporter->export( "csv", ( isset( $_POST['separator'] ) ? $_POST['separator'] : "," )  );
	
	echo json_encode( $response );
	
	die();
	
}

add_action( 'wp_ajax_wwb_export_books', 'wwb_export_books_callback' );
add_action( 'wp_ajax_nopriv_wwb_export_books', 'wwb_export_books_callback' );