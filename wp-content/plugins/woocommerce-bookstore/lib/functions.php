<?php
/**
 *
 * TABLE OF CONTENTS
 *
 * wpini_wb_message()
 * wwb_print_messages()
 * wwb_is_doing_ajax()
 * wwb_javascript_messages()
 * woo_bookstore_pagination()
 * woo_book_value_exists()
 * wwb_post_meta_name()
 * wwb_get_valid_book_meta()
 * woo_bookstore_thumbnail_url()
 * get_bookstore_taxonomies()
 * woo_book_post_class()
 * wb_book_dropdown_categories()
 * wb_wb_woocommerce_placeholder_img_src()
 *
 */

function wpini_wb_message( $type, $single = true ){
	
	$msg 	= '';
	$status = '';
	
	$errors = 
		array(
			'isbn_exists' 		=> __( 'ISBN exists.', 'woo-bookstore' ),
			'isbn13_exists' 	=> __( 'ISBN13 exists.', 'woo-bookstore' ),
			'isbn_char_num' 	=> __( 'ISBN should contain 10 characters.', 'woo-bookstore' ),
			'internal_server_error' => __( 'Internal server error', 'woo-bookstore' ),
			'required_fields'	=> __( 'Required fields cannot be empty.', 'woo-bookstore' ),
			'unique_field_exists' => __( "Value you've entered already exists. This value must be unique.", 'woo-bookstore' ),
			'dublicated_custom_fields' => __( "Found duplicated custom field keys.", 'woo-bookstore' ),
		);
	
	$info = 
		array(
			'woocommerce_disabled' 			=> __( 'You must enable WooCommerce in order to work with this plugin.', 'woo-bookstore' ),
			'woocommerce_version' 			=> __( 'Please note that WooCommerce Bookstore plugin requires minimum WooCommerce version 2.5.x. Some functions might not work as expected.', 'woo-bookstore' ),
			'no_results' 					=> __( 'No books found.', 'woo-bookstore' ),
			'confirm_delete_custom_field' 	=> __( 'Are you sure you want to delete this Book field?.', 'woo-bookstore' ),
			'book_fields_empty'				=> __( 'You cannot leave empty book fields.', 'woo-bookstore' ),
			'text_field_display'			=> __( 'Text', 'woo-bookstore' ),
			'date_field_display'			=> __( 'Date', 'woo-bookstore' ),
			'number_field_display'			=> __( 'Number', 'woo-bookstore' ),
			'textarea_field_display'		=> __( 'Textarea', 'woo-bookstore' ),
		);
	
	$success = 
		array(
			'settings_updated' => __( 'Settings updated.', 'woo-bookstore' ),
			'restore_success' => __( 'Successfully restored to defaults.', 'woo-bookstore' )
		);
	
	
	if( isset($errors[$type]) ){
		$msg = $errors[$type];
		$status = 'error';
	}
	
	if( isset($info[$type]) ){
		$msg = $info[$type];
		$status = 'info';
	}
	
	if( isset($success[$type]) ){
		$msg = $success[$type];
		$status = 'updated';
	}
	
	if( $single )
		return $msg;
	else
		return array( 'message' => $msg	, 'type' => $status );
}

function wwb_print_messages(){
	
	if( isset( $_GET['message'] ) ){
    	
		$messages = explode( ',', $_GET['message'] );
		if( !is_array( $messages ) )
			$messages = array( $messages );
		
		foreach( $messages as $message ){
			
			$response = wpini_wb_message( $message, false );
			echo '<div class="'.$response['type'].'"><p><strong>'.$response['message'].'</strong></p></div>';
			
		}
	}
	
}

function wwb_is_doing_ajax(){
	
	$is_ajax = false;
	
	if( defined( 'DOING_AJAX' ) )
		if( DOING_AJAX )
			$is_ajax = true;
	
	return $is_ajax;
}

function wwb_javascript_messages(){
	
	global $post; 
	
	return array(
				'url' 						=> admin_url( 'admin-ajax.php' ),
				'post_id'					=> isset( $post->ID ) ? $post->ID : '',
				'nothing_found_text' 		=> wpini_wb_message( 'no_results' ),
				'internal_server_error'		=> wpini_wb_message( 'internal_server_error' ),
				'isbn_chars_message' 		=> wpini_wb_message( 'isbn_char_num' ),
				'isbn_exists_message' 		=> wpini_wb_message( 'isbn_exists' ),
				'confirm_box_text'			=> wpini_wb_message( 'confirm_delete_custom_field' ),
				'book_fields_empty_text'	=> wpini_wb_message( 'book_fields_empty' ),
				'text_field_display'		=> wpini_wb_message( 'text_field_display' ),
				'date_field_display'		=> wpini_wb_message( 'date_field_display' ),
				'number_field_display'		=> wpini_wb_message( 'number_field_display' ),
				'textarea_field_display'	=> wpini_wb_message( 'textarea_field_display' ),
				'unique_field_exists'		=> wpini_wb_message( 'unique_field_exists' ),
				'required_fields'			=> wpini_wb_message( 'required_fields' ),				
				'yes_field_display'			=> __( 'Yes', 'woo-bookstore' ),
				'no_field_display'			=> __( 'No', 'woo-bookstore' ),
				'regular_text_name'			=> __( 'Regular', 'woo-bookstore' ),
				'short_text_name'			=> __( 'Short', 'woo-bookstore' ),
				'confirm_restore_defaults'	=> __( 'This will remove all fields you defined. Are you sure you want to continue?', 'woo-bookstore' ),
				'current_year'				=> date( "Y" ),
				'dublicated_custom_fields'	=> wpini_wb_message( 'dublicated_custom_fields' ),
        	);
	
}

function woo_bookstore_pagination( $query, $paged = "" ){
	
	$template_name = 'bookstore/woo-bookstore-pagination.php';
	$wc_get_template = function_exists('wc_get_template') ? 'wc_get_template' : 'woocommerce_get_template';
	$wc_get_template( $template_name, array( 
							'book_query' 	=> $query,
							'paged'			=> $paged
						), 
						'', 
						WPINI_WOO_BOOKSTORE_DIR . 'templates/' 
					);
	
	
}

/**
 * Check if value exists
 *
 *
 */
function woo_book_value_exists( $post, $key, $value ){
	
	$exists = false;
	
	$args = array(
				'post_type' 		=> array( 'product' ),
				'post_status'		=> 'publish',
				'posts_per_page'	=> '-1',
				'meta_query'		=> array(
											array(
												'key' 		=> $key,
												'value'		=> $value,
												'compare'	=> '='
											)
										)
			);
	

	if( !empty( $post ) )
		$args['post__not_in'] = array( $post );
		
	$products = new WP_Query( $args );
	
	if( $products->found_posts > 0  )
		$exists = true;
		
	wp_reset_query();
	
	return $exists;
}

function wwb_post_meta_name( $name ){
	
	$meta_keys = array(
					'isbn' 				=> '_wwcp_isbn',
					'isbn13' 			=> '_wwcp_isbn13',
					'page_num' 			=> '_wwcp_page_num',
					'published_date'	=> '_wwcp_published_date',
					'original_title'	=> '_wwcp_original_title',
					'tranlated_by'		=> '_wwcp_translated_by'
					);
	
	if( isset($meta_keys[$name]) )
		return $meta_keys[$name];
	else
		return '';
}

function wwb_get_valid_book_meta(){
	
	$custom_fields = woo_book_get_custom_fields();
	
	$meta_keys = array();
	
	if( !empty( $custom_fields ) ){
		foreach( $custom_fields as $field ){
			$meta_keys[] = $field['meta_key'];
		}
	}
	
	return $meta_keys;

}

function woo_bookstore_thumbnail_url( $term_id, $size = 'full' ) {
	
	$thumbnail_id = get_woocommerce_term_meta( $term_id, 'thumbnail_id', true );
	
	if ( $thumbnail_id ){
		$src = wp_get_attachment_image_src( $thumbnail_id, $size );
		return isset( $src[0] ) ? $src[0] : false;		
	}
}

function get_bookstore_taxonomies(){
	
	return array( 'book_category', 'book_author', 'book_publisher' );
	
}

function woo_book_post_class( $classes ) {
	
	if( ! in_array( 'product', $classes ) )
		$classes[] = 'product';
	
	return $classes;
}

function wb_book_dropdown_categories( $taxonomy, $args = array(), $deprecated_hierarchical = 1, $deprecated_show_uncategorized = 1, $deprecated_orderby = '' ) {
    
	global $wp_query;

    $current_product_cat = isset( $wp_query->query[$taxonomy] ) ? $wp_query->query[$taxonomy] : '';
    $defaults            = array(
        'pad_counts'         => 1,
        'show_count'         => 1,
        'hierarchical'       => 1,
        'hide_empty'         => 1,
        'show_uncategorized' => 1,
        'orderby'            => 'name',
        'selected'           => $current_product_cat,
        'menu_order'         => false
    );

    $args = wp_parse_args( $args, $defaults );

    if ( $args['orderby'] == 'order' ) {
        $args['menu_order'] = 'asc';
        $args['orderby']    = 'name';
    }

    $terms = get_terms( $taxonomy, apply_filters( 'woo_bookstore_book_dropdown_'.$taxonomy.'_get_terms_args', $args ) );

    if ( ! $terms ) {
        return;
    }

    $output  = "<select name='".$taxonomy."' class='dropdown_".$taxonomy."'>";
    $output .= '<option value="" ' .  selected( $current_product_cat, '', false ) . '>' . __( 'Select a category', 'woocommerce' ) . '</option>';
    $output .= wc_walk_category_dropdown_tree( $terms, 0, $args );
    if ( $args['show_uncategorized'] ) {
        $output .= '<option value="0" ' . selected( $current_product_cat, '0', false ) . '>' . __( 'Uncategorized', 'woocommerce' ) . '</option>';
    }
    $output .= "</select>";

    echo $output;
}

function wb_woocommerce_placeholder_img_src() {
	
	if( function_exists( 'wc_placeholder_img_src' ) ) {
		
		wc_placeholder_img_src();
		
	} else {
		
		woocommerce_placeholder_img_src();
		
	}
	
}