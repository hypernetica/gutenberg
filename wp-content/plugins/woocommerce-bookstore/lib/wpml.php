<?php
add_filter( 'woo_bookstore_custom_field_name', 'woo_bookstore_translate_fields', 10, 2 );
function woo_bookstore_translate_fields( $field, $type ){
	
	if( function_exists( 'icl_translate' ) ){
		
		$field = icl_translate( 'WooCommerce Bookstore Plugin', $type.'-'.sanitize_title( $field ), $field );
					
	}
	
	return $field;
	
}

add_filter( 'woo_bookstore_save_custom_fields', 'woo_bookstore_save_custom_fields_icl_register_string', 10, 2 );
function woo_bookstore_save_custom_fields_icl_register_string( $fields, $type ){
	
	if( function_exists( 'icl_translate' ) ){
		
		$current_language = ICL_LANGUAGE_CODE;
		if( $current_language == 'en' ){
			
			//register strings in WPML
			foreach( $fields as $key => $values ){
				icl_register_string( 'WooCommerce Bookstore Plugin', $type.'-'.sanitize_title( $values['name'] ), $values['name'] );
				if( $type == 'advanced-search-field' )
					icl_register_string( 'WooCommerce Bookstore Plugin', $type.'-'.sanitize_title( $values['label'] ), $values['label'] );
			}
		}
		
	}
	
	return $fields;
	
}