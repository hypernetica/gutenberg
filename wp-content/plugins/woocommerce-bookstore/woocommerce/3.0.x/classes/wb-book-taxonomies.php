<?php
/**
 * Woo_Bookstore_Taxonomies class.
 *
 * @class 		Woo_Bookstore_Taxonomies
 * @version		1.0
 * @category	Class
 * @author 		WPini
 */
class Woo_Bookstore_Taxonomies {
	
	public static function init() {
		
		$instance = new Woo_Bookstore_Taxonomies();
	
	}
	
	public function __construct(){
		
		$this->book_init();
		
		$book_taxonomies = get_bookstore_taxonomies();
		
		foreach( $book_taxonomies as $taxonomy ){
			
			add_filter( 'manage_edit-'.$taxonomy.'_columns', array( $this, 'book_taxonomy_columns' ), 5);
			add_action( 'manage_'.$taxonomy.'_custom_column', array( $this, 'add_image_column_to_taxonomy' ), 5, 3 );
			
		}
		
		//make book taxonomies sortable
		add_filter( 'woocommerce_sortable_taxonomies', array( &$this, 'make_book_taxonomies_sortable' ) );
	}
	
	public function book_init() {
		
		$post_types = array( 'product' );
		
		$shop_page_id = wc_get_page_id( 'shop' );
	
		$base_slug = $shop_page_id > 0 && get_page( $shop_page_id ) ? get_page_uri( $shop_page_id ) : 'shop';
	
		$category_base = get_option('woocommerce_prepend_shop_page_to_urls') == "yes" ? trailingslashit( $base_slug ) : '';
	
		$cap = version_compare( WOOCOMMERCE_VERSION, '2.0', '<' ) ? 'manage_woocommerce_products' : 'edit_products';
	
		$taxonomies = array();
		
		define( 'EP_BOOK', 8388608 ); 
		
		$taxonomies['book_category'] = array(
							'hierarchical' 			=> true,
							'update_count_callback' => '_update_post_term_count',
							'label' 				=> __( 'Book Categories', 'woo-bookstore'),
							'labels' => array(
									'name' 				=> __( 'Book Categories', 'woo-bookstore' ),
									'singular_name' 	=> __( 'Book Category', 'woo-bookstore' ),
									'search_items' 		=> __( 'Search Book Categories', 'woo-bookstore' ),
									'all_items' 		=> __( 'All Book Categories', 'woo-bookstore' ),
									'parent_item' 		=> __( 'Parent Category', 'woo-bookstore' ),
									'parent_item_colon' => __( 'Parent Category:', 'woo-bookstore' ),
									'edit_item' 		=> __( 'Edit Category', 'woo-bookstore' ),
									'update_item' 		=> __( 'Update Category', 'woo-bookstore' ),
									'add_new_item' 		=> __( 'Add New Category', 'woo-bookstore' ),
									'new_item_name' 	=> __( 'New Category Name', 'woo-bookstore' )
								),
							'show_ui' 				=> true,
							'show_in_nav_menus' 	=> true,
							'capabilities'			=> array(
								'manage_terms' 		=> $cap,
								'edit_terms' 		=> $cap,
								'delete_terms' 		=> $cap,
								'assign_terms' 		=> $cap
							),
							'rewrite' 				=> 
								array( 
									'slug' => get_option( 'woo_book_category_permalink', $category_base . __( 'book_category', 'woo-bookstore' ) ), 
									'with_front' => true, 
									'hierarchical' => true,
									'ep_mask'		=> EP_BOOK  
								)
					);
		
		$taxonomies['book_author'] = 
			array(
					'hierarchical' 			=> false,
					'update_count_callback' => '_update_post_term_count',
					'label' 				=> __( 'Authors', 'woo-bookstore'),
					'labels' => array(
							'name' 				=> __( 'Book Authors', 'woo-bookstore' ),
							'singular_name' 	=> __( 'Book Author', 'woo-bookstore' ),
							'search_items' 		=> __( 'Search Authors', 'woo-bookstore' ),
							'all_items' 		=> __( 'All Book Authors', 'woo-bookstore' ),
							'parent_item' 		=> __( 'Parent Author', 'woo-bookstore' ),
							'parent_item_colon' => __( 'Parent Author:', 'woo-bookstore' ),
							'edit_item' 		=> __( 'Edit Author', 'woo-bookstore' ),
							'update_item' 		=> __( 'Update Author', 'woo-bookstore' ),
							'add_new_item' 		=> __( 'Add New Author', 'woo-bookstore' ),
							'new_item_name' 	=> __( 'New Author Name', 'woo-bookstore' )
						),
					'show_ui' 				=> true,
					'show_in_nav_menus' 	=> true,
					'capabilities'			=> array(
						'manage_terms' 		=> $cap,
						'edit_terms' 		=> $cap,
						'delete_terms' 		=> $cap,
						'assign_terms' 		=> $cap
					),
					'rewrite' 				=> 
						array( 
							'slug' => get_option( 'woo_book_author_permalink', $category_base . __( 'book_author', 'woo-bookstore' ) ), 
							'with_front' => true, 
							'hierarchical' => false,
							'ep_mask'		=> EP_BOOK  
						)
			);
		
		$taxonomies['book_publisher'] = array(
							'hierarchical' 			=> false,
							'update_count_callback' => '_update_post_term_count',
							'label' 				=> __( 'Publishers', 'woo-bookstore'),
							'labels' => array(
									'name' 				=> __( 'Book Publishers', 'woo-bookstore' ),
									'singular_name' 	=> __( 'Book Publisher', 'woo-bookstore' ),
									'search_items' 		=> __( 'Search Publishers', 'woo-bookstore' ),
									'all_items' 		=> __( 'All Book Publishers', 'woo-bookstore' ),
									'parent_item' 		=> __( 'Parent Publisher', 'woo-bookstore' ),
									'parent_item_colon' => __( 'Parent Publisher:', 'woo-bookstore' ),
									'edit_item' 		=> __( 'Edit Publisher', 'woo-bookstore' ),
									'update_item' 		=> __( 'Update Publisher', 'woo-bookstore' ),
									'add_new_item' 		=> __( 'Add New Publisher', 'woo-bookstore' ),
									'new_item_name' 	=> __( 'New Publisher Name', 'woo-bookstore' )
								),
							'show_ui' 				=> true,
							'show_in_nav_menus' 	=> true,
							'capabilities'			=> array(
								'manage_terms' 		=> $cap,
								'edit_terms' 		=> $cap,
								'delete_terms' 		=> $cap,
								'assign_terms' 		=> $cap
							),
							'rewrite' 				=> 
								array( 
									'slug' => get_option( 'woo_book_publisher_permalink', $category_base . __( 'book_publisher', 'woo-bookstore' ) ), 
									'with_front' => true, 
									'hierarchical' => false,
									'ep_mask'		=> EP_BOOK
								)
					);
		
		if( !empty($taxonomies) ){
			
			foreach( $taxonomies as $key=>$taxonomy ){
				register_taxonomy( 
					$key,
					array( 'product' ),
					apply_filters( 'register_taxonomy_'.$key, $taxonomy )
				);
			}
			
		}
		
	}
	
	/**
	 * Admin columns
	 *
	 */
	public function add_book_place_column_content($content,$column_name,$term_id){
		$term= get_term($term_id, 'book_place');
		switch ($column_name) {
			case 'foo':
				//do your stuff here with $term or $term_id
				$content = 'test';
				break;
			default:
				break;
		}
		return $content;
	}
	
	public function book_taxonomy_columns( $defaults ) {
		
		$new_defaults = array();
		$count = 0;
		
		foreach( $defaults as $key => $value ){
			
			if( $key != 'description' ){
			
				if( $count == 1 )
					$new_defaults['taxonomy_thumb'] = __( 'Thumbnail', 'woo-bookstore' );
				
				if( $count == 3 )
					$new_defaults['shortened_description'] = __( 'Description', 'woo-bookstore' );
				
				$new_defaults[$key] = $value;		
				
			}
				
			$count++;
			
		}
		return $new_defaults;
		
	}
	
	public function add_image_column_to_taxonomy( $value, $column_name, $id ) {
		
		switch( $column_name ){
		
			case 'taxonomy_thumb':
				
				$thumbnail = woo_bookstore_thumbnail_url( $id );
				
				if ( ! $thumbnail )
					$thumbnail = wb_woocommerce_placeholder_img_src();
				
				return '<img src="'.$thumbnail.'" width="50" />';
				
				break;
			
			case 'shortened_description':
				
				$term = get_term_by( 'id', $id, $_GET['taxonomy'] );
				
				$term_description = "";
				if( ! is_wp_error( $term ) ){
					if( ! empty( $term->description ) )
						$term_description = $this->get_taxonomy_excerpt( $term->description );
				}
				
				return $term_description;
				
				break;
				
			default:
				break;
		
		}
		
	}
	
	public function get_taxonomy_excerpt( $description, $charlength = 140 ) {
		
		$excerpt = $description;
		$charlength++;
	
		if ( mb_strlen( $excerpt ) > $charlength ) {
			$subex = mb_substr( $excerpt, 0, $charlength - 5 );
			$exwords = explode( ' ', $subex );
			$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
			if ( $excut < 0 ) {
				echo mb_substr( $subex, 0, $excut );
			} else {
				echo $subex;
			}
			echo '[...]';
		} else {
			echo $excerpt;
		}
	}
	
	public function make_book_taxonomies_sortable( $sortables ) {
		
		$sortables[] = 'book_author';
		$sortables[] = 'book_category';
		$sortables[] = 'book_publisher';
		
		return $sortables;
	}
	
}