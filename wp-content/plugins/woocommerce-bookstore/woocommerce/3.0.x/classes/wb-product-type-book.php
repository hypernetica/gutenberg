<?php
/**
 * WC_Product_Book Class
 *
 *
 * @class 		WC_Product_Book
 * @package		WooCommerce Bookstore
 * @category	Class
 * @since		2.0
 *
 */
class WC_Product_Book extends WC_Product {
    
	/**
	 * Supported features such as 'ajax_add_to_cart'.
	 *
	 * @var array
	 */
	protected $supports = array();
	
	/**
     * __construct function.
     *
     * @access public
     * @param mixed $product
     */
	public function __construct( $product ) {
		
		parent::__construct( $product );

		$this->product_type = 'book';
		
		$this->id  = ( isset( $product->ID ) && !empty( $product->ID ) ? $product->ID : $product );    	
				
		// Set custom fields
		$custom_fields = woo_book_get_custom_fields();
		if( ! empty( $custom_fields ) ){
			foreach( $custom_fields as $field ){
				if( ! empty( $field['meta_key'] ) ){
					$this->{$field['meta_key']} = get_post_meta( $this->id, $field['meta_key'] , true ) ;				
				}
			}
		}
		
		$this->supports[] = 'ajax_add_to_cart';
		
	}
	
	public function get_categories( $sep = ', ', $before = '', $after = '' ){
		return get_the_term_list( $this->get_id(), 'book_category', $before, $sep, $after );
	}
	
	public function get_authors( $sep = ', ', $before = '', $after = '' ){
		return get_the_term_list( $this->get_id(), 'book_author', $before, $sep, $after );
	}
	
	public function get_publishers( $sep = ', ', $before = '', $after = '' ){
		return get_the_term_list( $this->get_id(), 'book_publisher', $before, $sep, $after );
	}
	
	/**
	 * Get the add to cart button text.
	 *
	 * @return string
	 */
	public function add_to_cart_text() {
		
		$text = $this->is_purchasable() && $this->is_in_stock() ? __( 'Add to cart', 'woo-bookstore' ) : __( 'Read more', 'woo-bookstore' );

		return apply_filters( 'woocommerce_product_add_to_cart_text', $text, $this );
	}
	
	/**
	 * Get the add to url used mainly in loops.
	 *
	 * @return string
	 */
	public function add_to_cart_url() {
		$url = $this->is_purchasable() && $this->is_in_stock() ? remove_query_arg( 'added-to-cart', add_query_arg( 'add-to-cart', $this->id ) ) : get_permalink( $this->id );

		return apply_filters( 'woocommerce_product_add_to_cart_url', $url, $this );
	}
	
}