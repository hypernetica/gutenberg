<?php
/**
 * Woo_Bookstore_Shortcodes class.
 *
 * @class 		Woo_Book_Shortcodes
 * @version		1.0
 * @category	Class
 * @author 		WPini
 */
class Woo_Bookstore_Shortcodes {

	/**
	 * Init shortcodes
	 *
	 */
	public static function init() {
		
		// Define shortcodes
		$shortcodes = self::get_available_shortcodes();
		
		if( ! empty( $shortcodes ) ){
			foreach ( $shortcodes as $shortcode => $args ) {
				add_shortcode( $shortcode, $args['function'] );				
			}
		}

	}
	
	/**
	 * Return all available shortcode
	 *
	 * @access private
	 * @param string $shortcode
	 * @return array
	 */
	private static function get_available_shortcodes( $shortcode = '' ){
		
		$shortcodes = 
			
			array(
			
				'woo_bookstore_thumbnails' => 
					array(
						'function' 		=> __CLASS__ . '::woo_book_taxonomy_thumbnails',
						'description'	=> __( 'Display thumbnails of a specific book taxonomy.', 'woo-bookstore' ),
						'template'		=> 'woocommerce/bookstore/book-taxonomy-thumbnails.php',
						'example'		=> '[woo_bookstore_thumbnails taxonomy="book_category" columns="3" class="book-category-listing book-category-listing-2"]'
					),
				'woo_bookstore_catalog' => 
					array(
						'function' 		=> __CLASS__ . '::woo_bookstore_shortcode_callback',
						'description'	=> __( 'The main Bookstore Page.', 'woo-bookstore' ),
						'template'		=> 'woocommerce/bookstore/woo-bookstore-catalog.php'
					),
				'woo_bookstore_advanced_search' => 
					array(
						'function' 		=> __CLASS__ . '::woo_bookstore_advanced_search_callback',
						'description'	=> __( 'Advanced book search page.', 'woo-bookstore' ),
						'template'		=> 'woocommerce/bookstore/',
						'example'		=> '[woo_bookstore_advanced_search widget=0]'
					)
			
			
			);
		
		$result = apply_filters( 'woocommerce_bookstore_available_shortcodes', $shortcodes );
		
		if( ! empty( $shortcode ) ){
			if( isset( $shortcodes[$shortcode] ) )
				$result = $shortcodes[$shortcode];
		}
		
		return $result;
	}
	
	/**
	 * Display shortcode information
	 *
	 * @access public
	 * @return HTML
	 */
	public static function show_shortcode_information_table(){
		
		$shortcodes = self::get_available_shortcodes();
		
		ob_start();
		?>
        <?php if( ! empty( $shortcodes ) ): ?>
			<?php foreach( $shortcodes as $shortcode => $args ) :?>            	
            <table class="form-table">
                <tbody>
                    <tr valign="top">
                        <th scope="row">
                            <?php _e( 'Shortcode', 'woo-bookstore' ) ?>
                        </th>
                        <td>
                            <input type="text" readonly="readonly" class="regular-text woo_bookstore_shortcode_preview" value="[<?php echo $shortcode ?>]" />
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">
                            <?php _e( 'Description', 'woo-bookstore' ) ?>
                        </th>
                        <td>
                            <?php echo $args['description'] ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">
                            <?php _e( 'Template', 'woo-bookstore' ) ?>
                        </th>
                        <td>
                            <code>[theme_folder]/<?php echo $args['template'] ?></code>
                        </td>
                    </tr>
                    <?php if( isset( $args['example'] ) ): ?>
                    <tr valign="top">
                        <th scope="row">
                            <?php _e( 'Example Use', 'woo-bookstore' ) ?>
                        </th>
                        <td>
                            <input type="text" readonly="readonly" class="large-text woo_bookstore_shortcode_preview" value='<?php echo $args['example'] ?>' />
                        </td>
                    </tr>
                    <?php endif ?>
                </tbody>
            </table>
            <hr />
            <?php endforeach ?>
		
		<?php else: ?>            
        	<div class="update-nag"><p><?php _e( 'No shortcodes defined in the system.', 'woo-bookstore' ) ?></p></div>
		<?php endif ?>           
        
        <?php
		echo ob_get_clean();
	}
	
	/**
	 * woo_book_taxonomy_thumbnails function.
	 *
	 * @access public
	 * @param mixed $atts
	 * @return void
	 */
	public static function woo_book_taxonomy_thumbnails( $atts ) {

		extract( shortcode_atts( array(
			  'show_empty' 		=> true,
			  'columns'			=> 4,
			  'hide_empty'		=> 0,
			  'orderby'			=> 'name',
			  'exclude'			=> '',
			  'number'			=> '',
			  'taxonomy'		=> 'book_author',
			  'class'			=> ''
		 ), $atts ) );
	
		$exclude = array_map( 'intval', explode(',', $exclude) );
		$order = $orderby == 'name' ? 'asc' : 'desc';
	
		$terms = get_terms( $taxonomy, array( 'hide_empty' => $hide_empty, 'orderby' => $orderby, 'exclude' => $exclude, 'number' => $number, 'order' => $order ) );
	
		if ( ! $terms )
			return;
	
		ob_start();
		
		$template_name = 'book-taxonomy-thumbnails.php';
		$wc_get_template = function_exists('wc_get_template') ? 'wc_get_template' : 'woocommerce_get_template';
		$wc_get_template( $template_name, array( 
								'terms' 	=> $terms,
								'columns'	=> $columns,
								'taxonomy'	=> $taxonomy,
								'class'		=> $class							
							), 
							'', 
							WPINI_WOO_BOOKSTORE_DIR . 'templates/bookstore/' 
						);
		
		return ob_get_clean();
	}
	
	
	/**
	 * woo_bookstore_shortcode_callback function.
	 *
	 * @access public
	 * @param mixed $atts
	 * @return void
	 */
	public static function woo_bookstore_shortcode_callback( $atts ) {

		global $woocommerce_loop, $wp_query;
		
		extract( shortcode_atts( array(
			  'columns'			=> 4,
			  'orderby'			=> 'name',
			  'order'			=> 'ASC',
			  'exclude'			=> '',
			  'number'			=> '',
			  'taxonomy'		=> '',
			  'posts_per_page'	=> 12
		 ), $atts ) );
		
		$exclude	= array_map( 'intval', explode(',', $exclude) );
		$order 		= $orderby == 'name' ? 'asc' : 'desc';
		$paged		= ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;
		
		$args = array(
					'post_type' 		=>	'product',
					'status'			=> 	'publish',
					'posts_per_page'	=>	$posts_per_page,
					'paged'				=>	$paged,
					'orderby'			=>	$orderby,
					'order'				=>	$order,
					'tax_query'	=> 	array(
										array(
											'taxonomy' 	=>	'product_type',
											'field'		=> 	'slug',
											'terms'		=> 	'book'
										)
									)
				);
				
		$books =  new WP_Query( apply_filters( 'woo_bookstore_query_args', $args ) );
		
		if ( ! $books->found_posts )
			return;
	
		ob_start();
		
		$template_name = 'woo-bookstore-catalog.php';
		$wc_get_template = function_exists('wc_get_template') ? 'wc_get_template' : 'woocommerce_get_template';
		$wc_get_template( $template_name, array( 
								'products' 	=> $books,
								'columns'	=> $columns
							), 
							'', 
							WPINI_WOO_BOOKSTORE_DIR . 'templates/bookstore/' 
						);
		
		woo_bookstore_pagination( $books, $paged );
		
		wp_reset_postdata();
	
		return '<div class="woocommerce columns-' . $columns . '">' . ob_get_clean() . '</div>';
		
	}
	
	/**
	 * woo_bookstore_advanced_search function.
	 *
	 * @access public
	 * @param mixed $atts
	 * @return void
	 */
	public static function woo_bookstore_advanced_search_callback( $atts ){
	
		shortcode_atts( array(
			
			'columns' 	=> 4,
			'per_page'	=> 12,
			'widget'	=> false
			
		), $atts );
		
		if( !isset( $atts['widget'] ) )
			$atts['widget'] = false;
		
		ob_start();
		
		$advanced_search = new Woo_Bookstore_Advanced_Search();
		$advanced_search->show_search_form( $atts );
		
		return ob_get_clean();
		
	}
		
}