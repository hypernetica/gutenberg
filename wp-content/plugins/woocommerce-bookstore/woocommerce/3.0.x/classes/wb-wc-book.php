<?php
/**
 * Book Class
 *
 *
 * @class 		WC_Book
 * @package		WooCommerce Bookstore
 * @category	Class
 * @since		1.0
 *
 */
class WC_Book{
	
	private static $instance;
	
	public static function init() {
		if (self::$instance == null) {
			self::$instance = new WC_Book();			
		}
	}
	
	public function __construct() {
		
		add_action( 'woocommerce_book_add_to_cart', array( $this, 'book_add_to_cart' ), 30 );
		add_filter( 'template_include', array( $this, 'wc_book_template_loader' ) );
		add_filter( 'woocommerce_loop_add_to_cart_link', array( $this, 'woo_bookstore_loop_add_to_cart_btn' ),10,2 );
	}
	
	public function book_add_to_cart() {
		wc_get_template( 'single-product/add-to-cart/book.php', array(), '', WPINI_WOO_BOOKSTORE_DIR . 'templates/' );
	}
	
	public function wc_book_template_loader( $template ){
		
		$find = array( 'woocommerce.php' );
		$file = '';

		if ( is_tax( 'book_category' ) ) {

			$term = get_queried_object();

			$file 		= 'taxonomy-' . $term->taxonomy . '.php';
			$find[] 	= 'taxonomy-' . $term->taxonomy . '-' . $term->slug . '.php';
			$find[] 	= WPINI_WOO_BOOKSTORE_DIR . 'taxonomy-' . $term->taxonomy . '-' . $term->slug . '.php';
			$find[] 	= $file;
			$find[] 	= WPINI_WOO_BOOKSTORE_DIR . $file;

		}
		
		if ( is_tax( 'book_publisher' ) ) {

			$term = get_queried_object();

			$file 		= 'taxonomy-' . $term->taxonomy . '.php';
			$find[] 	= 'taxonomy-' . $term->taxonomy . '-' . $term->slug . '.php';
			$find[] 	= WPINI_WOO_BOOKSTORE_DIR . 'taxonomy-' . $term->taxonomy . '-' . $term->slug . '.php';
			$find[] 	= $file;
			$find[] 	= WPINI_WOO_BOOKSTORE_DIR . $file;

		}
		
		if ( is_tax( 'book_author' ) ) {

			$term = get_queried_object();

			$file 		= 'taxonomy-' . $term->taxonomy . '.php';
			$find[] 	= 'taxonomy-' . $term->taxonomy . '-' . $term->slug . '.php';
			$find[] 	= WPINI_WOO_BOOKSTORE_DIR . 'taxonomy-' . $term->taxonomy . '-' . $term->slug . '.php';
			$find[] 	= $file;
			$find[] 	= WPINI_WOO_BOOKSTORE_DIR . $file;

		}

		if ( $file ) {
			$template = locate_template( $find );
			if ( ! $template ) $template = WPINI_WOO_BOOKSTORE_DIR . '/templates/' . $file;
		}

		return $template;
		
	}
	
	public function woo_bookstore_loop_add_to_cart_btn( $link, $product ){
	
		$book_link = $link;
		if( $product->is_type( "book" ) ){		
			$book_link = str_replace( 'product_type_book', 'product_type_simple', $book_link );		
		}
		
		return $book_link;
		
	}	
		
}	