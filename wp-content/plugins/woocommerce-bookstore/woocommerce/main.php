<?php
/**
 * WooCommerce includes
 *
 */
$prefix = WPINI_WOO_BOOKSTORE_DIR.'woocommerce/'.WPINI_WOO_BOOKSTORE_PREFIX_VERSION.'/';

require_once( $prefix.'hooks.php' );
require_once( $prefix.'admin-edit-product.php' );

require_once( $prefix.'classes/wb-wc-book.php' );
require_once( $prefix.'classes/wb-book-shortcodes.php' );
require_once( $prefix.'classes/wb-book-taxonomy-images.php' );
require_once( $prefix.'classes/wb-book-taxonomies.php' );
require_once( $prefix.'classes/wb-advanced-search.php' );

if( file_exists( get_stylesheet_directory().'/classes/wb-exporter.php' ) ) {
	
	require_once( get_stylesheet_directory().'/classes/wb-exporter.php' );
	
} else {
	
	require_once( $prefix.'classes/wb-exporter.php' );	
	
}

/**
 * Widgets
 *
 */
require_once( $prefix.'classes/wb-widgets.php' );

if( ! is_admin() )
	require_once( $prefix.'front-end.php' );

/** Book Conditional Tags */

if ( ! function_exists( 'is_book_category' ) ) {

	/**
	 * is_book_category - Returns true when viewing a product category.
	 *
	 * @access public
	 * @param string $term (default: '') The term slug your checking for. Leave blank to return true on any.
	 * @return bool
	 */
	function is_book_category( $term = '' ) {
		return is_tax( 'book_category', $term );
	}
}

if ( ! function_exists( 'is_book_author' ) ) {

	/**
	 * is_book_author - Returns true when viewing a product author.
	 *
	 * @access public
	 * @param string $term (default: '') The term slug your checking for. Leave blank to return true on any.
	 * @return bool
	 */
	function is_book_author( $term = '' ) {
		return is_tax( 'book_author', $term );
	}
}

if ( ! function_exists( 'is_book_publisher' ) ) {

	/**
	 * is_book_publisher - Returns true when viewing a product publisher.
	 *
	 * @access public
	 * @param string $term (default: '') The term slug your checking for. Leave blank to return true on any.
	 * @return bool
	 */
	function is_book_publisher( $term = '' ) {
		return is_tax( 'book_publisher', $term );
	}
}

if( ! function_exists( 'is_book' ) ){
	
	/**
	 * is_book - Returns true when viewing a single product page.
	 *
	 * @access public
	 * @param string $term (default: '') The term slug your checking for. Leave blank to return true on any.
	 * @return bool
	 */
	function is_book( $ID = '' ) {
		
		$is_book = false;
		
		if( is_product() ){
			
			global $product;
			if ( empty( $ID ) )
				$is_book = $product->is_type( 'book' );		
			else
				$is_book = ( $product->get_id() == $ID );			
		}
		
		return $is_book;
	}
	
}

/**
 * Returns true if a search is being executed
 *
 */
function woo_bookstore_is_search(){
	
	return ( isset( $_GET['wb_search'] ) && $_GET['wb_search'] == "books" );
	
}
?>