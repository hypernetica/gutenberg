<?php
/**
 * Book Exporter Class
 *
 *
 * @class 		WC_Book_Exporter
 * @package		WooCommerce Bookstore
 * @category	Class
 * @since		1.0
 *
 */
class WC_Book_Exporter {
	
	public function __construct() {}
	
	/**
	 * Export function 
	 *
	 * @param $type e.g. csv or xml
	 * @param $separator
	 *
	 * @return array
	 */
	public function export( $type, $separator = "," ) {
		
		$response = array(
			
			'status' => "error",
			'msg'	 => __( 'Unable to export products', 'woo-bookstore' )
			
		);
		
		$books = $this->fetch_books();
		
		if( ! empty( $books ) ) {
			
			if( $type == "csv" ) {
				
				$response = $this->export_csv( $books, $separator );
				
			}
		
		} else {
			
			$response['msg'] = __( 'Found no books', 'woo-bookstore' );
			
		}
				
		return $response;
		
	}
	
	/**
	 * Fetch website books
	 *
	 * @param void
	 *
	 * @return array
	 */
	public function fetch_books() {
		
		$books = array();
		
		$args = array(
		
			'post_type'			=> 'product',
			'post_status'		=> 'publish',
			'posts_per_page'	=> "-1",
			'tax_query' => 
				array(
					array(
						'taxonomy'	=> 'product_type',
						'field' 	=> 'slug',
						'terms' 	=> 'book'
					)
				)		
			
		);
		
		$query = new WP_Query( apply_filters( 'woo_bookstore_export_books_args', $args ) );
		$books = $query->posts;
		
		return $books;
			
	}
	
	/**
	 * Export csv file
	 *
	 * @param array $books
	 * @param string $separator
	 *
	 * @return array
	 */
	public function export_csv( $books, $separator ) {
	
		$status		= "error";
		$msg	 	= __( 'Unable to export products', 'woo-bookstore' );
		$created	= "";
			
		$export_file 	= WPINI_WOO_BOOKSTORE_UPLOADS_DIR.'books.csv';
		$custom_fields 	= woo_book_get_custom_fields();
		
		$csv = array();
				
		$csv_headers = array(
			
			"ID",
			"TITLE",
			"POST_CONTENT",
			"EXCERPT",
			"STATUS",
			"SKU",
			"PRICE",
			"SALE_PRICE",
			"PERMALINK",
			"AUTHOR",
			"BOOK_CATEGORY",
			"BOOK_PUBLISHER",
			"PRODUCT_CATEGORY",
			"FEATURED_IMAGE"
			
		);
		
		if( ! empty( $custom_fields ) ) {
			
			foreach( $custom_fields as $key => $field ) {
				
				$csv_headers[] = $field['meta_key'];
				
			}
			
		}
		
		if( ! empty( $books ) ) {
			
			foreach( $books as $key => $book ) {
				
				$row = array();
				
				$row["ID"]						= $book->ID;
				$row["TITLE"]					= $book->post_title;
				$row["POST_CONTENT"]			= $book->post_content;
				$row["EXCERPT"]					= $book->post_excerpt;
				$row["STATUS"]					= $book->post_status;
				$row["SKU"]						= get_post_meta( $book->ID, "_sku", true );
				$row["PRICE"]					= get_post_meta( $book->ID, "_regular_price", true );
				$row["SALE_PRICE"]				= get_post_meta( $book->ID, "_sale_price", true );
				$row["PERMALINK"]				= get_permalink( $book->ID );
				$row["AUTHOR"]					= $this->get_term_list_non_heirarchy( $book->ID, "book_author" );
				$row["BOOK_CATEGORY"]			= $this->get_term_tree( $book->ID, "book_category" );
				$row["BOOK_PUBLISHER"]			= $this->get_term_list_non_heirarchy( $book->ID, "book_publisher" );
				$row["PRODUCT_CATEGORY"]		= $this->get_term_tree( $book->ID, "product_cat" );
				$row["FEATURED_IMAGE"]			= get_the_post_thumbnail_url( $book->ID, "full" );
								
				if( ! empty( $custom_fields ) ) {
			
					foreach( $custom_fields as $key => $field ) {
						
						$row[$field['meta_key']] = get_post_meta( $book->ID, $field['meta_key'], true );
						
					}
					
				}
				
				array_push( $csv, $row );
				
			}
		}
		
		if( ! empty( $csv ) ) {
			
			$fp = fopen( $export_file, 'w' );
				
			if( $fp ) {
				
				$file_separator = $this->get_separator( $separator );
				
				fputcsv( $fp, $csv_headers, $file_separator );
								
				foreach ( $csv as $fields ) {
					fputcsv( $fp, $fields, $file_separator );
				}
				
				fclose( $fp );
				
				$filemtime = filemtime( $export_file );
				
				$status 	= 'success';
				$msg 		= sprintf( __( 'File created found: %s books', 'woo-bookstore' ), count( $csv ) );
				$created	= date_i18n( get_option( 'date_format' ), $filemtime )." ".date_i18n( get_option( 'time_format' ), $filemtime );
				
			}
			
		} else {
			
			$msg = __( 'Unable to create csv file', 'woo-bookstore' );
			
		}
		
		$response = array(
			
			'status' 	=> $status,
			'msg'	 	=> $msg,
			'created'	=> $created
			
		);
		
		return $response;
		
		
	}
	
	/**
	 * Recursively get taxonomy and its children
	 *
	 * @param string $taxonomy
	 * @param int $parent - parent term id
	 * @return array
	 */
	public function get_taxonomy_hierarchy( $taxonomy, $parent = 0, $post_terms = array() ) {
		
		// only 1 taxonomy
		$taxonomy = is_array( $taxonomy ) ? array_shift( $taxonomy ) : $taxonomy;
		// get all direct decendants of the $parent
		$terms = get_terms( $taxonomy, array( 'parent' => $parent ) );
		// prepare a new array.  these are the children of $parent
		// we'll ultimately copy all the $terms into this new array, but only after they
		// find their own children
		$children = array();
		// go through all the direct decendants of $parent, and gather their children
		foreach ( $terms as $term ){
			if( in_array( $term->term_id, $post_terms ) ) {
				// recurse to get the direct decendants of "this" term
				$term->children = $this->get_taxonomy_hierarchy( $taxonomy, $term->term_id, $post_terms );
				// add the term to our new array
				$children[ $term->term_id ] = $term;
			}
		}
		// send the results back to the caller
		return $children;
		
	}
	
	public function get_term_tree( $id, $taxonomy ) {
		
		$terms 		= array();
		$post_terms = wp_get_post_terms(  $id, $taxonomy, array( 'fields' => 'ids' ) );
		$hierarchy 	= $this->get_taxonomy_hierarchy( $taxonomy, 0, $post_terms );
				
		foreach( $hierarchy as $level_0_key => $level_0_term ) {
			
			$the_term = array();
			$the_term[] = $level_0_term->name;
			
			# Level 0
			if( ! empty( $level_0_term->children ) ) {
				
				foreach( $level_0_term->children as $level_1_key => $level_1_term ) {
					
					$the_term[] = $level_1_term->name;
					# Level 1
					if( ! empty( $level_1_term->children ) ) {
				
						foreach( $level_1_term->children as $level_2_key => $level_2_term ) {
							
							$the_term[] = $level_2_term->name;
							
							# Level 2
							if( ! empty( $level_2_term->children ) ) {
				
								foreach( $level_2_term->children as $level_3_key => $level_3_term ) {
									
									$the_term[] = $level_3_term->name;
									
									# Level 3
									if( ! empty( $level_3_term->children ) ) {
						
										foreach( $level_3_term->children as $level_4_key => $level_4_term ) {
											
											$the_term[] = $level_4_term->name;
											
											# Level 4
											if( ! empty( $level_4_term->children ) ) {
								
												foreach( $level_4_term->children as $level_5_key => $level_5_term ) {
													
													$the_term[] = $level_5_term->name;
													
													
													
												}
											
											}											
											
										}
									
									}
									
								}
							
							}
							
						}
					
					}					
							
				}
				
			}
			
			$terms[] = implode( ">", $the_term );
		
		}
		
		return implode( "|", $terms );
		
	}
	
	public function get_term_list_non_heirarchy( $id, $taxonomy ) {
		
		$terms = wp_get_post_terms( $id, $taxonomy, array( "fields" => "names" ) );
		return implode( "|", $terms );
		
	}
	
	public function get_separator( $separator ) {
		
		$sep = "";
		
		switch( $separator ) {
			
			case 'comma':
				$sep = ",";
				break;
			case 'semicolon':
				$sep = ";";
				break;
			case 'tab':
				$sep = "\t";
				break;
			default:
				$sep = ",";
				break;
		}
		
		return $sep;
		
	}

}