<?php
/**
 * Woo_Bookstore_Author_Box Class
 *
 * Simple widget to display author box in book single page
 *
 * @class 		Woo_Bookstore_Author_Box
 * @package		WooCommerce Bookstore
 * @category	Class
 * @since		1.0
 *
 */
class Woo_Bookstore_Author_Box extends WP_Widget {
	
	function __construct() {
		
		parent::__construct(
			'Woo_Bookstore_Author_Box', // Base ID
			__( 'WooCommerce Bookstore | Author', 'woo-bookstore' ), // Name
			array(
			 'description' => __( "This widget is for the Single Product page. If the book has an author then a widget will be displayed with author's bio.", 'woo-bookstore' ), ) // Args
		);
	}
	
	public function widget( $args, $instance ) {
		
		if( is_book() ){
			
			global $post;
			
			$book_author	= wp_get_post_terms( $post->ID, 'book_author', $args );
			
			if( ! empty( $book_author ) ){
				
				echo $args['before_widget'];
				if ( ! empty( $instance['title'] ) ) {
					echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
				}
				
				foreach( $book_author as $key => $author ){
					?>
                    <p>
                    <?php if( $instance['show_thumbnails'] == "yes" ): ?>
                        <?php 
                        $thumbnail = woo_bookstore_thumbnail_url( $author->term_id, 'thumbnail' );
                        
                        if ( ! $thumbnail )
                            $thumbnail = wb_woocommerce_placeholder_img_src();
                        ?>
                        <img src="<?php echo $thumbnail; ?>" class="alignleft" alt="<?php echo $author->name; ?>" />
            
                    <?php endif; ?>
                    <?php 
						
						$author_description = $author->description;
						
						if( $instance['excerpt'] > 0 ){
							
							$charlength = $instance['excerpt'];
							$charlength++;
						
							if ( mb_strlen( $author_description ) > $charlength ) {
								
								$subex = mb_substr( $author_description, 0, $charlength - 5 );
								$exwords = explode( ' ', $subex );
								$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
								if ( $excut < 0 ) {
									$author_description = mb_substr( $subex, 0, $excut );
								} else {
									$author_description = $subex;
								}
								$author_description .= '[...]';
							} 

						}
						
						apply_filters( 'woo_bookstore_author_widget', $author_description );
						
						if( $instance['auto_p'] == "yes" )
							echo wpautop( $author_description );
						else
							echo $author_description;
					?>
                    </p>
                <?php
				}                
			}
			
			echo $args['after_widget'];
			
		}
	}
	
	public function form( $instance ) {
		
		$title 				= __( 'Author Information', 'woo-bookstore' );
		$show_thumbnails 	= false;
		
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		
		if ( isset( $instance[ 'show_thumbnails' ] ) ) {
			$show_thumbnails = $instance[ 'show_thumbnails' ];
		}
		
		if ( isset( $instance[ 'auto_p' ] ) ) {
			$auto_p = $instance[ 'auto_p' ];
		}
		
		if ( isset( $instance[ 'excerpt' ] ) ) {
			$excerpt = $instance[ 'excerpt' ];
		}
		?>
        
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'woo-bookstore' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'excerpt' ); ?>"><?php _e( 'Excerpt Number:', 'woo-bookstore' ); ?></label> 
			<input placeholder="<?php _e( '-1 for unlimited', 'woo-bookstore' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'excerpt' ); ?>" name="<?php echo $this->get_field_name( 'excerpt' ); ?>" type="text" value="<?php echo esc_attr( $excerpt ); ?>">
		</p>
        <p>
			<input class="widefat" id="<?php echo $this->get_field_id( 'show_thumbnails' ); ?>" name="<?php echo $this->get_field_name( 'show_thumbnails' ); ?>" type="checkbox" <?php if( $show_thumbnails == 'yes' ): ?>CHECKED="CHECKED"<?php endif ?> value="yes">&nbsp;<label for="<?php echo $this->get_field_id( 'show_thumbnails' ); ?>"><?php _e( 'Show Thumbnail?', 'woo-bookstore' ); ?></label> 
		</p>
        <p>
        	<input id="<?php echo $this->get_field_id( 'auto_p' ); ?>" name="<?php echo $this->get_field_name( 'auto_p' ); ?>" type="checkbox" <?php if( $auto_p == 'yes' ): ?>CHECKED="CHECKED"<?php endif ?> value="yes">&nbsp;<label for=<?php echo $this->get_field_id( 'auto_p' ); ?>"><?php  _e( 'Automatically add paragraphs', 'woo-bookstore' ); ?></label>
        </p>
		<?php 
	}
	
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['title'] 				= ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['show_thumbnails'] 	= ( ! empty( $new_instance['show_thumbnails'] ) ) ? strip_tags( $new_instance['show_thumbnails'] ) : '';
		$instance['auto_p'] 			= ( ! empty( $new_instance['auto_p'] ) ) ? strip_tags( $new_instance['auto_p'] ) : '';	
		$instance['excerpt'] 			= ( ! empty( $new_instance['excerpt'] ) ) ? (int) $new_instance['excerpt'] : '-1';
		
		return $instance;
	
	}
	
}