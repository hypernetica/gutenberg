<?php
/**
 * Woo_Bookstore_Advanced_Search class.
 *
 * @class 		Woo_Bookstore_Advanced_Search
 * @version		1.0
 * @category	Class
 * @author 		WPini
 */
class Woo_Bookstore_Advanced_Search {
	
	public static function init() {	}
	
	public function __construct(){}
	
	/**
	 * Display Bookstore advanced search form
	 *
	 */
	public function show_search_form( $atts = array() ){
		
		$args = array(
		
					'advanced_search'	=> $this,
					'fields' 			=> $this->get_advanced_search_fields(),
					'search_terms'		=> $this->get_search_terms(),
					'atts'				=> $atts
					
				);
		
		$template_name = 'advanced-search.php';
		$wc_get_template = function_exists( 'wc_get_template' ) ? 'wc_get_template' : 'woocommerce_get_template';
		$wc_get_template( $template_name, $args, '', WPINI_WOO_BOOKSTORE_DIR . 'templates/bookstore/' );
		
	}
	
	/**
	 * Get search options
	 *
	 * @return array
	 */
	public function get_advanced_search_options(){
		
		$defaults = array();
		
		$book_taxonomies	= get_bookstore_taxonomies();
		
		if( ! empty( $book_taxonomies ) ){
			
			foreach( $book_taxonomies as $taxonomy ){
				
				$tax =  get_taxonomy( $taxonomy ) ;
				
				$defaults[$taxonomy]['name'] 			= $tax->labels->name;
				$defaults[$taxonomy]['label'] 			= __( 'Select','woo-bookstore' ).' '.$tax->labels->name.'...';
				$defaults[$taxonomy]['display_type'] 	= '';
				$defaults[$taxonomy]['enabled'] 		= 0;
				$defaults[$taxonomy]['class'] 			= '';
				$defaults[$taxonomy]['autocomplete']	= false;
				$defaults[$taxonomy]['type'] 			= 'taxonomy';
				
			}
			
		}
		
		$custom_fields 		= woo_book_get_custom_fields();
		if( ! empty( $custom_fields ) ){
			foreach( $custom_fields as $key => $field ){
				
				$defaults[$field['meta_key']]['name'] 			= $field['name'];
				$defaults[$field['meta_key']]['label'] 			= __( 'Select','woo-bookstore' ).' '.$field['name'].'...';
				$defaults[$field['meta_key']]['display_type'] 	= '';
				$defaults[$field['meta_key']]['enabled'] 		= 0;
				$defaults[$field['meta_key']]['class'] 			= '';
				$defaults[$field['meta_key']]['autocomplete']	= false;
				$defaults[$field['meta_key']]['type'] 			= 'meta';
				
			}
		}
		
		/**
		 * Add free text as option
		 *
		 */
		$defaults['wb_free_text']['name'] 			= __( 'Free text','woo-bookstore' );
		$defaults['wb_free_text']['label'] 			= __( 'Enter ISBN, Title, Code...','woo-bookstore' );
		$defaults['wb_free_text']['display_type'] 	= 'text';
		$defaults['wb_free_text']['enabled'] 		= 0;
		$defaults['wb_free_text']['class'] 			= '';
		$defaults['wb_free_text']['autocomplete']	= false;
		$defaults['wb_free_text']['type'] 			= 'free_text';
		
		$options = maybe_unserialize( get_option( 'woo_bookstore_advanced_search' ) );

		if( ! empty( $options ) ){
			
			foreach( $options as $key => $field ){
				if( isset( $defaults[$key] ) ){
					unset( $defaults[$key] );
					$defaults[$key] = $field;
				}
			}
			
		}
		
		return $defaults;
		
	}
	
	/**
	 * Return fields for advanced search
	 *
	 */
	public static function get_advanced_search_fields(){
		
		$fields  = array();
		
		$options = maybe_unserialize( get_option( 'woo_bookstore_advanced_search' ) );
		if( ! empty( $options ) ){
			foreach( $options as $key => $values ){
				if( isset( $values['enabled'] ) && $values['enabled'] )
					$fields[$key] = $values;
			}
		}
		
		return $fields;
			
	}
	
	/**
	 * Get field types
	 *
	 */
	public function get_field_types(){
		
		return array(
		
					/*'checkbox',*/
					'multiselect',
					'select',					
					'text',
					
				);
		
	}
	
	/** 
	 * Dropdown field types
	 *
	 * @param string $id
	 * @param string $type
	 * @param string $selected
	 * @param string $class
	 *
	 * @return HTML
	 */
	public function dropdown_field_types( $id, $type, $selected = "", $class = "" ){
		
		$field_types = $this->get_field_types();
		$html = '<select name="'.$id.'"'.( ! empty( $class ) ? 'class="'.$class.'"' : "" ).'>';
		foreach( $field_types as $ftype ){
			
			$disabled = "";
			if( $type == 'free_text' && ( $ftype == 'multiselect' || $ftype == 'select' ) )
				$disabled = 'DISABLED="DISABLED"';
			if( $type == 'meta' && $ftype == 'multiselect' )
				$disabled = 'DISABLED="DISABLED"';
				
			$html .= '<option '.$disabled.' '.( ( $selected == $ftype )? 'SELECTED="SELECTED"' : "" ).' value="'.$ftype.'">'.$ftype.'</option>';
		}
		$html .= '</select>';
		echo $html;
	}
	
	/**
	 * Display field's HTML output
	 * @param string $key the id of the term
	 * @param array $values 
	 * @param array $selected the values from search query
	 *
	 * @return mixed HTML
	 */	
	public function display_field_html( $key, $values, $selected = array() ){
		
		$html = "";
		$autocomplete_class = "";
		
		extract( $values );
		
		$args = array(
			
			'orderby'           => 'name', 
			'order'             => 'ASC',
			'hide_empty'        => true, 
			'fields'            => 'all', 
			'slug'              => ''
			
		); 
		
		$field_label = "";
		if( isset( $label ) )
			$field_label = apply_filters( 'woo_bookstore_custom_field_name', $label, 'advanced-search-field' );
		
		if( isset( $taxonomies ) )
			$terms = get_terms( $taxonomies, $args );
		
		if( isset( $autocomplete ) && $autocomplete )
			$autocomplete_class = " book-search-autocomplete ";//the class for autocomplete search
		
		switch( $display_type ){
			
			case 'select' :
				
				$html .= '<select id="type_'.$type.'" name="fields['.$type.']['.$key.']" class="woobookstore-search-field'.( isset( $class ) ? " ".$class : "" ).'">';
				$html .= '<option value="any">'.$field_label.'</option>';
				
				//if it is taxonomy get the values
				if( $type == 'taxonomy' ){
					
					$terms = get_terms( $key, apply_filters( 'book_advanced_search_'.$key.'_get_terms', $args ) );
					
					if( ! is_wp_error( $terms ) ){
						foreach ( $terms as $key => $term ){
							$html .= '<option value="'.$term->slug.'"'.( in_array( $term->slug, $selected )? ' SELECTED="SELECTED"' : "" ).'>'.$term->name.'</option>';	
						}
					}
					
				}elseif( $type == 'meta' ){
					
					$terms = $this->get_meta_terms( $key );
						
					if( ! empty( $terms ) ){
						foreach ( $terms as $key => $term ){
							$html .= '<option value="'.$key.'"'.( in_array( $term, $selected )? ' SELECTED="SELECTED"' : "" ).'>'.$term.'</option>';	
						}
					}
					
				}
				
				$html .= '</select>';
				break;
				
			case 'multiselect':
				
				$html .= '<select id="type_'.$type.'" multiple="multiple" name="fields['.$type.']['.$key.'][]" class="woobookstore-search-field'.( isset( $class ) ? " ".$class : "" ).'">';
				//if it is taxonomy get the values
				if( $type == 'taxonomy' ){
					$terms = get_terms( $key, apply_filters( 'book_advanced_search_'.$key.'_get_terms', $args ) );
				}
				$html .= '<option value="any">'.$field_label.'</option>';
				if( ! is_wp_error( $terms ) ){
					foreach ( $terms as $key => $term ){
						$html .= '<option value="'.$term->slug.'"'.( in_array( $term->slug, $selected )? ' SELECTED="SELECTED"' : "" ).'>'.$term->name.'</option>';	
					}
				}
				$html .= '</select>';
				break;
				
			
			case 'text':
				
				if( $type == 'free_text' ){
					
					$field_name ='fields['.$type.']';
					$selected 	= isset( $_GET['filter_keyword'] )? $_GET['filter_keyword'] : "" ;
					
				}else{
					
					$field_name = 'fields['.$type.']['.$key.']';
					$selected 	= array_shift( $selected );
					
				}
				
				$html .= '<input id="search_'.$key.'" type="text" name="'.$field_name.'" class="'.$autocomplete_class.'woobookstore-search-field'.( isset( $class ) ? " ".$class : "" ).'" placeholder="'.$field_label.'" value="'.$selected.'" />';				
				break;
			
		}
		
		echo apply_filters( 'woo_bookstore_html_field', $html, $key, $values );
		
	}
	
	/**
	 * Search for terms
	 *
	 * @param string $term e.g. 12345678
	 * @param string $term_name e.g. ISBN
	 * @param string $type whether it is a taxonomy or postmeta
	 *
	 * @return array $books
	 */
	public static function search_for_book_term( $term, $term_name, $term_type, $fields = array() ){
		
		$books 		= array();
		$do_search 	= false;//boolean to make a serach or not
		
		$autocomplete_book_num  = get_option( 'woobookstore_advanced_search_autocomplete_book_num' );
		$cache_enabled			= ( get_option( 'woobookstore_enable_cache' ) == 'yes' )? true : false;
		
		if( $cache_enabled ){
			
			$cache_id 	= self::build_cache_id( $term.$term_name.$term_type );//build a unique cache id	
			$books 		= maybe_unserialize( self::get_cache_results( $cache_id ) );
		
		}
				
		if( empty( $books ) ){
			
			$post__id = array();
			
			$meta_query = 
				array(
					array(
						'meta_key' 		=> '_visibility',
						'meta_value'	=> 'visible',
						'compare'		=> 'IN'
					)
				);
						
			$tax_query	= array(
							array(
								'taxonomy'	=> 'product_type',
								'field' 	=> 'slug',
								'terms' 	=> 'book'
							) 
						);
			
			$args = array(
						'post_type'		=> 'product',
						'post_status'	=> 'publish',
						'fields'		=> 'ids'
					);
			
			//Check if it is a free text search
			switch( $term_type ){
				
				case 'free_text':
					
					$post__in = self::free_text_search( $term );
					if( ! empty( $post__in )  )
						$do_search = true;
					
					$post_from_authors = self::search_authors_by_term( $term );
					if( ! empty( $post_from_authors )  ){
						$post__in = array_merge( $post__in, $post_from_authors );
						$do_search = true;	
					}
					
					break;
				
				case 'meta':
				
					// Search meta
					$do_search = true;
					$args['meta_key'] 		= $term_name;
					$args['meta_value'] 	= $term;
					$args['meta_compare'] 	= 'LIKE';
					
					break;
				
				case 'taxonomy':
					
					// Search taxonomy terms
					$taxonomy_terms = get_terms(
	
						array(
							'taxonomy' 		=> $term_name,
							'hide_empty' 	=> true,
							'name__like'	=> $term,
							'fields'		=> 'names'
						)
						
					);
					
					if( ! empty( $taxonomy_terms ) && ! is_wp_error( $taxonomy_terms ) ) {
						
						$do_search = true;
						array_push( 
							$tax_query, 
							array(
								'taxonomy'	=> $term_name,
								'field' 	=> 'name',
								'terms' 	=> $taxonomy_terms,
								'operator'	=> 'IN'
							) 
						);
					
					}
					
					break;
				
			}
			
			if( $do_search ){
				
				$args['meta_query']		= $meta_query;
				$args['tax_query']		= $tax_query;
				$args['posts_per_page']	= $autocomplete_book_num;
				
				if( ! empty( $post__in ) ) 
					$args['post__in']	= $post__in;							
				
				$query_books = new WP_Query( apply_filters( 'woo_bookstore_query_args', $args ) );
				
				if( $query_books->found_posts ){
					foreach ( $query_books->posts as $post_id ){
						
						$authors = implode( ',', wp_get_post_terms( $post_id, 'book_author', array( 'fields' => 'names' ) ) );
						
						$post_title = get_the_title( $post_id ).' ( '.$authors.' )';
						$permalink	= get_permalink( $post_id );
						
						array_push( $books, 
							array(
								'label' 	=> $post_title,
								'value'		=> $post_title,
								'url'		=> $permalink,
								'category'	=> __( "Books", 'woo-bookstore' )
							)
						);
					}
				}
				
				if( $term_type == "free_text" ) {
					
					if( get_option( 'woobookstore_advanced_search_autocomplete_group' ) == "yes" ) {
					
						$group_results = self::get_autocomplete_group_results( $term, $term_name, $term_type, $fields, $autocomplete_book_num );
						
						if( ! empty( $group_results ) ) {
							
							$books = array_merge( $books, $group_results );
						
						}
					
					}
				
				}
				
			}
			
			if( $cache_enabled )
				self::push_to_cache( $cache_id, $books );
			
		}
		
		return $books;
		
	}
	
	/**
	 * Group results by book taxonomies
	 *
	 * @param string $term e.g. 12345678
	 * @param string $term_name e.g. ISBN
	 * @param string $type whether it is a taxonomy or postmeta
	 *
	 * @return array $group_results
	 */
	public static function get_autocomplete_group_results( $term, $term_name, $term_type , $fields ) {
		
		$group_results = woo_bookstore_get_autocomplete_group_results( $term, $term_name, $term_type, $fields, $autocomplete_book_num );
		
		return $group_results;
			
	}
	
	/**
	 * Get results from cache
	 *
	 * @param int $cache_id the cache_id to find the query
	 *
	 * @return array $results
	 */
	public static function get_cache_results( $cache_id ){
		
		global $wpdb;
		
		$cache_type		= self::get_cache_type();
		$seconds		= self::get_cache_lifetime();
		
		$results = array();
		
		switch( $cache_type ){
			
			case "file":
			
				$cache_file = self::get_cache_folder( $cache_id );
				
				if( file_exists( $cache_file ) ){
					
					//get file maketime
					$today  		= strtotime( date( 'Y-m-d H:i:s' ) );
					$cache_date 	= filemtime( $cache_file );
					$difference 	= abs( $today - $cache_date );
					
					if( $difference <= $seconds ){
						
						$file_contents = "";
						
						//read file
						$fh = fopen( $cache_file, "r" );
						while ( ! feof( $fh ) ) {
						   $line = fgets($fh);
						   $file_contents .= $line;
						}
						
						fclose( $fh );
						
						$results = maybe_unserialize( $file_contents );
						
					}
					
				}
				break;
				
			default:
				
				$sql = "SELECT `results`, `datetime` FROM `{$wpdb->prefix}woobookstore_cache` WHERE `cache_id` = %s";
				
				$row = $wpdb->get_row( 
							$wpdb->prepare( $sql, $cache_id ), 
							ARRAY_A 
						);
				
				if( $wpdb->num_rows ){
					
					$today  		= strtotime( date( 'Y-m-d H:i:s' ) );
					$cache_date 	= $row['datetime'];
					$difference 	= abs( $today - $cache_date );
					
					if( $difference <= $seconds ){
						$results = maybe_unserialize( $row['results'] );
					}
								
				}
				break;
					
		}
				
		return $results;
		
	}
	
	/**
	 * Build a unique id for the cache
	 *
	 * @param array $data
	 *
	 */
	public static function build_cache_id( $data ) {
    	
		$data = strtolower( $data );
		$data = str_replace( ' ','', $data );
		
		$chars = array( 
			'a', 'b', 'c', 'd', 'e','f', 'g', 'h', 'i', 'j','k', 'l', 'm', 'n', 'o','p', 'q', 'r', 's', 't','u', 'v', 'w', 'x', 'y','z', '1', '2', '3', '4',
			'5', '6', '7', '8', '9', '0', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '+', '=', ',', '<', '.', '>', '?', '/', '€'
		);
		
		$alpha_flip 	= array_flip($chars);
		$return_value 	= "";
		$length 		= strlen( $data );
		
		for ( $i = 0; $i < $length; $i++ ) {
			if( isset( $alpha_flip[$data[$i]] ) )
				$return_value .= $alpha_flip[$data[$i]];
		}
		
		$return_value = md5( str_replace( '-','', $return_value ).NONCE_SALT );
		$return_value = filter_var( $return_value, FILTER_SANITIZE_NUMBER_INT );
		
		if( strlen( $return_value > 30 ) )
			return substr( $return_value, 0, 30 );
		else
			return $return_value;
		
	}
	
	/**
	 * push_to_cache
	 *
	 * @param string $cache_id
	 * @param mixed $results the data to cache
	 *
	 */
	public static function push_to_cache( $cache_id, $results ){
		
		global $wpdb;
		
		$cache_type		= self::get_cache_type();
		$cache_lifetime = self::get_cache_lifetime();
		
		switch( $cache_type ){
			
			case "file":
				
				$cache_file = self::get_cache_folder( $cache_id );
				
				$fh = fopen( $cache_file, "w+" );
				fwrite( $fh, serialize( $results ) );
				fclose( $fh );
				
				break;
			
			default:
				
				$result = $wpdb->update( 
					$wpdb->prefix.'woobookstore_cache', 
					array( 
						'results' 	=> serialize( $results ), 
						'datetime' 	=> strtotime( date( 'Y-m-d H:i:s', strtotime( "+".$cache_lifetime." seconds" ) ) )
					), 
					array(
						'cache_id' 	=> $cache_id
					),
					array( 
						'%s', 
						'%s' 
					),
					array( '%s' )
				);
				
				if( empty( $result ) ){
					
					$wpdb->insert( 
						$wpdb->prefix.'woobookstore_cache', 
						array( 
							'id'		=> NULL,
							'cache_id'	=> $cache_id,
							'results' 	=> serialize( $results ), 
							'datetime' 	=> strtotime( date( 'Y-m-d H:i:s' , strtotime( "+".$cache_lifetime." seconds" ) ) ) 
						), 
						array( 
							'%d',
							'%s',
							'%s', 
							'%s' 
						) 
					);
				
				}
				break;
				
		}
	}
	
	/**
	 * Get cache lifetime in seconds
	 * 
	 * @return int
	 */
	public static function get_cache_lifetime(){
		
		$cache_lifetime = get_option( 'woobookstore_cache_lifetime' ) ;
		$seconds		= ( empty( $cache_lifetime ) )? 300 : $cache_lifetime;
		
		return $seconds;
		
	}
	
	/**
	 * Get cache type
	 *
	 * @return string db or file
	 */
	public static function get_cache_type(){
		
		$cache_type = get_option( 'woobookstore_cache_type' );
		$cache_type		= ( empty( $cache_type ) )? "db" : $cache_type;
		
		return $cache_type;
		
	}
	
	/** 
	 * Get cache folder
	 *
	 * @param int cache_id
	 *
	 * @return str path to file
	 */
	public static function get_cache_folder( $cache_id = "" ){
		
		if( ! is_dir( ABSPATH.'wp-content/cache' ) )
			mkdir( ABSPATH.'wp-content/cache' );
		
		if( ! is_dir( ABSPATH.'wp-content/cache/woo-bookstore' ) )
			mkdir( ABSPATH.'wp-content/cache/woo-bookstore' );
		
		$multisite = '';
		
		if( WPINI_WOO_BOOKSTORE_IS_MULTISITE ) {
			
			$blog_id 	= get_current_blog_id();
			$multisite 	= '/'.$blog_id;
		}
		
		$book_cache_folder = ABSPATH.'wp-content/cache/woo-bookstore'.$multisite;
		if( ! is_dir( $book_cache_folder ) ){
			
			mkdir( $book_cache_folder );
				
		}
		
		if( ! file_exists( $book_cache_folder."/.htaccess" ) ){
		
			$htaccess = "#Secure folder".PHP_EOL;
			$htaccess .= "Order deny,allow".PHP_EOL;
			$htaccess .= "Deny from all".PHP_EOL;
			$htaccess .= "Allow from 127.0.0.1";
			
			$fh = fopen( $book_cache_folder."/.htaccess", "w+" );
			fwrite( $fh, $htaccess );
			fclose( $fh );
			
			chmod( $book_cache_folder."/.htaccess", 0644 );   
			
		}
				
		if( empty( $cache_id ) ){
			
			return $book_cache_folder."/";
			
		}else{
			
			return $book_cache_folder.'/'.$cache_id.'.txt';
				
		}
		
	}
	
	/**
	 * Get query terms from search if any
	 *
	 * @return array
	 */
	public function get_search_terms(){
		
		$search_terms = array();
		
		if( isset( $_GET['wb_search'] ) ){
			
			if( $_GET['wb_search'] == 'books' ){
				
				$query_terms = $_GET;
				unset( $query_terms['wb_search'] );
				
				//return an array of book fields and their types e.g. taxonomy or postmeta
				$field_types = get_option( 'woo_bookstore_advanced_search_field_types' );
				if( ! empty( $field_types ) )			
					$field_types = maybe_unserialize( $field_types );
				
				foreach( $query_terms as $term_key => $values ){
					
					if( preg_match( '/filter_/', $term_key ) ){
						
						$term_key 	= str_replace( 'filter_', '', $term_key );
						$term_type 	= isset( $field_types[$term_key] ) ? $field_types[$term_key] : $term_key;
						$terms 		= explode( ',', $values );
						$search_terms[$term_key] = 
							array(
								'type' 	=> $term_type,
								'terms' => $terms
							);
					}
					
				}
				
			}
		}
		
		return $search_terms;
		
	}
	
	/**
	 * Free text book search
	 *
	 * @param string $s
	 *
	 * @return array post_ids
	 */
	public static function free_text_search( $s ){
		
		$posts = array();
		$post__not_in = array();
		
		$term = esc_sql( $s );
		
		$meta_query = 
			array(
				array(
					'meta_key' 		=> '_visibility',
					'meta_value'	=> 'visible',
					'compare'		=> 'IN'
				)
			);
					
		$tax_query	= array(
						array(
							'taxonomy'	=> 'product_type',
							'field' 	=> 'slug',
							'terms' 	=> 'book'
						) 
					);
		
		$args  = array(
		
					's'				=>  $term,
					'post_type' 	=> 'product',
					'status'		=> 'publish',
					'meta_query'	=> $meta_query,
					'tax_query'		=> $tax_query,
					'fields'		=> 'ids',
					'posts_per_page'	=> "-1"
					
				);
		
		$books = new WP_Query( apply_filters( 'woo_bookstore_query_args', $args ) );
		if( $books->found_posts ){
			
			$posts = array_merge( $posts, $books->posts );
			$post__not_in = array_merge( $post__not_in, $books->posts );
		
		}
		
		// Search meta as well		
		$custom_fields 	= woo_book_get_custom_fields();
		$post_meta 		= array();
		
		if( ! empty( $custom_fields ) ){
			
			foreach( $custom_fields as $key => $values ){
				
				$meta_query = 
					array(
						array(
							'meta_key' 		=> '_visibility',
							'meta_value'	=> 'visible',
							'compare'		=> 'IN'
						)
					);
				
				array_push(
					$meta_query, 
					array( 
						'key'     => $values['meta_key'],
						'value'   => $term,
						'compare' => 'LIKE',
					)
				);
				
				$args  = array(
						
							'post_type' 		=> 'product',
							'status'			=> 'publish',
							'meta_query'		=> $meta_query,
							'tax_query'			=> $tax_query,
							'post__not_in'		=> $post__not_in,
							'fields'			=> 'ids',
							'posts_per_page'	=> "-1"
							
						);
				
				$books = new WP_Query( apply_filters( 'woo_bookstore_query_args', $args ) );
				if( $books->found_posts ) {
					
					$posts = array_merge( $posts, $books->posts );
					$post__not_in = array_merge( $post__not_in, $books->posts );
					
				}
				
			}
			
		}
		
		return $posts;
		
	}
	
	/**
	 * Search book authors when a Free Text is given
	 *
	 * @param string $s
	 *
	 * @return array post_ids
	 */
	public static function search_authors_by_term( $s ){
		
		global $wpdb;
		
		$ids 		= array();
		$term_ids 	= array();
		
		$sql = "SELECT `term_id` FROM `{$wpdb->prefix}terms` WHERE `name` LIKE %s AND `term_id` IN ( SELECT `term_id` FROM `{$wpdb->prefix}term_taxonomy` WHERE `taxonomy` IN ('book_author'))";
		
		$term_ids = $wpdb->get_col( $wpdb->prepare( $sql, '%'.$s.'%' ) );
		
		if( ! empty( $term_ids ) ){
			
			$args  = array(
					
						'post_type' 		=> 'product',
						'status'			=> 'publish',
						'meta_query'		=> array(
							array(
								'meta_key' 		=> '_visibility',
								'meta_value'	=> 'visible',
								'compare'		=> 'IN'
							)
						),
						'tax_query'			=> array(
							array(
								'taxonomy' 	=> 'book_author',
								'field'		=> 'term_id',
								'terms'		=> $term_ids
							)
						),
						'posts_per_page'	=> '-1',
						'fields'			=> 'ids'
						
					);
			
			$results = new WP_Query( apply_filters( 'woo_bookstore_query_args', $args ) );
			if( $results->found_posts > 0 )
				$ids = $results->posts;
				
		}
		
		return $ids;
		
	}
	
	/**
	 * Get all terms for a meta field
	 *
	 * @param string $field
	 *
	 * @return array $terms
	 */
	public function get_meta_terms( $field ){
		
		global $wpdb;
		
		$sql = "SELECT DISTINCT `meta_value` FROM `{$wpdb->prefix}postmeta` WHERE `meta_key` = %s";
		
		$data = 
			$wpdb->get_results(
				$wpdb->prepare(
					$sql,
					$field
				)
			);
		
		if( ! empty( $data ) ){
			foreach( $data as $key => $row ){
				$terms[$row->meta_value] = $row->meta_value;
			}
		}
		
		
		return apply_filters( 'book_advanced_search_'.$field.'_get_terms', $terms );
		
	}
	
	/**
	 * Return book ids from search parameters
	 *
	 */
	public function get_book_ids_from_search(){
		
		$ids = array();
		
		$search_params = $this->get_search_terms();
		
		$cache_enabled	= ( get_option( 'woobookstore_enable_cache' ) == 'yes' )? true : false;
		$paged 			= ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		
		$lang = "";
		if( defined( 'ICL_LANGUAGE_CODE' ) )
			$lang = ICL_LANGUAGE_CODE;
		
		if( ! empty( $search_params ) ){
			
			$terms_str = "";
			
			foreach( $search_params as $key => $data ){
				
				if( isset( $data['terms'] ) )
					$terms_str .= implode( ',', $data['terms'] );
				
			}
			
		}
		
		if( $cache_enabled ){
			
			$cache_id 	= Woo_Bookstore_Advanced_Search::build_cache_id( $lang.$paged.$terms_str );
			$books 		= Woo_Bookstore_Advanced_Search::get_cache_results( $cache_id );
			
		}
		
		if( !empty( $books ) ){
			
			$ids = $books->posts;
			
		}else{
			
			if( ! empty( $search_params ) ){
				
				$do_search = false;
				
				$data = 
					array(
						'meta' 		=> array(),
						'taxonomy'	=> array(),
						'free_text'	=> ""
					);
				
				foreach( $search_params as $key => $data ){
					
					switch( $data['type'] ){
						
						case 'taxonomy':
							$data['taxonomy'][$key] = $data['terms'];
							break;
						
						case 'meta':
							$data['meta'][$key]	= $data['terms'];
							break;
						
						case 'keyword':
							
							$data['free_text'] = array_shift( $data['terms'] );
							break;
						
					}
					
				}
				
				$meta_query = 
					array(
						array(
							'meta_key' 		=> '_visibility',
							'meta_value'	=> 'visible',
							'compare'		=> 'IN'
						)
					);
							
				$tax_query	= array(
								array(
									'taxonomy'	=> 'product_type',
									'field' 	=> 'slug',
									'terms' 	=> 'book'
								) 
							);
				
				$post__in = array();
				
				//build taxonomy query			
				if( isset( $data['taxonomy'] ) && ! empty( $data['taxonomy'] ) ){
					
					foreach( $data['taxonomy'] as $taxonomy => $values ){
						
						if( ! is_array( $values ) )
							$values = array( $values );
						
						foreach( $values as $val ){
							
							if( !empty( $val ) && $val != 'any' ){
								$do_search = true;
								array_push( 
									$tax_query, 
									array(
										'taxonomy'	=> $taxonomy,
										'field' 	=> 'slug',
										'terms' 	=> $val,
										'operator'	=> 'IN'
									)
								);
								
							}
						}
							
						
					}
				}
				
				//build meta query	
				if( isset( $data['meta'] ) && ! empty( $data['meta'] ) ) {
					
					foreach( $data['meta'] as $meta => $values ){
						
						if( ! is_array( $values ) )
							$values = array( $values );
						
						foreach( $values as $val ){
							
							if( !empty( $val ) && $val != 'any' ){
								$do_search = true;
								array_push( 
									$meta_query, 
									array(
										'key'		=> $meta,
										'value' 	=> $val,
										'compare' 	=> 'LIKE'
									) 
								);
								
							}
						}
						
					}
					
				}
				
				//free text search
				if( isset( $data['free_text'] ) && ! empty( $data['free_text'] ) ) {
					
					$post__in = Woo_Bookstore_Advanced_Search::free_text_search( $data['free_text'] );
					if( ! empty( $post__in )  )
						$do_search = true;
					
				}
				
				if( $do_search ){
					
					$args = array(
								'post_type'			=> 'product',
								'post_status'		=> 'publish',
								'meta_query'		=> $meta_query,
								'tax_query'			=> $tax_query,
								'posts_per_page'	=> $per_page,
								'paged'				=> $paged,
								'post__in'			=> $post__in,
								'fields'			=> 'ids',
								'posts_per_page'	=> '-1'
							);
					
					$books 	= new WP_Query( apply_filters( 'woo_bookstore_query_args', $args ) );
					$ids 	= $books->posts;
				}
				
				if( $cache_enabled )
					Woo_Bookstore_Advanced_Search::push_to_cache( $cache_id, $books );
					
			}
		
		}
				
		return $ids;
		
	}
		
}