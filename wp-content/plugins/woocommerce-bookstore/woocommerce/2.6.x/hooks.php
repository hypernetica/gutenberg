<?php
/**
 * WooCommerce hooks
 *
 * TABLE OF CONTENTS
 *
 * wpini_woo_bookstore_add_product_type_book()
 * wwb_create_custom_product_type_book()
 * woo_bookstore_filter_related_products()
 * woo_bookstore_after_setup_theme_modify_query()
 * woo_bookstore_filter_products()
 * woo_bookstore_query_args_search_everywhere()
 * woo_bookstore_query_args_stock()
 * woo_bookstore_get_autocomplete_group_results()
 *
 */
add_filter( 'product_type_selector', 'wpini_woo_bookstore_add_product_type_book' );
function wpini_woo_bookstore_add_product_type_book( $types ){
    $types[ 'book' ] = __( 'Book', 'woo-bookstore' );
    return $types;
}

add_action( 'plugins_loaded', 'wwb_create_custom_product_type_book' );
function wwb_create_custom_product_type_book(){
    
	require_once( 'classes/wb-product-type-book.php' );
	
}

/**
 * Related Product Query
 *
 */
add_filter( 'woocommerce_related_products_args', 'woo_bookstore_filter_related_products' );

function woo_bookstore_filter_related_products( $query_args ){
	
	if( "yes" === get_option( 'woobookstore_filter_related_products' ) ) {
		
		global $post;
		
		$post__in = array();
		
		$book_author = wp_get_post_terms( $post->ID, 'book_author', array( 'fields' => 'ids' ) );
		
		$args = 
			array(
				'post_type' 	=> 'product',
				'post_status'	=> 'publish',
				'meta_query'	=> 
					array(
						array(
							'meta_key' 		=> '_visibility',
							'meta_value'	=> 'visible',
							'compare'		=> 'IN'
						)
					),
				'fields'		=> 'ids',
				'post__not_in'	=> array( $post->ID )
			
			);
			
		if ( ! empty( $book_author ) ){
			
			$args['tax_query']
				= array(
					array(
						'taxonomy'	=> 'product_type',
						'field' 	=> 'slug',
						'terms' 	=> 'book'
					),
					array(
						'taxonomy'	=> 'book_author',
						'field' 	=> 'term_id',
						'terms' 	=> $book_author
					)
				);
			
			$query = new WP_Query( $args );
			if( $query->found_posts )
				$post__in = array_merge( $post__in, $query->posts );
			
					
		}
		
		if( empty( $post__in ) ){
			
			$book_publisher = wp_get_post_terms( $post->ID, 'book_publisher', array( 'fields' => 'ids' ) );
			
			if ( ! empty( $book_publisher ) ){
				
				$args['tax_query']
					= array(
						array(
							'taxonomy'	=> 'product_type',
							'field' 	=> 'slug',
							'terms' 	=> 'book'
						),
						array(
							'taxonomy'	=> 'book_publisher',
							'field' 	=> 'term_id',
							'terms' 	=> $book_publisher
						)
					);
				
				$query = new WP_Query( $args );
				if( $query->found_posts )
					$post__in = array_merge( $post__in, $query->posts );
					
			}
		}
		
		if( empty( $post__in ) ){
			
			$book_category 	= wp_get_post_terms( $post->ID, 'book_category', array( 'fields' => 'ids' ) );
			
			if ( ! empty( $book_category ) ){
				
				$args['tax_query']
					= array(
						array(
							'taxonomy'	=> 'product_type',
							'field' 	=> 'slug',
							'terms' 	=> 'book'
						),
						array(
							'taxonomy'	=> 'book_category',
							'field' 	=> 'term_id',
							'terms' 	=> $book_category
						)
					);
				
				$query = new WP_Query( $args );
				if( $query->found_posts )
					$post__in = array_merge( $post__in, $query->posts );
					
			}
		}
		
		if( ! empty( $post__in ) ){
			
			$query_args['post__in'] 	= $post__in;
					
		}
	
	} // if( "yes" === get_option( 'woobookstore_filter_related_products' ) )
	
	return $query_args;
	
}

/**
 * modify pre_get_post
 *
 */
add_action( 'after_setup_theme', 'woo_bookstore_after_setup_theme_modify_query' );

function woo_bookstore_after_setup_theme_modify_query(){
	add_action( 'pre_get_posts', 'woo_bookstore_filter_products' );
}

function woo_bookstore_filter_products( $query ) {	
    
	if ( ! is_admin() && $query->is_main_query() && woo_bookstore_is_search() && is_shop() ) {	
		
		$search_everywhere = ( get_option( 'woobookstore_search_everywhere' ) === "yes" );
		
		if( $search_everywhere ){
			
			$advanced_search 	= new Woo_Bookstore_Advanced_Search;
			$post__in 			= $advanced_search->get_book_ids_from_search();
			
			if( ! empty( $post__in ) ){			
				
				$query->set( 'post__in', $post__in );				
												
			}else{
				
				//simply produce a not found page
				$query->set( 'post_type', 'post' );	
					
			}
			
		}
		
		
	} 
	
}

add_filter( 'woo_bookstore_query_args', 'woo_bookstore_query_args_search_everywhere', 10, 1 );
function woo_bookstore_query_args_search_everywhere( $args ){
	
	$search_everywhere = ( get_option( 'woobookstore_search_everywhere' ) === "yes" );
	
	if( $search_everywhere && woo_bookstore_is_search() ){
		
		$advanced_search 	= new Woo_Bookstore_Advanced_Search;
		$post__in 			= $advanced_search->get_book_ids_from_search();
		
		if( ! empty( $post__in ) ){			
			
			$args['post__in'] = $post__in;
			
		
		}else{

			//simply produce a not found page
			$args['post_type'] = 'post';
				
		}
		
	}
	
	return $args;
	
}

add_filter( 'woo_bookstore_query_args', 'woo_bookstore_query_args_stock', 11, 1 );
function woo_bookstore_query_args_stock( $args ) {
	
	if( get_option( 'woocommerce_hide_out_of_stock_items' ) === "yes" ) {
		
		$args['meta_key'] 		= '_stock_status';
		$args['meta_value']   	= 'instock';
		$args['meta_compare'] 	= 'IN';
	
	}
	
	return $args;
	
}

/** 
 * Group results in autocomplete dropdown(hookable)
 *
 * @param string $term e.g. 12345678
 * @param string $term_name e.g. ISBN
 * @param string $term_type whether it is a taxonomy or postmeta
 * @param array $fields
 * @param int $autocomplete_book_num
 *
 * @return array $group_results
 */

if( ! function_exists( 'woo_bookstore_get_autocomplete_group_results' ) ) {
function woo_bookstore_get_autocomplete_group_results( $term, $term_name, $term_type, $fields, $autocomplete_book_num  ) {
	
	$group_results 		= array();		
	$taxonomies 		= get_option( 'woobookstore_advanced_search_autocomplete_taxonomies' );	
	$number_of_terms 	= ( $autocomplete_book_num > 0 ? $autocomplete_book_num : 0 );
	
	if( ! empty( $taxonomies ) ) {
		
		foreach( $taxonomies as $key => $book_taxonomy ) {
			
			switch( $book_taxonomy ) {
				
				case 'book_category':
					
					$taxonomy_terms = 
						get_terms( 
							'book_category', 
							array(
								'hide_empty' 	=> true,
								'name__like' 	=> $term,
								'number' 		=> $number_of_terms
							) 
						);
					
					if( !is_wp_error( $taxonomy_terms ) && ! empty( $taxonomy_terms ) ) {
					
						foreach( $taxonomy_terms as $key => $the_term ) {
							
							array_push(
				
								$group_results,
								array(
									'label' 	=> $the_term->name,
									'value'		=> $the_term->name,
									'url'		=> get_term_link( $the_term, 'book_category' ),
									'category'	=> __( "Book Categories", 'woo-bookstore' )
								)
								
							);
						
						}
						
					}
					
					break;
					
				case 'book_author':
					
					$taxonomy_terms = 
						get_terms( 
							'book_author', 
							array(
								'hide_empty' 	=> true,
								'name__like' 	=> $term,
								'number' 		=> $number_of_terms
							) 						
						);
					
					if( !is_wp_error( $taxonomy_terms ) && ! empty( $taxonomy_terms ) ) {
					
						foreach( $taxonomy_terms as $key => $the_term ) {
							
							array_push(
				
								$group_results,
								array(
									'label' 	=> $the_term->name,
									'value'		=> $the_term->name,
									'url'		=> get_term_link( $the_term, 'book_author' ),
									'category'	=> __( "Book Authors", 'woo-bookstore' )
								)
								
							);
						
						}
						
					}
					
					break;
					
				case 'book_publisher':
					
					$taxonomy_terms = 
						get_terms( 
							'book_publisher', 
							array(
								'hide_empty' 	=> true,
								'name__like' 	=> $term,
								'number' 		=> $number_of_terms
							) 
						);
					
					if( !is_wp_error( $taxonomy_terms ) && ! empty( $taxonomy_terms ) ) {
					
						foreach( $taxonomy_terms as $key => $the_term ) {
							
							array_push(
				
								$group_results,
								array(
									'label' 	=> $the_term->name,
									'value'		=> $the_term->name,
									'url'		=> get_term_link( $the_term, 'book_publisher' ),
									'category'	=> __( "Book Publishers", 'woo-bookstore' )
								)
								
							);
						
						}
						
					}
					
					break;
				
			}
			
		}
		
	}
	
	return $group_results;
	
}
}