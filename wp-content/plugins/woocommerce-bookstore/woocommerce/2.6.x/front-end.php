<?php
/**
 * Front End Scripts
 *
 * TABLE OF CONTENTS
 *
 * woo_bookstore_tab()
 * woo_bookstore_tab_content()
 * woo_bookstore_display_book_details()
 *
 */

if( woo_bookstore_show_book_details_in_tab() ){
	
	add_filter( 'woocommerce_product_tabs', 'woo_bookstore_tab', 98 );
	
}else{
	
	$book_details_action 	= get_option( 'woo_book_details_action' );
	$valid_actions 			= get_valid_actions_for_book_details();
	
	if( ! empty( $book_details_action ) ){
	
		if( $book_details_action != 'none' && array_key_exists( $book_details_action, $valid_actions ) ){
		
			add_action( $book_details_action, 'woo_bookstore_display_book_details' );
		
		}
	}
	
}

function woo_bookstore_tab( $tabs ) {
	
	global $product;
	
	if( $product->product_type == 'book'  ){
		
		$tabs['book_info'] = array(
			'title' 	=>	__( 'Book Information', 'woo-bookstore' ),
			'priority' 	=> 	woo_bookstore_get_book_details_tab_priority(),
			'callback' 	=> 'woo_bookstore_tab_content'
		);
		
	}
	
	return $tabs;
 
}

function woo_bookstore_tab_content() {
 	
	global $product;
	$book = new WC_Product_Book( $product->get_id() );	
		
	$book_data 	= array();
	$count = 0;
	
	$custom_fields = woo_book_get_custom_fields();
	
	foreach( $custom_fields as $field ){
		if( isset( $field['visible'] ) && $field['visible'] == 'yes' ){
			
			$book_data[$count] = array(
									'title' => apply_filters( 'woo_bookstore_custom_field_name', $field['name'], 'book-field' ),
									'value' => $book->{$field['meta_key']},
									'class' => str_replace( ',', ' ', $field['class'] )
								);
			$count++;	
		}
		
	}
	
	if( !empty( $book ) ){
		
		$args = array(
					'book'					=> $book,
					'book_data' 			=> $book_data,
					'book_custom_fields' 	=> $custom_fields
				);
		
		$wc_get_template = function_exists('wc_get_template') ? 'wc_get_template' : 'woocommerce_get_template';
		$wc_get_template( 'single-product/tabs/tab-book.php', $args, '', WPINI_WOO_BOOKSTORE_DIR . 'templates/' );
	
	}
	
}

function woo_bookstore_display_book_details( $product_id ) {
 	
	if( empty( $product_id ) ){
		
		global $product;
		$book = new WC_Product_Book( $product->get_id() );	
		
	}else{
		
		$book = new WC_Product_Book( $product_id );
		
	}
	
	$book_data 	= array();
	$count = 0;
	
	$custom_fields = woo_book_get_custom_fields();
	
	foreach( $custom_fields as $field ){
		if( $field['visible'] == 'yes' ){
			
			$book_data[$count] = array(
									'title' => __( $field['name'], 'woo-bookstore' ),
									'value' => $book->{$field['meta_key']},
									'class' => str_replace( ',', ' ', $field['class'] )
								);
			$count++;	
		}
		
	}
	
	if( !empty($book) ){
		
		$args = array(
					'book'					=> $book,
					'book_data' 			=> $book_data,
					'book_custom_fields' 	=> $custom_fields
				);
		
		$wc_get_template = function_exists('wc_get_template') ? 'wc_get_template' : 'woocommerce_get_template';
		$wc_get_template( 'single-product/tabs/tab-book.php', $args, '', WPINI_WOO_BOOKSTORE_DIR . 'templates/' );
	
	}
	
}

/**
 * Author
 *
 */
if( woo_bookstore_single_display_author_bio() && woo_boostore_display_author_bio_as_tab() ){
	
	add_filter( 'woocommerce_product_tabs', 'woo_bookstore_author_tab', 99 );
	
}

function woo_bookstore_author_tab( $tabs ) {
	
	global $product;
	
	if( $product->product_type == 'book'  ){
		
		$tabs['author_info'] = array(
			'title' 	=>	__( 'Author', 'woo-bookstore' ),
			'priority' 	=> 	woo_bookstore_get_author_details_tab_priority(),
			'callback' 	=> 'woo_bookstore_author_tab_content'
		);
		
	}
	
	return $tabs;
 
}

function woo_bookstore_author_tab_content(){
	
	$args = array();
	
	$wc_get_template = function_exists('wc_get_template') ? 'wc_get_template' : 'woocommerce_get_template';
	$wc_get_template( 'single-product/tabs/tab-author.php', $args, '', WPINI_WOO_BOOKSTORE_DIR . 'templates/' );
	
}

/**
 * Author
 *
 */
if( woo_bookstore_single_display_publisher_desc() && woo_boostore_display_publisher_desc_as_tab() ){
	
	add_filter( 'woocommerce_product_tabs', 'woo_bookstore_publisher_tab', 99 );
	
}

function woo_bookstore_publisher_tab( $tabs ) {
	
	global $product;
	
	if( $product->product_type == 'book'  ){
		
		$tabs['publisher_info'] = array(
			'title' 	=>	__( 'Publisher', 'woo-bookstore' ),
			'priority' 	=> 	woo_bookstore_get_publisher_description_tab_priority(),
			'callback' 	=> 'woo_bookstore_publisher_tab_content'
		);
		
	}
	
	return $tabs;
 
}

function woo_bookstore_publisher_tab_content(){
	
	$args = array();
	
	$wc_get_template = function_exists('wc_get_template') ? 'wc_get_template' : 'woocommerce_get_template';
	$wc_get_template( 'single-product/tabs/tab-publisher.php', $args, '', WPINI_WOO_BOOKSTORE_DIR . 'templates/' );
	
}