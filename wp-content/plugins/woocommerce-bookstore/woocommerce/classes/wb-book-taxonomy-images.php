<?php
/**
 * WPini_Book_Taxonomy_Image Class
 *
 * Add images to book taxonomies
 *
 * @class 		WPini_Book_Taxonomy_Image
 * @package		WooCommerce Bookstore
 * @category	Class
 * @since		1.0
 *
 */
class WPini_Book_Taxonomy_Image{
	
	private static $instance;
	
	var $book_taxonomies 			= array();
	var $current_admin_taxonomy 	= '';
	
	public static function init() {
		
		if (self::$instance == null) {
			self::$instance = new WPini_Book_Taxonomy_Image();			
		}
		
	}
	
	public function __construct() {
		
		//register image dimensions
		$this->register_taxonomy_image_sizes();
		
		if( is_admin() ){
			
			/**
			 * Register custom field for image dimensions. This field is going to be used here Woocommerce > Settings > Bookstore 
			 *
			 */
			if ( ! has_action( 'woocommerce_admin_field_woo_bookstore_image_width' ) ) {
				add_action( 'woocommerce_admin_field_woo_bookstore_image_width', array( $this, 'admin_fields_woo_bookstore_image_width' ) );
			}
			
			$this->book_taxonomies = get_bookstore_taxonomies();
			$taxonomies = $this->book_taxonomies;
			
			add_action( 'admin_enqueue_scripts', array( $this, 'assets' ) );
			
			if( !empty($taxonomies) ){
				
				foreach( $taxonomies as $taxonomy ){
					
					add_action( $taxonomy.'_add_form_fields', array( $this, 'wpini_add_thumbnail_field' ) );
					add_action( $taxonomy.'_edit_form_fields', array( $this, 'wpwini_edit_thumbnail_field' ), 10, 2 );
					add_action( 'created_term', array( $this, 'wpini_thumbnail_field_save' ), 10, 3 );
					add_action( 'edit_term', array( $this, 'wpini_thumbnail_field_save' ), 10, 3 );
									
				}
			}
			
		}
	}
	
	/**
	 * admin_fields_woo_bookstore_image_width
	 *
	 *
	 */
	public function admin_fields_woo_bookstore_image_width( $value ){
		
		$option = maybe_unserialize( get_option( $value['id']  ) );
		
		$width 	= ( isset( $option['width'] ) ?  $option['width'] : $value['default']['width'] );
        $height = ( isset( $option['height'] ) ?  $option['height'] : $value['default']['height'] );
        $crop   = ( isset( $option['crop'] ) ?  $option['crop'] : $value['default']['crop'] );
        $crop   = ( $crop == 'on' || $crop == '1' ) ? 1 : 0;
        $crop 	= checked( 1, $crop, false );

        ?><tr valign="top">
        <th scope="row" class="titledesc"><?php echo esc_html( $value['title'] ) ?> <?php echo $value['desc'] ?></th>
        <td class="forminp image_width_settings">

            <input name="<?php echo esc_attr( $value['id'] ); ?>[width]" id="<?php echo esc_attr( $value['id'] ); ?>-width" type="text" size="3" value="<?php echo $width; ?>" /> &times; <input name="<?php echo esc_attr( $value['id'] ); ?>[height]" id="<?php echo esc_attr( $value['id'] ); ?>-height" type="text" size="3" value="<?php echo $height; ?>" />px

            <label><input name="<?php echo esc_attr( $value['id'] ); ?>[crop]" id="<?php echo esc_attr( $value['id'] ); ?>-crop" type="checkbox" <?php echo $crop; ?> /> <?php _e( 'Hard Crop?', 'yit' ); ?></label>

        </td>
        </tr><?php
		
	}
	
	/**
	 * Register Image size
	 *
	 */
	public function register_taxonomy_image_sizes(){
		
		if ( function_exists( 'add_image_size' ) ) {
			
			$sizes = array(
						'wb_book_category-thumb',
						'wb_book_author-thumb',
						'wb_book_publisher-thumb'
					);
			
			foreach( $sizes as $size ){
				
				$options 	= maybe_unserialize( get_option( $size.'_image_size' ) );
				$width 		= isset( $options['width'] )? $options['width'] : 300;
				$height		= isset( $options['height'] )? $options['height'] : 500;
				$crop		= isset( $options['crop'] )? $options['crop'] : 0;
				
				add_image_size( $size, $width, $height, $crop );
				
			}
		
		}
	
	}
	
	/**
     * assets.
     *
     * @access public
     * @return void
     */
    function assets() {
    	
		$screen = get_current_screen();
		$screens = array();
		
		if( !empty($this->book_taxonomies) ){
			foreach( $this->book_taxonomies as $taxonomy ){
				$screens[] = 'edit-'.$taxonomy;
			}
		}
		
		if ( in_array( $screen->id, $screens ) ) {
			$this->current_admin_taxonomy = $screen->taxonomy;
			wp_enqueue_media();
		}
		
    }

	/**
	 * Taxonomy thumbnails
	 */
	function wpini_add_thumbnail_field() {
		
		global $woocommerce;
		$current_taxonomy = $this->current_admin_taxonomy;
		?>
		<div class="form-field">
			<label><?php _e( 'Thumbnail', 'woo-bookstore' ); ?></label>
			<div id="<?php echo $current_taxonomy ?>_thumbnail" style="float:left;margin-right:10px;"><img src="<?php echo woocommerce_placeholder_img_src(); ?>" width="60px" height="60px" /></div>
			<div style="line-height:60px;">
				<input type="hidden" id="<?php echo $current_taxonomy ?>_thumbnail_id" name="<?php echo $current_taxonomy ?>_thumbnail_id" />
				<button type="button" class="upload_image_button button"><?php _e('Upload/Add image', 'woo-bookstore'); ?></button>
				<button type="button" class="remove_image_button button"><?php _e('Remove image', 'woo-bookstore'); ?></button>
			</div>
			<script type="text/javascript">

				jQuery(function(){
					 // Only show the "remove image" button when needed
					 if ( ! jQuery('#<?php echo $current_taxonomy ?>_thumbnail_id').val() )
						 jQuery('.remove_image_button').hide();

					// Uploading files
					var file_frame;

					jQuery(document).on( 'click', '.upload_image_button', function( event ){

						event.preventDefault();

						// If the media frame already exists, reopen it.
						if ( file_frame ) {
							file_frame.open();
							return;
						}

						// Create the media frame.
						file_frame = wp.media.frames.downloadable_file = wp.media({
							title: '<?php _e( 'Choose an image', 'woocommerce' ); ?>',
							button: {
								text: '<?php _e( 'Use image', 'woocommerce' ); ?>',
							},
							multiple: false
						});

						// When an image is selected, run a callback.
						file_frame.on( 'select', function() {
							attachment = file_frame.state().get('selection').first().toJSON();

							jQuery('#<?php echo $current_taxonomy ?>_thumbnail_id').val( attachment.id );
							jQuery('#<?php echo $current_taxonomy ?>_thumbnail img').attr('src', attachment.url );
							jQuery('.remove_image_button').show();
						});

						// Finally, open the modal.
						file_frame.open();
					});

					jQuery(document).on( 'click', '.remove_image_button', function( event ){
						jQuery('#<?php echo $current_taxonomy ?>_thumbnail img').attr('src', '<?php echo woocommerce_placeholder_img_src(); ?>');
						jQuery('#<?php echo $current_taxonomy ?>_thumbnail_id').val('');
						jQuery('.remove_image_button').hide();
						return false;
					});
				});

			</script>
			<div class="clear"></div>
		</div>
		<?php
	}

	function wpwini_edit_thumbnail_field( $term, $taxonomy ) {
		
		global $woocommerce;
		$current_taxonomy = $taxonomy;
		
		$image 			= '';
		$thumbnail_id 	= get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
		if ($thumbnail_id) :
			$image = wp_get_attachment_url( $thumbnail_id );
		else :
			$image = woocommerce_placeholder_img_src();
		endif;
		?>
		<tr class="form-field">
			<th scope="row" valign="top"><label><?php _e('Thumbnail', 'woo-bookstore'); ?></label></th>
			<td>
				<div id="<?php echo $current_taxonomy ?>_thumbnail" style="float:left;margin-right:10px;"><img src="<?php echo $image; ?>" width="60px" height="60px" /></div>
				<div style="line-height:60px;">
					<input type="hidden" id="<?php echo $current_taxonomy ?>_thumbnail_id" name="<?php echo $current_taxonomy ?>_thumbnail_id" value="<?php echo $thumbnail_id; ?>" />
					<button type="button" class="upload_image_button button"><?php _e('Upload/Add image', 'woo-bookstore'); ?></button>
					<button type="button" class="remove_image_button button"><?php _e('Remove image', 'woo-bookstore'); ?></button>
				</div>
				<script type="text/javascript">

					jQuery(function(){

						 // Only show the "remove image" button when needed
						 if ( ! jQuery('#<?php echo $current_taxonomy ?>_thumbnail_id').val() )
							 jQuery('.remove_image_button').hide();

						// Uploading files
						var file_frame;

						jQuery(document).on( 'click', '.upload_image_button', function( event ){

							event.preventDefault();

							// If the media frame already exists, reopen it.
							if ( file_frame ) {
								file_frame.open();
								return;
							}

							// Create the media frame.
							file_frame = wp.media.frames.downloadable_file = wp.media({
								title: '<?php _e( 'Choose an image', 'woocommerce' ); ?>',
								button: {
									text: '<?php _e( 'Use image', 'woocommerce' ); ?>',
								},
								multiple: false
							});

							// When an image is selected, run a callback.
							file_frame.on( 'select', function() {
								attachment = file_frame.state().get('selection').first().toJSON();

								jQuery('#<?php echo $current_taxonomy ?>_thumbnail_id').val( attachment.id );
								jQuery('#<?php echo $current_taxonomy ?>_thumbnail img').attr('src', attachment.url );
								jQuery('.remove_image_button').show();
							});

							// Finally, open the modal.
							file_frame.open();
						});

						jQuery(document).on( 'click', '.remove_image_button', function( event ){
							jQuery('#<?php echo $current_taxonomy ?>_thumbnail img').attr('src', '<?php echo woocommerce_placeholder_img_src(); ?>');
							jQuery('#<?php echo $current_taxonomy ?>_thumbnail_id').val('');
							jQuery('.remove_image_button').hide();
							return false;
						});
					});

				</script>
				<div class="clear"></div>
			</td>
		</tr>
		<?php
	}

	function wpini_thumbnail_field_save( $term_id, $tt_id, $taxonomy ) {
		if ( isset( $_POST[$taxonomy.'_thumbnail_id'] ) )
			update_woocommerce_term_meta($term_id, 'thumbnail_id', $_POST[$taxonomy.'_thumbnail_id']);
	}
	
}
?>