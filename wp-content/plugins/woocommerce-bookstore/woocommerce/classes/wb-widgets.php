<?php
/**
 * Woo_Bookstore_Widgets Class
 *
 *
 * @class 		Woo_Bookstore_Widgets
 * @package		WooCommerce Bookstore
 * @category	Class
 * @since		1.0.7
 *
 */
class Woo_Bookstore_Widgets{
	
	protected static $_instance = null;
	
	/**
	 * Initialize widgets
	 *
	 */
	public static function init() {
		
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
		
	}
	
	/**
	 * Constructor
	 *
	 */
	public function __construct() {
		
		require_once( WPINI_WOO_BOOKSTORE_DIR.'woocommerce/classes/widgets/wb-author-box.php' );
		require_once( WPINI_WOO_BOOKSTORE_DIR.'woocommerce/classes/widgets/wb-taxonomy-widget.php' );
		
		add_action( 'widgets_init', array( &$this, 'register_widgets' ) );
		
	}
	
	public function register_widgets(){
		
		register_widget( 'Woo_Bookstore_Taxonomy_Widget' );
		register_widget( 'Woo_Bookstore_Author_Box' );
		
	}
	
}