<?php
/**
 * WC_Product_Book Class
 *
 *
 * @class 		WC_Product_Book
 * @package		WooCommerce Bookstore
 * @category	Class
 * @since		1.0
 *
 */
class WC_Product_Book extends WC_Product_Simple{
    
	/**
     * __construct function.
     *
     * @access public
     * @param mixed $product
     */
	public function __construct( $product ) {
		
		parent::__construct( $product );
		$this->product_type 	= 'book';
		
		$this->id   			= ( isset($product->ID) && !empty($product->ID) ? $product->ID : $product );    	
		//$this->ID   			= ( isset($product->ID) && !empty($product->ID) ? $product->ID : $product );    	
		
		//set custom fields
		$custom_fields = woo_book_get_custom_fields();
		if( ! empty( $custom_fields ) ){
			foreach( $custom_fields as $field ){
				if( ! empty( $field['meta_key'] ) ){
					$this->{$field['meta_key']} = get_post_meta( $this->id, $field['meta_key'] , true ) ;				
				}
			}
		}
	}
	
	public function get_categories( $sep = ', ', $before = '', $after = '' ){
		return get_the_term_list( $this->id, 'book_category', $before, $sep, $after );
	}
	
	public function get_authors( $sep = ', ', $before = '', $after = '' ){
		return get_the_term_list( $this->id, 'book_author', $before, $sep, $after );
	}
	
	public function get_publishers( $sep = ', ', $before = '', $after = '' ){
		return get_the_term_list( $this->id, 'book_publisher', $before, $sep, $after );
	}
	
}
?>