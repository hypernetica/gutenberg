<?php
/**
 * TABLE OF CONTENTS
 *
 * admin_show_book_elements()
 *
 *
 **/

//When book product type selected display simple product fields as well
//add_action('woocommerce_product_options_general_product_data', 'admin_show_book_elements' );
function admin_show_book_elements(){
	
	ob_start();
	?>
    <script>
	jQuery(document).ready(function($) {
        $('.show_if_simple').addClass('show_if_book');
    });
    </script>
	<?php
	echo ob_get_clean();
}

/** Add book product tab */
add_action( 'woocommerce_product_write_panel_tabs', 'wwb_book_tab' );
function wwb_book_tab() {
	
	?>
    <li class="show_if_book hide_if_virtual hide_if_simple hide_if_grouped hide_if_external"><a href="#woo_book_tabs"><?php _e( 'Book', 'woo-bookstore' ); ?></a></li>
	<?php

}

/** Book product tab content */
add_action( 'woocommerce_product_write_panels', 'wwb_product_type_book_tab_content' );
function wwb_product_type_book_tab_content() {
	
	global $woocommerce, $post;
	//get book info
	$book = new WC_Product_Book( $post->ID );
	?>
    <div id="woo_book_tabs" class="panel woocommerce_options_panel">
		
		<?php
            wp_nonce_field( plugin_basename( __FILE__ ), 'edit_book' );
            $ajax_nonce = wp_create_nonce( 'wwb_ajax_call' );
            
            $custom_fields = woo_book_get_custom_fields();
		?>
    	<input type="hidden" value="<?php echo $ajax_nonce ?>" name="ajax_security" id="ajax_security"  />
        <div class="options_group">
            <h3 style="margin-left:7px"><?php _e( 'Book Details', 'woo-bookstore' ) ?></h3>
            <p class="description"><?php _e( 'An extra product tab will be added to show book details.','woo-bookstore') ?></p>
            <div id="wwb-error-container" style="color:red"></div>
		</div>
                
       	<?php if( !empty( $custom_fields ) ): ?>
       
            <div class="options_group">
                
				<?php foreach( $custom_fields as $field ): ?>        	
                
                <?php 
                    $type 			= 'text';
                    $input_classes 	= '';
                    
                    $current_value = isset( $book->{$field['meta_key']} ) ? $book->{$field['meta_key']} : '';
                    
                    if( $field['field_type'] == 'textarea' )
                        $type = 'textarea';
                        
                    if( $field['field_type'] == 'number' )
                        $type = 'number';
                    
                    if( $field['field_type'] == 'date' )
                        $input_classes .= ' bookstore_date';
                    
                    if( $field['unique'] == 'yes' )
                        $input_classes .= ' bookstore_is_unique';
                    
                    $maxlength = '';
                    
                    if( isset( $field['length'] ) ){
                        if( $field['length'] != '*' ){
                            if( is_numeric( $field['length'] ) ){
                                $maxlength = 'maxlength="'.absint( $field['length'] ).'"';
                            }
                        }
                    }
                        
                ?>                    
                    <p class="form-field custom_field_type">
                        <label for="<?php echo $field['meta_key'] ?>"><?php _e( $field['name'], 'woo-bookstore' ) ?></label>
                        <?php if( $type != 'textarea' ) : ?>
                        
                        <input class="<?php echo $field['admin_class'] ?><?php echo $input_classes ?>" <?php echo $maxlength ?> type="<?php echo $type ?>" value="<?php echo $current_value ?>" name="book[<?php echo $field['meta_key'] ?>]" id="<?php echo $field['meta_key'] ?>" />
                        
                        <?php else: ?>
                        <textarea class="<?php echo $field['admin_class'] ?>" style="width:100%" name="book[<?php echo $field['meta_key'] ?>]" id="<?php echo $field['meta_key'] ?>"><?php echo $current_value ?></textarea>
                        <?php endif ?>   
                        
                    </p>
                    
                <?php endforeach ?>
                
            </div> 
        <?php endif ?>
        
	</div>        
    <?php	
		
}

/** Save Content */
add_action( 'woocommerce_process_product_meta', 'wwb_save_tabdata', 10, 4 );
function wwb_save_tabdata( $post_ID , $post ){

	if( isset( $_POST['book'] ) && !empty( $_POST['book'] ) ){

		$meta_keys 		= wwb_get_valid_book_meta();
		$unique_keys 	= woo_book_get_unique_fields();
		
		foreach ( $_POST['book'] as $key => $value ){
			
			if( in_array( $key, $meta_keys ) ){
				
				//check if value is unique
				if( in_array( $key, $unique_keys ) ){
					if( ! woo_book_value_exists( $post_ID, $key, $value ) )
						add_post_meta( $post_ID, $key, $value, true ) or update_post_meta( $post_ID, $key, $value );		
				}else{
					add_post_meta( $post_ID, $key, $value, true ) or update_post_meta( $post_ID, $key, $value );
				}
				
			}
		}
		
		//purge the cache?
		if( get_option( 'woobookstore_purge_cache_on_post_save' ) == 'yes' ){
			
			global $wpdb;
			$wpdb->query( "TRUNCATE TABLE {$wpdb->prefix}woobookstore_cache" );
			
			//clear cache files as well
			$cache_folder = Woo_Bookstore_Advanced_Search::get_cache_folder();
			if( is_dir( $cache_folder ) ){
				array_map( 'unlink', glob( $cache_folder."*" ) );
			}
			
		}
				
	}
	
}

/**
 * Purge the cache on ajax quick edit page
 *
 */
add_action( 'woocommerce_product_quick_edit_save', 'wwb_woocommerce_product_quick_edit_save_purge_cache', 10, 1 );

function wwb_woocommerce_product_quick_edit_save_purge_cache( $product ){

	if ( $product->is_type('book') ) {
	
		//purge the cache?
		if( get_option( 'woobookstore_purge_cache_on_post_save' ) == 'yes' ){
			
			global $wpdb;
			$wpdb->query( "TRUNCATE TABLE {$wpdb->prefix}woobookstore_cache" );
			
			//clear cache files as well
			$cache_folder = Woo_Bookstore_Advanced_Search::get_cache_folder();
			if( is_dir( $cache_folder ) ){
				array_map( 'unlink', glob( $cache_folder."*" ) );
			}
			
		}
	
	}

}
?>