jQuery( document ).ready(function($) {
    
	$( "#woo-bookstore-admin-updater" ).click( function(e) {
        
		e.preventDefault();
		
		var element_id = $( this ).attr( 'id' );
		var spinner_id = $( this ).attr( 'id' )+"_loading";
		
		$( this ).after( '<span class="spinner" style="display:block;visibility:visible;float:left" id="'+spinner_id+'"></span>' );
		
		$.ajax({
		  type: "POST",
		  url: wwb_script_vars.url,
		  data: {
				action:'wwb_admin_run_updater',
				security: $( this ).data( 'nonce' )
		  }
		})
		.always(function(data){
			
			$( "#"+spinner_id ).remove();
			var obj = jQuery.parseJSON(data);
			
			$( "#woo-bookstore-updater-messages" ).html( '<span class="'+obj.status+'">'+obj.msg+'</span>' );
			$( "#woo-bookstore-updater-messages" ).show();
			
			if( obj.status == 'success' )
				$( "#woo-bookstore-admin-updater" ).remove();
		
		});
			
	});
	
});