jQuery(document).ready(function($) {
    
	/**
	 * Datepicker
	 *
	 */
	$( '.bookstore_date' ).datepicker({
		changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'MM yy',
		yearRange: "1900:wwb_script_vars.current_year",
        onClose: function( dateText, inst ) { 
            var month	= $( "#ui-datepicker-div .ui-datepicker-month :selected" ).val();
            var year 	= $( "#ui-datepicker-div .ui-datepicker-year :selected" ).val();
            $(this).val( $.datepicker.formatDate( 'mm-yy', new Date( year, month, 1 ) ) );
        }
	});
	
	$('.show_if_simple').addClass('show_if_book');
	
	/**
	 * Product Type Book
	 *
	 */
	$( '#product-type' ).change(function() {
        
		if( $(this).val() == 'book' ){
			
			$( '#book_categorydiv' ).show('slow');
			$( '#book_authordiv' ).show('slow');
			$( '#book_publisherdiv' ).show('slow');
			$( '#tagsdiv-book_author' ).show('slow');
			$( '#tagsdiv-book_publisher' ).show('slow');
			
		}else{
			
			$( '#book_categorydiv' ).hide('slow');
			$( '#book_authordiv' ).hide('slow');
			$( '#book_publisherdiv' ).hide('slow');
			$( '#tagsdiv-book_author' ).hide('slow');
			$( '#tagsdiv-book_publisher' ).hide('slow');
			
		}
		
    });
	
	/**
	 * Sort Book Fields
	 *
	 */
	$( "#book_custom_fields_sortable" ).sortable({
		handle: '.sort_handler',
		cursor: "move"
	});
	
	$( "#book_custom_fields_sortable" ).disableSelection();
	
	$( "#book_advanced_search_sortable" ).sortable({
		handle: '.sort_handler',
		cursor: "move"
	});
	
	$( "#book_advanced_search_sortable" ).disableSelection();
	
	/**
	 * Fix firefox
	 *
	 */
	if( navigator.userAgent.toLowerCase().indexOf('firefox') > -1 ){
		
		$("input, select, textarea").bind('mousedown.ui-disableSelection selectstart.ui-disableSelection', function(e){
			e.stopImmediatePropagation();
		});
				
	}
	
	/** 
	 * Remove Row 
	 *
	 */	
	$( '.remove_btn' ).click(function(e) {
        
		e.preventDefault();
		if( woo_count_field_rows() > 1 ){
			
			if ( confirm(  wwb_script_vars.confirm_box_text ) ) {
				$( '#'+$( this ).attr( 'rel' ) ).remove();
			}
			
		}else{
			
			woo_bookstore_show_msg( 'error', wwb_script_vars.book_fields_empty_text, '#woo_bookstore_custom_fields_messages' )
			
		}
		
    });
	
	/**
	 * Admin autocomplete checkbox
	 *
	 */
	$( ".wb-admin-autocomplete-check" ).click(function(e) {
    	
		var row 				= $( this ).data( 'row' );
		var select_container 	= "td.display_type_dropdown_container_"+row+" select";
		var field_type			= $( ".field_type_container_"+row+" input" ).data( 'type' );
		var selected_value		= $( select_container ).val();
		
		if( $( this ).is( ":checked" ) ) {
			
			$( select_container+">option" ).each(function(index, element) {
					
				if( $( element ).val() != "text" ) {
					
					$( element ).attr( "disabled", "disabled" );
					$( element ).removeAttr( "selected" );
					
				} else {
					
					$( element ).attr( "selected", "selected" );
					
				}
				
			});
			
			selected_value = "text";
				
		} else {
			
			$( select_container+">option" ).each(function(index, element) {

				$( element ).removeAttr( "disabled" );
				$( element ).removeAttr( "selected" );

			});
			
			
		} //$( this ).is( ":checked" )
		
		if( field_type == "free_text" ) {
			
			$( select_container+">option" ).each(function(index, element) {
					
				if( $( element ).val() != "text" ) {
					
					$( element ).attr( "disabled", "disabled" );
					$( element ).removeAttr( "selected" );
					
				}
			
			});
			
		}		
		
		
		if( field_type == "meta" ) {
			
			$( select_container+">option" ).each(function(index, element) {
					
				if( $( element ).val() == "multiselect" ) {
					
					$( element ).attr( "disabled", "disabled" );
					$( element ).removeAttr( "selected" );
					
				}
			
			});
			
		}
		
		$( select_container ).val( selected_value );
		
	});
	
	/**
	 * Add Row
	 *
	 */
	$( '.add_btn' ).click(function(e) {
        
		e.preventDefault();
		var row_count = woo_count_field_rows() + 1;
		$( '#book_custom_fields_sortable tr:last' ).after( wp_woo_book_new_tr( row_count ) );
		
	});
	
	$( '.woo_bookstore_restore' ).click(function(e) {
        
		e.preventDefault();
		
		if ( confirm(  wwb_script_vars.confirm_restore_defaults ) ) {
			window.location = $( this ).attr( 'href' ) ;
		}
		
    });			
	
	/**
	 * Check book's field if is unique
	 *
	 */
	$( ".bookstore_is_unique" ).focusout( function(e) {
        
		if( $( this ).val() != "" ){
			
			var element_id = $( this ).attr( 'id' );
			var spinner_id = $( this ).attr( 'id' )+"_loading";
			
			$( "#"+element_id+"_validation_message" ).remove();
			$( "#"+element_id ).css( 'border', '1px solid #ddd' );
			
			$( this ).after( '<span class="spinner" style="display:block;visibility:visible;float:left" id="'+spinner_id+'"></span>' );
			
			$.ajax({
			  type: "POST",
			  url: wwb_script_vars.url,
			  data: {
					action:'wwb_search_value',
					post: $( '#post_ID' ).val(),
					type: $( this ).attr( 'id' ),
					value: $( this ).val(),
					security: $( "#ajax_security" ).val()
			  }
			})
			.always(function(data){
				
				$( "#"+spinner_id ).remove();
				if( data == "true" ){
					$( "#"+element_id ).css( 'border-color', 'red' );
					$( "#"+element_id ).after( '<p id="'+element_id+'_validation_message" class="description" style="color:red">'+wwb_script_vars.unique_field_exists+'</p>' );
				}
				
			});
			
		}
    
	});
	
	// Shortcode preview
	$( '.woo_bookstore_shortcode_preview' ).click(function(e) {
        $(this).select();
    });
	
	/**
	 * Empty cache
	 *
	 */
	$( "#woo_bookstore_admin_empty_cache" ).click(function(e) {
        
		e.preventDefault();
		
		woo_bookstore_loading( 'woo_bookstore_admin_empty_cache', 'show' );
		
		$.ajax({
			type: "POST",
			url: wwb_script_vars.url,
			beforeSend: function(){
				
				$( ".woo_bookstore_ajax_btn" ).each( function( index, element ) {
					$( element ).attr( "disabled","disabled" );
				});
				
			},
			data: {
				action:'wwb_clear_cache',
				security: $( "#wwb_empty_cache_non" ).val()
			}
		})
		.always(function(data){
			
			var response_msg = "";
			
			try {
			
				var obj = jQuery.parseJSON(data);
				if( obj.msg == 'error' )
					$( "#woo_bookstore_admin_empty_cache_messages" ).css( 'color', 'red' );
				else
					$( "#woo_bookstore_admin_empty_cache_messages" ).css( 'color', 'green' );
				
				response_msg = obj.msg;
				
			} catch( Exception ) {
				
				response_msg = wwb_script_vars.internal_server_error;
				
			}
			
			$( ".woo_bookstore_ajax_btn" ).each( function( index, element ) {
				$( element ).removeAttr( "disabled" );
			});
							
			$( "#woo_bookstore_admin_empty_cache_messages" ).html ( response_msg );
			woo_bookstore_loading( 'woo_bookstore_admin_empty_cache', 'hide' );
			
		});
		
    });
	
	/**
	 * Export CSV
	 *
	 */
	$( "#woo_bookstore_export_books" ).click(function(e) {
        
		e.preventDefault();
		
		woo_bookstore_loading( 'woo_bookstore_admin_empty_cache', 'show' );
		
		$.ajax({
			
			type: "POST",
			url: wwb_script_vars.url,
			beforeSend: function(){
				
				$( ".woo_bookstore_ajax_btn" ).each( function( index, element ) {
					$( element ).attr( "disabled","disabled" );
				});
				
			},
			data: {
				action:'wwb_export_books',
				security: $( "#wwb_export_non" ).val(),
				separator: $( "#woobookstore_export_separator" ).val()
			}
			
		})
		.always( function( data ){
			
			var response_msg = "";
			
			try {
				
				var obj = jQuery.parseJSON(data);
				
				if( obj.msg == 'error' )
					$( "#woo_bookstore_admin_export_messages" ).css( 'color', 'red' );
				else
					$( "#woo_bookstore_admin_export_messages" ).css( 'color', 'green' );
				
				response_msg = obj.msg;
				
				if( obj.created != "" ) {
					
					$( ".export_creation_date" ).text( "" );
					$( ".export_creation_date" ).text( obj.created );
					
				}
				
			} catch( Exception ) {
				
				response_msg = wwb_script_vars.internal_server_error;
				
			}
			
			$( ".woo_bookstore_ajax_btn" ).each( function( index, element ) {
				$( element ).removeAttr( "disabled" );
			});
							
			$( "#woo_bookstore_admin_export_messages" ).html ( response_msg );
			woo_bookstore_loading( 'woo_bookstore_export_books', 'hide' );
			
		});
		
    });
	
	$( '.woo-bookstore-admin-form' ).submit(function(e) {
        
		var check = woo_bookstore_check_fields();
	
		return check;
		
    });
	
	$( "#save_woo_book_options" ).click(function(e) {
		
		var _custom_fields_values = [];
		
		var _errors = "";
		
        $( ".wb_admin_custom_field_name" ).each(function(index, element) {
        	
			$( element ).css( 'border-color', '#ddd' );
			_custom_fields_values[$( element ).data( 'line' )] = $( element ).val();
		
		});
		
		$( ".wb_admin_custom_field_name" ).each(function(index, element) {
        	
			_current_line = $( element ).data( 'line' );
			_current_val  = $( element ).val();
			
			$( _custom_fields_values ).each(function( arr_index, arr_element) {
				
				if( _current_val == arr_element && _current_line != arr_index ) {

					$( element ).css( 'border-color', 'red' );
					_errors = _errors + " " + arr_element;
					
				}
			});
		
		});
		
		if( _errors != "" ) {
			
		 	e.preventDefault();
			woo_bookstore_show_msg( 'error', wwb_script_vars.dublicated_custom_fields, '#woo_bookstore_custom_fields_messages' );
			
		} else {
			
			return true;
			
		}
		
    });
	
});

function wpini_woo_book_init_scripts(){
	
	/** Remove Row */
	jQuery( '.remove_btn' ).click(function(e) {
        
		e.preventDefault();
		if( woo_count_field_rows() > 1 ){
			
			if ( confirm(  wwb_script_vars.confirm_box_text ) ) {
				jQuery( '#'+jQuery( this ).attr( 'rel' ) ).remove();
			}
			
		}else{
			
			woo_bookstore_show_msg( 'error', wwb_script_vars.book_fields_empty_text, '#woo_bookstore_custom_fields_messages' )
			
		}
		
    });
	
}

function wb_remove_table_row( element ){
	
	if( woo_count_field_rows() > 1 ){
			
		if ( confirm(  wwb_script_vars.confirm_box_text ) ) {
			jQuery( '#'+element ).remove();
		}
		
	}else{
		
		woo_bookstore_show_msg( 'error', wwb_script_vars.book_fields_empty_text, '#woo_bookstore_custom_fields_messages' )
		
	}
	
}

function woo_count_field_rows(){
	
	return jQuery('.book_custom_field_row').length;
	
}

function woo_bookstore_show_msg( type, msg, container ){
	
	jQuery( container ).html( msg );
	jQuery( container ).show( 'slow' );
	
}

function wp_woo_book_new_tr( count ){
	
	var html = '';
	
	html = 	'<tr class="book_custom_field_row" id="custom_field_row_'+count+'">'+
				'<td><input class="wb_text_field" type="text" value="" name="book_fields['+count+'][name]" id="" /></td>'+
				'<td class="wb_aligncenter"><select name="book_fields['+count+'][field_type]"><option value="text">'+wwb_script_vars.text_field_display+'</option><option value="number">'+wwb_script_vars.number_field_display+'</option><option value="date">'+wwb_script_vars.date_field_display+'</option><option value="textarea">'+wwb_script_vars.textarea_field_display+'</option></select></td>'+
				'<td class="wb_aligncenter"><select name="book_fields['+count+'][unique]"><option value="no">'+wwb_script_vars.no_field_display+'</option><option value="yes">'+wwb_script_vars.yes_field_display+'</option></select></td>'+
				'<td><input class="wb_text_field wb-required wb_admin_custom_field_name" type="text" value="" name="book_fields['+count+'][meta_key]" id="" data-line="'+count+'" /></td>'+
				'<td class="wb_aligncenter"><input type="number" class="small-text" value="" name="book_fields['+count+'][length]" id="" /></td>'+
				'<td><input class="wb_text_field" type="text" value="" name="book_fields['+count+'][class]" id="" /></td>'+
				'<td class="wb_aligncenter"><select name="book_fields['+count+'][admin_class]"><option value="short">'+wwb_script_vars.short_text_name+'</option><option value="regular-text">'+wwb_script_vars.regular_text_name+'</option></select></td>'+
				'<td class="wb_aligncenter"><input type="checkbox" value="yes" name="book_fields['+count+'][visible]" id="" /></td>'+
				'<td class="sort_handler wb_aligncenter"><span class="sort_btn"><i class="fa fa-bars fa-lg"></i></span></td>'+
				'<td class="wb_aligncenter"><a href="#" rel="custom_field_row_'+count+'" class="remove_btn" onclick="wb_remove_table_row(\'custom_field_row_'+count+'\')"><i class="fa fa-times-circle fa-lg"></i></a></td>'+
			'</tr>';
	
	return html;
}

/**
 * Validate admin forms
 *
 */
function woo_bookstore_check_fields(){
	
	var errors = 0;
	var check = true;
	
	jQuery( '.wb-required' ).each(function(index, element) {
        
		var loop = jQuery( this );
		
		if( jQuery( this ).val() == "" ){
			
			errors++;
			check = false;
			loop.css( 'border-color', 'red' );
			
		}
		
    });
	
	if( errors > 0 ){
		
		woo_bookstore_scroll_to_top();
		woo_bookstore_show_msg( 'error', '<div class="error"><p>'+wwb_script_vars.required_fields+'</p></div>', '.wb-admin-messages' );
		
	}
	
	return check;	
}

function woo_bookstore_admin_init_autocomplete_form() {
	
	jQuery( ".wb-admin-autocomplete-check" ).each(function(index, element) {
    	
		var row 				= jQuery( element ).data( 'row' );
		var select_container 	= "td.display_type_dropdown_container_"+row+" select";
		var field_type			= jQuery( ".field_type_container_"+row+" input" ).data( 'type' );

		if( jQuery( element ).is( ":checked" ) ) {
			
			jQuery( select_container+">option" ).each(function(index, element) {
					
				if( jQuery( element ).val() != "text" ) {
					
					jQuery( element ).attr( "disabled", "disabled" );
					jQuery( element ).removeAttr( "selected" );
					
				} else {
					
					jQuery( element ).attr( "selected", "selected" );
					
				}
				
			});
				
		} 
		
	});
	
}