function woo_bookstore_loading( element, action ){
	
	var id = element+"_spinner";
	
	if( action == 'hide' ){
		
		jQuery( '.bookstore-advanced-search-btn' ).each(function(index, element) {
            jQuery( element ).removeAttr( 'disabled' );
        });
		
		jQuery( '.wwb_loader' ).css( 'display', 'none' );
		
	} else {
		
		var is_loader_visible = jQuery( '.wwb_loader' ).is( ':visible' );
		
		jQuery( '.bookstore-advanced-search-btn' ).attr( 'disabled', 'disabled' );
		
		if(	! is_loader_visible ) {
			
			var html = '<div class="wwb_loader" id="'+id+'"><div class="bar1"></div><div class="bar2"></div><div class="bar3"></div><div class="bar4"></div><div class="bar5"></div><div class="bar6"></div><div class="bar7"></div><div class="bar8"></div><div class="bar9"></div><div class="bar10"></div><div class="bar11"></div><div class="bar12"></div></div>';
	
			jQuery( "#"+element ).after( html );
		
		}
		
	}
	
}

/**
 * Scrol to top function
 *
 * @package 1.7
 * @author WPini
 */
function woo_bookstore_scroll_to_top(){
	
	jQuery("html, body").animate({ scrollTop: 0 }, "slow");
	
}