/**
 * Group autocomplete results
 *
 */
jQuery( function() {
	jQuery.widget( "custom.catcomplete", jQuery.ui.autocomplete, {
	  _create: function() {
		this._super();
		this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
	  },
	  _renderMenu: function( ul, items ) {
		var that = this,
		  currentCategory = "";
		jQuery.each( items, function( index, item ) {
		  var li;
		  if ( item.category != currentCategory ) {
			ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
			currentCategory = item.category;
		  }
		  li = that._renderItemData( ul, item );
		  if ( item.category ) {
			li.attr( "aria-label", item.category + " : " + item.label );
		  }
		});
	  }
	});
});

jQuery( document ).ready(function($) {
 	
	if( wb_script_vars.woobookstore_advanced_search_autocomplete_group == "yes" ) {
		
		$( ".book-search-autocomplete" ).catcomplete({
		  
		  source: function(request, response) {
				
				$.ajax({
					url: wb_script_vars.ajax_url+'?action=wb_advanced_search_books&type='+this.element[0].id ,
					dataType: "json",
					beforeSend: woo_bookstore_loading( this.element[0].id, 'show' ),
					data: {
						term: request.term
					},
					success: function(data) {
						response( data );						
					},
					complete: function( data ){
						woo_bookstore_loading( $( this ).attr( "id" ), 'hide' );
					}
				});
			},
			minLength: 3,
			messages: {
				noResults: 'Found no results',
				results: function() {
					return '';
				}
			},
			select: function (e, ui) {
				if( ui.item.url.length > 0 ) {
					location.href = ui.item.url;			
				}
			}
			
		});	
		
	} else {
		
		$( ".book-search-autocomplete" ).autocomplete({
		  
		  source: function(request, response) {
				
				$.ajax({
					url: wb_script_vars.ajax_url+'?action=wb_advanced_search_books&type='+this.element[0].id ,
					dataType: "json",
					beforeSend: woo_bookstore_loading( this.element[0].id, 'show' ),
					data: {
						term: request.term
					},
					success: function(data) {
						response( data );						
					},
					complete: function( data ){
						woo_bookstore_loading( $( this ).attr( "id" ), 'hide' );
					}
				});
			},
			minLength: 3,
			messages: {
				noResults: 'Found no results',
				results: function() {
					return '';
				}
			},
			select: function (e, ui) {
				if( ui.item.url.length > 0 ) {
					location.href = ui.item.url;								
				}
			}
			
		});	
	
	}
	
	//Modify search before submit form 
	$( "#bookstore_search_btn" ).click(function(e) {
        
		e.preventDefault();
		
		woo_bookstore_loading( "bookstore_search_btn", 'show' );
		
		$.ajax({
		  type: "POST",
		  url: wb_script_vars.ajax_url,
		  data: {
			  	action: 'woo_bookstore_search_params',
				data: $( "#book-advanced-search" ).serialize()
		  }
		})
		.always(function(data){
			
			var obj = jQuery.parseJSON( data );
			woo_bookstore_loading( "bookstore_search_btn", 'hide' );
			
			if( obj.url.length > 0 )
				window.location = obj.url;
			
		});
		
    });
	
	
});

function woo_bookstore_pagination_args( href ){
	
	jQuery( 'ul.page-numbers li a' ).each(function(index, element) {
        
		var old_url = jQuery( this ).attr( 'href' );
		
		jQuery( this ).attr( 'href', href+'&paged='+woo_bookstore_gup( 'paged', old_url ) );
    });
	
}

function woo_bookstore_gup( name, url ) {
	if (!url) url = location.href
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp( regexS );
	var results = regex.exec( url );
	return results == null ? null : results[1];
}