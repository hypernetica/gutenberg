<?php
/**
 * The Template to display advanced book search form.
 *
 * Override this template by copying it to yourtheme/woocommerce/no-books-found.php
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<p class="woocommerce-info"><?php _e( 'No books were found matching your selection.', 'woo-bookstore' ); ?></p>