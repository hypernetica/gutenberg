<?php
/**
 * Book Product Tab
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product/tabs/tab-publisher.php
 *
 * @author 		WPini
 * @package 	WooBookstore/templates
 * @version     1.0
 */
?>
<?php global $product; ?>
<?php $book_publisher = wp_get_post_terms( $product->get_id(), 'book_publisher', $args ); ?>

<h2><?php _e( 'Publisher information', 'woo-bookstore' ) ?></h2>

<?php if( ! empty( $book_publisher ) ): ?>

	<?php foreach( $book_publisher as $key => $publisher ): ?>
		<p>
		<?php if( woo_bookstore_single_display_publisher_thumbnail() ): ?>
			
			<?php 
			
			$thumbnail = woo_bookstore_thumbnail_url( $publisher->term_id, 'wb_book_publisher-thumb' );
			
			if ( ! $thumbnail )
				$thumbnail = wb_woocommerce_placeholder_img_src();
			?>
			<img src="<?php echo $thumbnail; ?>" class="alignleft" alt="<?php echo $publisher->name; ?>" />

		<?php endif; ?>
		<?php echo wpautop( $publisher->description ) ?>
		</p>
	<?php endforeach ?>
	
<?php endif ?>   