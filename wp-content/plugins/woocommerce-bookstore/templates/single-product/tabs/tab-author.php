<?php
/**
 * Book Author Tab
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product/tabs/tab-author.php
 *
 * @author 		WPini
 * @package 	WooBookstore/templates
 * @version     1.0
 */
?>

<?php global $product; ?>
<?php $book_author = wp_get_post_terms( $product->get_id(), 'book_author', $args ); ?>
    
<h2><?php _e( 'Author information', 'woo-bookstore' ) ?></h2>

<?php if( ! empty( $book_author ) ): ?>
    <?php foreach( $book_author as $key => $author ): ?>
        <p>
        <?php if( woo_bookstore_single_display_author_thumbnail() ): ?>
            
            <?php 
            
            $thumbnail = woo_bookstore_thumbnail_url( $author->term_id, 'wb_book_author-thumb' );
            
            if ( ! $thumbnail )
                $thumbnail = wb_woocommerce_placeholder_img_src();
            ?>
            <img src="<?php echo $thumbnail; ?>" class="alignleft" alt="<?php echo $author->name; ?>" />

        <?php endif; ?>
        <?php echo wpautop( $author->description ) ?>
        </p>
    <?php endforeach ?>
<?php endif ?>   