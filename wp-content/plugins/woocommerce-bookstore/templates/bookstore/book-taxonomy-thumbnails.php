<?php
/**
 * The Template Book Taxonomies Thumbnails.
 *
 * Override this template by copying it to yourtheme/woocommerce/book-taxonomy-thumbnails.php
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div class="woocommerce columns-<?php echo $columns ?> <?php echo $class ?>">
	
	<?php woocommerce_product_loop_start(); ?>
    
        <ul id="book-tax-thumbnails" class="products">
            
            <?php foreach ( $terms as $index => $term ) : 
                
                $thumbnail = woo_bookstore_thumbnail_url( $term->term_id, apply_filters( $taxonomy.'_thumbnail_size_listing', 'shop_catalog' ) );
                
                if ( ! $thumbnail )
                    $thumbnail = wb_woocommerce_placeholder_img_src();
                
                $class = 'product-category product';
                
                if ( $index == 0 || $index % $columns == 0 )
                    $class .= ' first';
                elseif ( ( $index + 1 ) % $columns == 0 )
                    $class .= ' last';
                    
                //$width = floor( ( ( 100 - ( ( $columns - 1 ) * 2 ) ) / $columns ) * 100 ) / 100;
                ?>
                <li class="<?php echo $class; ?>" <?php /* style="width: <?php echo $width; ?>%;" */ ?>>
                	
                    <a href="<?php echo get_term_link( $term->slug, $taxonomy ); ?>" title="<?php echo $term->name; ?>">
                        <img src="<?php echo $thumbnail; ?>" alt="<?php echo $term->name; ?>" />
                        <h3><?php echo $term->name ?><?php echo apply_filters( $taxonomy.'_archive_count', sprintf( ' <mark class="count">(%s)</mark>', $term->count ) ) ?></h3>
                    </a>  
                    
                    <?php echo $term->description ?>
                        
                </li>
        
            <?php endforeach; ?>
            
        </ul>
    
    <?php woocommerce_product_loop_end(); ?>
    
    <?php do_action( 'woocommerce_shortcode_after_product_cat_loop' ); ?>
    
    <?php 
    woocommerce_reset_loop();
    wp_reset_postdata();
    
    // Remove ordering query arguments
    WC()->query->remove_ordering_args();
    ?>
</div>