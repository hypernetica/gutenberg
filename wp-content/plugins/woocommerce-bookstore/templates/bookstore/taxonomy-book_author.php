<?php
/**
 * The Template for displaying book author
 *
 * Override this template by copying it to yourtheme/woocommerce/taxonomy-book_author.php
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$wc_get_template = function_exists('wc_get_template') ? 'wc_get_template' : 'woocommerce_get_template';
$wc_get_template( 'archive-product.php', $args, '', WPINI_WOO_BOOKSTORE_DIR . 'templates/' );