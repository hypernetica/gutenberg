<?php
/**
 * Pagination - Show numbered pagination for bookstore.
 *
 * Override this template by copying it to yourtheme/woocommerce/bookstore/woo-bookstore-pagination.php
 *
 * @author 		WPini
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $book_query->max_num_pages <= 1 ) {
	return;
}

if( ! isset( $paged ) || empty( $paged ) ){
	$paged = max( 1, get_query_var( 'paged' ) );	
}
?>
<nav class="woocommerce-pagination">
	<?php
		echo paginate_links( apply_filters( 'woocommerce_pagination_args', array(
			'base'         => esc_url( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
			'format'       => '',
			'current'      => $paged,
			'total'        => $book_query->max_num_pages,
			'prev_text'    => '&larr;',
			'next_text'    => '&rarr;',
			'type'         => 'list',
			'end_size'     => 3,
			'mid_size'     => 3
		) ) );
	?>
</nav>