<?php
/**
 * The Template to display the main page of your bookstore.
 *
 * Override this template by copying it to yourtheme/woocommerce/woo-bookstore-catalog.php
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<?php if ( $products->have_posts() ) : ?>

	<?php woocommerce_product_loop_start(); ?>

        <?php while ( $products->have_posts() ) : $products->the_post(); ?>

            <?php wc_get_template_part( 'content', 'product' ); ?>

        <?php endwhile; // end of the loop. ?>

    <?php woocommerce_product_loop_end(); ?>

<?php endif; ?>