<?php
/**
 * The Template to display advanced book search form.
 *
 * Override this template by copying it to yourtheme/woocommerce/advanced-search.php
 *
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post;

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
?>

<div class="woocommerce">

	<?php if( ! empty( $fields ) ): ?>
        
        <form method="get" action="woo_bookstore_search_params" id="book-advanced-search">
        
        <?php foreach( $fields as $key => $values ): ?>
            <?php $selected = isset( $search_terms[$key]['terms']  ) ? $search_terms[$key]['terms'] : array(); ?>
            <?php $advanced_search->display_field_html( $key, $values, $selected ) ?>
            
        <?php endforeach ?>
        <input type="submit" id="bookstore_search_btn" class="bookstore-advanced-search-btn" value="<?php _e( 'Find Books', 'woo-bookstore' ) ?>" />
        </form>
        
        <?php if( ! empty( $search_terms ) && ! $atts['widget'] ): ?>
            
            <div id="woo_bookstore_search_results_ajax">
                <div style="width:100%" class="wwb_loader"><?php _e( 'Loading books...', 'woo-bookstore' ) ?></div>
            </div>
            
			<script>
            jQuery( document ).ready(function($) {
                
				woo_bookstore_scroll_to_top();
                
                //Search results in advanced search
                if ( $( '#woo_bookstore_search_results_ajax' ).length ){
                    
					woo_bookstore_loading( "woo_bookstore_search_results_ajax", 'show' );
					
					$.ajax({
						type: "POST",
						url: wb_script_vars.ajax_url,
						data: {
							action: 'woo_bookstore_ajax_results',
							data: $( "#book-advanced-search" ).serialize(),
							paged: '<?php echo $paged ?>',
							<?php 
							if( isset( $atts ) && ! empty( $atts ) ){
								
								$params = array();
								
								foreach( $atts as $att  => $val ){
									$params[] = $att.':"'.$val.'"';
								}
								
								echo implode( ',', $params );
							}
							?>
						}
                    })
                    .always(function(data){
                        
						woo_bookstore_loading( "woo_bookstore_search_results_ajax", 'hide' );
                        
                        var obj = jQuery.parseJSON( data );
                        
                        if( obj.results.length > 0 ){
                            
                            $( '#woo_bookstore_search_results_ajax' ).html( obj.results );
                            woo_bookstore_pagination_args( '<?php echo get_permalink( $post->ID ) ?>?<?php echo http_build_query( $_GET, '', '&') ?>' );
							
                        }
                        
                    });
                      
                }
                  
            });
            </script>
            
        <?php endif ?>
        
    <?php else: ?>
        <ul class="woocommerce-error">
            <li><?php _e( 'Go to admin options to edit Advanced Book Search Fields', 'woo-bookstore' ) ?></li>
        </ul>
    <?php endif ?>

</div>