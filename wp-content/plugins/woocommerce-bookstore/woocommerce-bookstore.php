<?php
/** 
 * Plugin Name: WooCommerce Bookstore
 * Plugin URI: http://www.wpini.com/woocommerce-bookstore-plugin/ 
 * Description: Convert you WooCommerce store to online book store. Sell books using WooCommerce.
 * Author: WPini 
 * Version: 3.0
 * Author URI: http://wpini.com 
 *
 * Copyright 2015 WPini  (email : info@wpini.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package		WooCommerce Bookstore
 * @author		Nikolas Theologos
 * @since		1.0
 */  

define( 'WPINI_WB', 'woo-bookstore' );
define( 'WPINI_WOO_BOOKSTORE_VERSION', '3.0' );
define( 'WPINI_WOO_BOOKSTORE_DIR', plugin_dir_path( __FILE__ ) );
define( 'WPINI_WOO_BOOKSTORE_UIR', plugin_dir_url( __FILE__ ) );
define( 'WPINI_WOO_BOOKSTORE_SETTINGS_URL', admin_url().'admin.php?page=woo-bookstore-settings' );
define( 'WPINI_WOO_BOOKSTORE_UPLOADS_DIR', trailingslashit( WP_CONTENT_DIR ).'uploads/wpini-bookstore/' );
define( 'WPINI_WOO_BOOKSTORE_UPLOADS_DIR_URI', content_url( "uploads/wpini-bookstore/" ) );
define( 'WPINI_WOO_BOOKSTORE_IS_MULTISITE', is_multisite() );
define( 'WPINI_WOO_BOOKSTORE_WC_VERSION', woo_bookstore_get_wc_version() );

$prefix = "";
if( version_compare( WPINI_WOO_BOOKSTORE_WC_VERSION, '3.0', '<' ) )
	define( 'WPINI_WOO_BOOKSTORE_PREFIX_VERSION', '2.6.x' );
else
	define( 'WPINI_WOO_BOOKSTORE_PREFIX_VERSION', '3.0.x' );

// Debug mode
$debug = get_option( 'woobookstore_debug_mode' );
define( 'WOO_BOOKSTORE_DEBUG', ( $debug == 'yes' ? true :false ) );// For debug purposes only. If you set it to true then all style and scripts will be unminified

function woo_bookstore_load_textdomain(){
	load_plugin_textdomain( 'woo-bookstore', false, basename( dirname( __FILE__ ) ) . '/languages' );
}

add_action( 'plugins_loaded', 'woo_bookstore_load_textdomain' );

/**
 * Activations
 *
 */
require_once( 'init.php' );
register_activation_hook( __FILE__, 'woo_bookstore_init' );

require_once( 'includes.php' );

$is_woocommerce_active = false;

if( WPINI_WOO_BOOKSTORE_IS_MULTISITE ) {
	
	if( ! function_exists( 'is_plugin_active_for_network' ) )
		require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
		
	$is_woocommerce_active = is_plugin_active_for_network( 'woocommerce/woocommerce.php' );
	
} else {
	
	$is_woocommerce_active = in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) );
	
}

/** 
 * Is WooCommerce Enabled? 
 *
 */
if( $is_woocommerce_active ){
	
	// Version check
	if( version_compare( WPINI_WOO_BOOKSTORE_WC_VERSION, '2.5', '<' ) ) {
		
		function wpini_wbookstore_version_msg() {
			?>
			<div class="error">
				<p><strong><?php echo wpini_wb_message( 'woocommerce_version' ) ?></strong></p>
			</div>
			<?php
		}
		add_action( 'admin_notices', 'wpini_wbookstore_version_msg' );
		
	}

	require_once( 'assets.php' );
	
	add_action( 'init', array( 'Woo_Bookstore_Shortcodes', 'init' ), 10 );
	add_action( 'init', array( 'Woo_Bookstore_Taxonomies', 'init' ), 10 );
	add_action( 'after_setup_theme', array( 'Woo_Bookstore_Widgets', 'init') );
	add_action( 'setup_theme', array( 'WPini_Book_Taxonomy_Image', 'init' )  );
		
	/** 
	 * admin menu 
	 *
	 */
	add_action('admin_menu', 'woocommerce_bookstore_admin_settings');  
	function woocommerce_bookstore_admin_settings() {  
		
		add_submenu_page( 
			'woocommerce', 
			__( 'Bookstore', 'woo-bookstore' ), 
			__( 'Bookstore', 'woo-bookstore' ), 
			'manage_woocommerce', 
			'woo-bookstore-settings', 
			'woo_bookstore_settings'
		);
		 
	} 
	
	WPini_Bookstore_Admin::init();	
	WC_Book::init();		
	
	
		
} else {
	
	function wpini_wbookstore_msg() {
		?>
		<div class="error">
			<p><strong><?php echo wpini_wb_message( 'woocommerce_disabled' ) ?></strong></p>
		</div>
		<?php
	}
	add_action( 'admin_notices', 'wpini_wbookstore_msg' );
	
}

// Get WooCommerce version
function woo_bookstore_get_wc_version() {
        
	if ( ! function_exists( 'get_plugins' ) )
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	
        // Create the plugins folder and file variables
	$plugin_folder = get_plugins( '/' . 'woocommerce' );
	$plugin_file = 'woocommerce.php';
	
	// If the plugin version number is set, return it 
	if ( isset( $plugin_folder[$plugin_file]['Version'] ) ) {
		return $plugin_folder[$plugin_file]['Version'];

	} else {
	// Otherwise return null
		return NULL;
	}

}