<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              N/A
 * @since             1.0.0
 * @package           Woo_Search_Export
 *
 * @wordpress-plugin
 * Plugin Name:       Search Export for Woocommerce
 * Plugin URI:        http://hypernetica.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Hypernetica
 * Author URI:        N/A
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woo-search-export
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WOO_SEARCH_EXPORT_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-woo-search-export-activator.php
 */
function activate_woo_search_export() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woo-search-export-activator.php';
	Woo_Search_Export_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-woo-search-export-deactivator.php
 */
function deactivate_woo_search_export() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woo-search-export-deactivator.php';
	Woo_Search_Export_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_woo_search_export' );
register_deactivation_hook( __FILE__, 'deactivate_woo_search_export' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-woo-search-export.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_woo_search_export() {

	$plugin = new Woo_Search_Export();
	$plugin->run();

}
run_woo_search_export();


/************* FIND WHERE TO PUT ALL THE BELOW *****************/

add_action('wp_enqueue_scripts', 'hyp_setup_scripts');
function hyp_setup_scripts() {
	wp_register_style( 'hyp-woo-search-export', plugins_url ('css/style.css' , __FILE__ ));
	wp_enqueue_style('hyp-woo-search-export');
	wp_register_script( 'hyp-woo-search-export', plugins_url ('js/export.js' , __FILE__ ), array( 'jquery' ) );
	wp_enqueue_script( 'hyp-woo-search-export');
	wp_localize_script('hyp-woo-search-export', 'params', array(
		'pluginsUrl' => plugins_url('', __FILE__ ),
	));

}


add_action('woocommerce_before_shop_loop', 'hyp_show_export_button', 20);

function hyp_show_export_button() {
	global $wp_query;
	echo ('<div class="hyp-export-button-container">
		<form action="' . admin_url ('admin-post.php' ) .'" method="post">
		<input type="hidden" name="action" value="export_search">
		<input id="hyp-exp-but" class="hyp-export-button" type="submit" value="Export Search"/>
		<input type="hidden" name="query_vars" value="' . urlencode( json_encode( $wp_query->query_vars )) . '"/>
                </form></div>');

}


//add_action('woocommerce_after_shop_loop', 'hyp_cache_results', 20);
function hyp_cache_results( ) {
	//print_r($products);
//	die('1o23123123123123');
	//global $woocommerce;
	//$products = $woocommerce->query->get_posts();
	global $wp_query;
	$wp_query->posts_per_page = 0;
	$products = $wp_query->get_posts();

	print_r ($products);

	//return $products;
}

//add_filter('woocommerce_json_search_found_products', 'hyp_cache_results', 100 , 1);


add_action( 'admin_post_export_search', 'hyp_export_search' );
add_action( 'admin_post_nopriv_export_search', 'hyp_export_search' );

function hyp_export_search() {
    // Handle request then generate response using echo or leaving PHP and using HTML
	header('Content-Encoding: UTF-8');
	header("Content-type: text/csv; charset=utf-8");
        header("Content-Disposition: attachment; filename=search.csv");
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false);
	echo "\xEF\xBB\xBF"; // UTF-8 BOM

        if( isset($_POST['hyp-exp-qry']) ) {
                $qry_params = urldecode(json_decode($_POST['hyp-exp-qry']));
                print_r( $qry_params);
	}
	//var_dump($wp_query);
	$params = filter_input_array(INPUT_POST);
	$query_vars = json_decode(urldecode($params['query_vars']));
	$query_vars->posts_per_page = 0;
	$query_vars->update_post_term_cache = false;
	$query_vars->update_post_meta_cache = false;
	$query_vars->nopaging = true;
	//var_dump($query_vars);

	// Execute query
	$qry = new WP_Query ( $query_vars );
	$posts = $qry->get_posts();
	
	// Set fields to return. TODO: Make dynamic.
	$fields = [ 'ID', 'post_name', 'post_title','foobar' ] ;
	$delimiter = ';';

	// Export fields to CSV	
	$out = fopen('php://output', 'w');
	fputcsv($out, $fields, $delimiter);
	foreach ($posts as $post) {
		fputcsv($out, iterator_to_array(_hyp_yield_post_info($post, $fields)), $delimiter);
	}
	fclose($out);

	die();
}

function _hyp_yield_post_info($post, $fields ) {
	
	foreach ($fields as $field) {
		if (isset($post->$field)) { 
			yield $post->$field;
		}
		else {
			yield NULL;
		}
	}

}
