<?php

/**
 * Fired during plugin deactivation
 *
 * @link       N/A
 * @since      1.0.0
 *
 * @package    Woo_Search_Export
 * @subpackage Woo_Search_Export/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Woo_Search_Export
 * @subpackage Woo_Search_Export/includes
 * @author     Spyros Nathanail <spyros.nathanail@hypernetica.com>
 */
class Woo_Search_Export_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
